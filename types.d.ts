/* eslint-disable import/order */
/* eslint-disable import/no-duplicates */

declare module "*.mdx" {
  import { ComponentType } from "react";

  const ReactComponent: ComponentType;

  export default ReactComponent;
}

declare module "*.md" {
  import { ComponentType } from "react";

  const ReactComponent: ComponentType;

  export default ReactComponent;
}

declare module "markdown-truncate" {
  export default function truncateMarkdown(
    inputText: string,
    options: { limit: number; ellipsis: boolean }
  ): string;
}
