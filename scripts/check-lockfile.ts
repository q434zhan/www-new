import lockfile from "../package-lock.json";

if (lockfile.lockfileVersion !== 2) {
  console.error(
    `
Please upgrade to npm v7 and revert changes to the lockfile.

- \`npm i -g npm\` to upgrade.
`.trim()
  );
  process.exit(1);
}
