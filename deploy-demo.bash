#!/bin/bash
export NEXT_PUBLIC_BASE_PATH="/~$USER/website-demo"
rm -rf ~/www/website-demo
npm install
npm run build
npm run export
mv out ~/www/website-demo
