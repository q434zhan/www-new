const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
});

module.exports = withMDX({
  pageExtensions: ["ts", "tsx"],
  trailingSlash: true,
  basePath: process.env.NEXT_PUBLIC_BASE_PATH,
  eslint: { ignoreDuringBuilds: true },
  rewrites: async () => {
    return [
      {
        source: '/stats(/?)',
        destination: '/stats.html',
      },
    ];
  },
});
