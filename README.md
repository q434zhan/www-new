# README

## Documentation

- [Architecture and Folder Structure](docs/architecture.md)
- [Everything about pages](docs/pages.md)

## Development

### Dependencies

Make sure that you have `node` >= 14 and `npm` >= 7. Node 14 ships with npm v6,
so if you're using node 14, you would need to upgrade npm. Alternatively you
could also upgrade to node 16, which ships with npm 7.

How to upgrade npm: `npm i -g npm`

### Local

- `npm install` to install project dependencies
- `npm run build:images` to optimize images for the first time after cloning
- `npm run dev` to run the dev server (http://localhost:3000)

### Production

- `npm install` to install project dependencies
- `npm run build` to generate html/css/js
- `npm run export` to move the built files (along with assets in the public directory) to the `/out` directory
- Use your favourite web server to host the files in the `/out` directory. (A very simple one would be `python -m http.server` - not sure if it should actually be used for production :P)

## Deploy

- `groups` (make sure you're in the `www` group)
- `curl -o- https://git.csclub.uwaterloo.ca/www/www-new/raw/branch/main/deploy.sh | bash` (run on `caffeine`)
