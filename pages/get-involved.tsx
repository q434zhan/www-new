import React from "react";

import { ConnectWithUs } from "@/components/ConnectWithUs";
import { EmailSignup } from "@/components/EmailSignup";
import { Image } from "@/components/Image";
import { Title } from "@/components/Title";

import Content from "../content/get-involved.mdx";

import styles from "./get-involved.module.css";

export default function GetInvolved() {
  return (
    <div className={styles.page}>
      <Title>Get Involved</Title>
      <header>
        <div className={styles.headerText}>
          <h1>Get Involved!</h1>
          <p>
            If you’re interested in getting involved with CS Club, don’t
            hesitate to get in touch with us!
          </p>
        </div>
        <Image src="images/get-involved/codey.svg" className={styles.codey} />
      </header>
      <main className={styles.content}>
        <Content />
      </main>
      <div className={styles.connectWithUs}>
        <ConnectWithUs />
      </div>
      <div className={styles.emailSignup}>
        <EmailSignup />
      </div>
    </div>
  );
}
