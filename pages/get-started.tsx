import Head from "next/head";
import React from "react";

export default function GetStartedRedirect() {
  return (
    <Head>
      <meta httpEquiv="refresh" content="0;url=/get-involved" />
    </Head>
  );
}
