import { GetStaticProps } from "next";

import { ArchivePage, Props } from "@/components/ArchivePage";
import { getEventTermsByYear, getEventYears } from "@/lib/events";

export default ArchivePage;

export const getStaticProps: GetStaticProps<Props> = async () => {
  const years = (await getEventYears()).reverse();
  const yearsWithTerms = await Promise.all(
    years.map(async (year) => ({
      year: year.toString(),
      terms: (await getEventTermsByYear(year)).reverse(),
    }))
  );

  return { props: { items: yearsWithTerms, type: "events" } };
};
