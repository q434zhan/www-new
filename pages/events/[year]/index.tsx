import { ParsedUrlQuery } from "querystring";

import { GetStaticPaths, GetStaticProps } from "next";
import React from "react";

import { Link } from "@/components/Link";
import { Title } from "@/components/Title";
import { getEventYears, getEventTermsByYear } from "@/lib/events";
import { Term } from "@/utils";

import styles from "./index.module.css";

interface Props {
  year: string;
  terms: Term[];
}

export default function Year(props: Props) {
  return (
    <div className={styles.main}>
      <Title>{["Events", props.year]}</Title>
      <h2>
        Events Archive:<span className={styles.blue}>{` ${props.year}`}</span>
      </h2>
      <ul className={styles.list}>
        {props.terms.map((term) => (
          <li key={`${props.year}-${term}`}>
            <Link href={`/events/${props.year}/${term}`}>
              {`${term.charAt(0).toUpperCase()}${term.slice(1)}`}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

interface Params extends ParsedUrlQuery {
  year: string;
}

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { year } = context.params!;
  return {
    props: {
      year: year,
      terms: await getEventTermsByYear(parseInt(year)),
    },
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const years = await getEventYears();
  const paths = years.map((curYear) => ({
    params: { year: curYear.toString() },
  }));
  return {
    paths: paths,
    fallback: false,
  };
};
