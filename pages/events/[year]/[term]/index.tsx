import { ParsedUrlQuery } from "querystring";

import { GetStaticPaths, GetStaticProps } from "next";
import { MDXRemote } from "next-mdx-remote";
import React from "react";

import { EventCard } from "@/components/EventCard";
import { Link } from "@/components/Link";
import { MiniEventCard } from "@/components/MiniEventCard";
import { Title } from "@/components/Title";
import {
  Event,
  getEventYears,
  getEventTermsByYear,
  getEventsPageProps,
} from "@/lib/events";
import { capitalize, Term, TermYear } from "@/utils";

import styles from "./index.module.css";

export interface Props {
  year: number;
  term: Term;
  pastEvents: Event[];
  futureEvents: Event[];
  isCurrentTerm: boolean;
  pastTerms: TermYear[];
  futureTerms: TermYear[];
}

export default function TermPage(props: Props) {
  let headerTerms = [{ year: props.year, term: props.term }];

  // p, Current, f
  if (props.pastTerms.length > 0 && props.futureTerms.length > 0) {
    headerTerms = [
      ...props.pastTerms.slice(-1),
      ...headerTerms,
      ...props.futureTerms.slice(0, 1),
    ];
  }
  // p, p, Current
  else if (props.pastTerms.length > 0) {
    headerTerms = [...props.pastTerms.slice(-2), ...headerTerms];
  }
  // Current, f, f
  else {
    headerTerms = [...headerTerms, ...props.futureTerms.slice(0, 2)];
  }

  headerTerms.reverse();

  const hasPastEvents = props.pastEvents.length !== 0;
  const hasFutureEvents = props.futureEvents.length !== 0;

  return (
    <div className={styles.main}>
      <Title>{["Events", `${capitalize(props.term)} ${props.year}`]}</Title>
      <div className={styles.header}>
        {headerTerms.map((link) => (
          <HeaderLink
            {...link}
            isCurrentTerm={link.year === props.year && link.term === props.term}
            key={`${link.year}${link.term}`}
          />
        ))}
        <Link href="/events/archive">Archive</Link>
      </div>
      {hasFutureEvents && (
        <section>
          <h1>Upcoming Events</h1>
          <div className={styles.miniEventCards}>
            {props.futureEvents.map(({ content, metadata }) => (
              <EventCard
                {...metadata}
                startDate={new Date(metadata.startDate)}
                endDate={
                  metadata.endDate ? new Date(metadata.endDate) : undefined
                }
                key={metadata.name + metadata.startDate.toString()}
                titleLinked={true}
              >
                <MDXRemote {...content} />
              </EventCard>
            ))}
          </div>
        </section>
      )}
      {hasPastEvents && (
        <section>
          {props.isCurrentTerm ? (
            <h1>Past Events</h1>
          ) : (
            <h1>
              Events Archive:
              <span className={styles.blue}>
                {` ${capitalize(props.term)} ${props.year}`}
              </span>
            </h1>
          )}
          <div className={styles.miniEventCards}>
            {props.pastEvents.map((event, idx) => (
              <MiniEventCard
                {...event.metadata}
                startDate={new Date(event.metadata.startDate)}
                endDate={
                  event.metadata.endDate
                    ? new Date(event.metadata.endDate)
                    : undefined
                }
                description={<MDXRemote {...event.content} />}
                key={event.metadata.name + event.metadata.startDate.toString()}
                background={idx % 2 === 0 ? "dark-bg" : "normal-bg"}
              />
            ))}
          </div>
        </section>
      )}
      {!hasFutureEvents && !hasPastEvents && (
        <>
          <h1>Events</h1>
          There are no upcoming or past events for the{" "}
          {`${capitalize(props.term)} ${props.year}`} term. Please check back
          later!
        </>
      )}
    </div>
  );
}

function HeaderLink(props: {
  year: number;
  term: Term;
  isCurrentTerm?: boolean;
}) {
  return (
    <Link href={`/events/${props.year}/${props.term}`}>
      <span className={props.isCurrentTerm ? styles.curTerm : ""}>
        {`${capitalize(props.term)} ${props.year}`}
      </span>
    </Link>
  );
}

interface Params extends ParsedUrlQuery {
  year: string;
  term: Term;
}

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const params = context.params!;

  return {
    props: await getEventsPageProps({
      year: parseInt(params.year),
      term: params.term,
    }),
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const years = await getEventYears();
  const paths = (
    await Promise.all(
      years.map(async (year) => {
        const terms = await getEventTermsByYear(year);
        return terms.map((curTerm) => ({
          params: { year: year.toString(), term: curTerm },
        }));
      })
    )
  ).flat();

  return {
    paths: paths,
    fallback: false,
  };
};
