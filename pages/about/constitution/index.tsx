import path from "path";

import {
  createReadAllPage,
  Options,
} from "@/components/OrganizedContent/ReadAll";
import { createReadAllGetStaticProps } from "@/components/OrganizedContent/static";

export const options: Options = {
  title: "Constitution",
  image: "images/constitution.svg",
  pagePath: path.join("about", "constitution"),
  numberedSections: true,
};

export default createReadAllPage(options);

export const getStaticProps = createReadAllGetStaticProps(options.pagePath);
