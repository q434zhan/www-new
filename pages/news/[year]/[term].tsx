import { ParsedUrlQuery } from "querystring";

import { GetStaticPaths, GetStaticProps } from "next";
import { MDXRemote } from "next-mdx-remote";
import React from "react";

import { NewsCard } from "@/components/NewsCard";
import {
  ShapesConfig,
  defaultGetShapesConfig,
  GetShapesConfig,
} from "@/components/ShapesBackground";
import { Title } from "@/components/Title";
import {
  getNewsBySlug,
  getNewsByTerm,
  getNewsTermsByYear,
  getNewsYears,
  News,
} from "@/lib/news";
import { capitalize, Term } from "@/utils";

import styles from "./[term].module.css";

interface Props {
  year: string;
  term: Term;
  news: News[];
}

export default function TermNews({ year, term, news }: Props) {
  return (
    <div className={styles.page}>
      <Title>{["News", `${capitalize(term)} ${capitalize(year)}`]}</Title>
      <h1>
        News Archive:{" "}
        <span className={styles.term}>
          {capitalize(term)} {year}
        </span>
      </h1>
      {news.map(({ content, metadata }, idx) => (
        <NewsCard
          key={idx}
          {...metadata}
          date={new Date(metadata.date)}
          fit={true}
        >
          <MDXRemote {...content} />
        </NewsCard>
      ))}
    </div>
  );
}

TermNews.getShapesConfig = ((width, height) => {
  return window.innerWidth <= 768
    ? ({} as ShapesConfig)
    : defaultGetShapesConfig(width, height);
}) as GetShapesConfig;

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { year, term } = context.params!;
  const slugs = await getNewsByTerm(year, term);
  const news = await Promise.all(
    slugs.map((slug) => getNewsBySlug(year, term, slug))
  );

  // Reverse so that we are displaying the most recent news
  // of term first
  return { props: { year, term, news: news.reverse() } };
};

interface Params extends ParsedUrlQuery {
  year: string;
  term: Term;
}

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const years = await getNewsYears();
  const terms = await Promise.all(
    years.map(async (year) => {
      const termsInYear = await getNewsTermsByYear(year);
      return termsInYear.map((term) => ({ year, term }));
    })
  );

  return {
    paths: terms.flat().map((params) => ({ params })),
    fallback: false,
  };
};
