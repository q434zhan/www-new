import path from "path";

import {
  createReadAllPage,
  Options,
} from "@/components/OrganizedContent/ReadAll";
import { createReadAllGetStaticProps } from "@/components/OrganizedContent/static";

export const options: Options = {
  title: "Services",
  image: "images/services.svg",
  pagePath: path.join("resources", "services"),
  description:
    "Here you can find all the links and instructions to the different resources available to all CSC members!",
  imagePosition: "right",
};

export default createReadAllPage(options);

export const getStaticProps = createReadAllGetStaticProps(options.pagePath);
