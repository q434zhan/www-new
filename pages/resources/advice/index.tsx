import Head from "next/head";
import React from "react";

export default function AdviceRedirect() {
  return (
    <Head>
      <meta httpEquiv="refresh" content="0;url=/resources/advice/co-op" />
    </Head>
  );
}
