import { GetStaticProps } from "next";
import React from "react";

import { Image } from "@/components/Image";
import { MiniTechTalkCard } from "@/components/MiniTechTalkCard";
import { Title } from "@/components/Title";
import { getTechTalks, Metadata } from "@/lib/tech-talks";

import styles from "./index.module.css";

interface Props {
  talks: Metadata[];
}

export default function TechTalks({ talks }: Props) {
  return (
    <div className={styles.page}>
      <Title>Tech Talks</Title>
      <Header />
      <div className={styles.miniCards}>
        {talks.map((talk) => (
          <MiniTechTalkCard
            {...talk}
            poster={talk.thumbnails.small}
            key={talk.slug}
          />
        ))}
      </div>
    </div>
  );
}

export function Header() {
  return (
    <header className={styles.headerContainer}>
      <div className={styles.header}>
        <h1>Tech Talks</h1>
        <p className={styles.headerContent}>
          These are the audio and video recordings of past CSC and other
          university-related talks. Our public events can also be found on our
          YouTube channel.
        </p>
      </div>
      <Image src="images/tech-talks.svg" className={styles.image} />
    </header>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  return {
    props: { talks: (await getTechTalks()).map(({ metadata }) => metadata) },
  };
};
