import React, { ReactNode } from "react";

import { Image } from "@/components/Image";

import styles from "./Header.module.css";

export interface Props {
  title: string;
  image: string;
  children: ReactNode;
  description?: string;
  imagePosition?: "left" | "right";
}

export function Header({
  title,
  image,
  children,
  description,
  imagePosition,
}: Props) {
  return (
    <main className={styles.page}>
      <header
        className={`${styles.headerContainer} ${
          imagePosition === "right" ? styles.imageRight : ""
        }`}
      >
        <Image src={image} className={styles.headerImage} />
        <div>
          <h1 className={styles.header}>{title}</h1>
          {description && <p className={styles.description}>{description}</p>}
        </div>
      </header>
      {children}
    </main>
  );
}
