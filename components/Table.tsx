import React, { TableHTMLAttributes } from "react";

import styles from "./Table.module.css";

export function Table(props: TableHTMLAttributes<HTMLTableElement>) {
  const className = [styles.table, props.className ?? ""].join(" ");

  return <table {...props} className={className} />;
}
