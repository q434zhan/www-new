import Link from "next/link";
import React from "react";

import { Image } from "./Image";

import styles from "./MiniTechTalkCard.module.css";

interface MiniTechTalkProps {
  slug: string;
  title: string;
  presentors: string[];
  poster: string;
}

export function MiniTechTalkCard({
  slug,
  title,
  presentors,
  poster,
}: MiniTechTalkProps) {
  const presentorsStr = presentors.join(", ");

  return (
    <article className={styles.card}>
      <Link href={`/resources/tech-talks/${slug}`}>
        <a>
          <aside>
            <Image
              alt={`Thumbnail of tech talk by ${presentorsStr}: ${title}`}
              src={poster}
            />
          </aside>
          <div className={styles.content}>
            <h1>{title}</h1>
            <p>{presentorsStr}</p>
          </div>
        </a>
      </Link>
    </article>
  );
}
