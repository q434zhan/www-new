import React from "react";

import { capitalize, Term } from "@/utils";

import { Link } from "./Link";
import {
  ShapesConfig,
  GetShapesConfig,
  defaultGetShapesConfig,
} from "./ShapesBackground";
import { Title } from "./Title";

import styles from "./ArchivePage.module.css";

export interface Props {
  type: "news" | "events";
  items: {
    year: string;
    terms: Term[];
  }[];
}

export function ArchivePage({ items, type }: Props) {
  return (
    <>
      <Title>{[capitalize(type), "Archive"]}</Title>
      <div className={styles.page}>
        <h1>{capitalize(type)} Archive</h1>
        <ul className={styles.list}>
          {items.map(({ year, terms }) =>
            terms.map((term) => (
              <li key={`/${type}/${year}/${term}`}>
                <Link href={`/${type}/${year}/${term}`}>
                  {capitalize(term)} {year}
                </Link>
              </li>
            ))
          )}
        </ul>
      </div>
    </>
  );
}

ArchivePage.getShapesConfig = ((width, height) => {
  return window.innerWidth <= 768
    ? ({} as ShapesConfig)
    : defaultGetShapesConfig(width, height);
}) as GetShapesConfig;
