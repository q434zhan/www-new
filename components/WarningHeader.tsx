import { parse } from "date-fns";
import React from "react";

import { DATE_FORMAT, getLocalDateFromEST } from "@/utils";

import warnings from "../content/warnings/warnings.json";

import styles from "./WarningHeader.module.css";

interface Warning {
  message: string;
  startDate: string;
  endDate: string;
}

function getCurrentWarning(): Warning | null {
  const today = new Date();
  const currentWarnings: Warning[] = warnings.filter((warning) => {
    // convert dates to date objects in EST time zone
    let startDate = parse(warning.startDate, DATE_FORMAT, new Date());
    let endDate = parse(warning.endDate, DATE_FORMAT, new Date());

    if (
      !startDate ||
      !endDate ||
      isNaN(startDate.getTime()) || // this checks if the parsed date is not valid (eg. wrong format), since getLocalDateFromEST fails with invalid dates
      isNaN(endDate.getTime())
    ) {
      throw new Error('WARNING WITH INVALID DATES: "' + warning.message + '"');
    }

    startDate = getLocalDateFromEST(startDate);
    endDate = getLocalDateFromEST(endDate);

    return (
      startDate.getTime() <= today.getTime() &&
      endDate.getTime() >= today.getTime()
    );
  });

  if (currentWarnings.length > 1) {
    // If more than one warning is scheduled, log an error to the console. We cannot throw an error, since the site would go down on the live
    //  website, on the day when more than one warning is scheduled.
    console.error(
      "ERROR: MORE THAN ONE WARNING SCHEDULED CURRENTLY! ",
      currentWarnings
    );
  }

  return currentWarnings.length === 0 ? null : currentWarnings[0];
}

export function WarningHeader() {
  const warning = getCurrentWarning();

  if (warning == null) {
    return null;
  }

  return <div className={styles.warning}>{warning.message}</div>;
}
