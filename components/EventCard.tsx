import React, { ReactNode } from "react";

import { Button } from "./Button";
import { EventSetting } from "./EventSetting";
import { Image } from "./Image";
import { Link } from "./Link";

import styles from "./EventCard.module.css";

interface EventCardProps {
  name: string;
  short: string;
  startDate: Date;
  endDate?: Date;
  online: boolean;
  location: string;
  poster?: string;
  registerLink?: string;
  permaLink: string;
  showDescription?: boolean;
  children: ReactNode;
  year: number;
  term: string;
  slug: string;
  titleLinked: boolean;
}

export function EventCard({
  permaLink,
  name,
  startDate,
  endDate,
  online,
  location,
  poster,
  registerLink,
  children,
  showDescription = false,
  year,
  term,
  slug,
  titleLinked,
}: EventCardProps) {
  return (
    <article className={styles.card}>
      {poster && (
        <aside>
          <Image alt={name} src={poster} />
          {registerLink && (
            <Button
              isLink={true}
              href={registerLink}
              size="small"
              className={`${styles.registerButton} ${styles.registerButtonWithPoster}`}
            >
              Register
            </Button>
          )}
        </aside>
      )}
      <section
        className={[
          styles.content,
          showDescription ? styles.mobileShowDescriptionContent : "",
        ].join(" ")}
      >
        <h1>
          {titleLinked ? (
            <Link href={`/events/${year}/${term}/${slug}`}>{name}</Link>
          ) : (
            name
          )}
        </h1>
        <h2>
          <EventSetting
            startDate={startDate}
            endDate={endDate}
            online={online}
            location={location}
          />
        </h2>
        {!showDescription && (
          <Link href={permaLink}>
            <span className={styles.mobileLearnMore}>Learn more</span>
          </Link>
        )}
        <div className={styles.children}>{children}</div>
        {!poster && registerLink && (
          <Button
            isLink={true}
            href={registerLink}
            size="small"
            className={styles.registerButton}
          >
            Register
          </Button>
        )}
      </section>
    </article>
  );
}
