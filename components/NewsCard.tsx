import React, { ReactNode } from "react";

import { Link } from "./Link";

import styles from "./NewsCard.module.css";

interface NewsCardProps {
  date: Date;
  author: string;
  children: ReactNode;
  permalink: string;
  fit?: boolean;
}

export const NewsCard: React.FC<NewsCardProps> = ({
  date,
  author,
  children,
  permalink,
  fit = false, // resizes the article to fit the parent container if it's not a mini card
}) => {
  const classes = fit ? [styles.card, styles.fit] : [styles.card];

  return (
    <article className={classes.join(" ")}>
      <h1 className={styles.date}>
        <time dateTime={date.toISOString()}>
          {date.toLocaleDateString("en-US", {
            year: "numeric",
            month: "long",
            day: "numeric",
          })}
        </time>
      </h1>
      <address className={styles.author}>{author}</address>
      <div className={styles.content}>{children}</div>
      {!fit && (
        <Link href={permalink}>
          <span>Learn more</span>
        </Link>
      )}
    </article>
  );
};
