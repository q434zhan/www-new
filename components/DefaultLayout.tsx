import React from "react";

import styles from "./DefaultLayout.module.css";

export function DefaultLayout(props: { children: React.ReactNode }) {
  return <div className={styles.defaultLayout}>{props.children}</div>;
}
