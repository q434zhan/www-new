import React, { AnchorHTMLAttributes, ButtonHTMLAttributes } from "react";

import styles from "./Button.module.css";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  isLink?: false;
}

interface LinkProps extends AnchorHTMLAttributes<HTMLAnchorElement> {
  isLink: true;
}

type Props = (ButtonProps | LinkProps) & { size?: "small" | "normal" };

export function Button(props: Props) {
  const btnSize = props.size ? props.size : "normal";
  if (props.isLink) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { size, isLink, ...otherProps } = props;
    return (
      <a
        {...otherProps}
        target="_blank"
        rel="noreferrer"
        className={`${styles.link} ${styles[btnSize]} ${
          otherProps.className ?? ""
        }`}
      />
    );
  } else {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { size, isLink, ...otherProps } = props;
    return (
      <button
        {...otherProps}
        className={`${styles.button} ${styles[btnSize]} ${
          otherProps.className ?? ""
        }`}
      />
    );
  }
}
