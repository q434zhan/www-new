import React from "react";

import { Button } from "./Button";
import { EventSetting } from "./EventSetting";
import { Image } from "./Image";
import { Link } from "./Link";
import { Discord, Twitch, Instagram, Facebook } from "./SocialLinks";

import styles from "./EventDescriptionCard.module.css";

interface Props {
  name: string;
  short: string;
  online: boolean;
  location: string;
  startDate: Date;
  endDate?: Date;
  poster?: string;
  registerLink?: string;
  permaLink: string;
}

/**
 * @remarks
 * - Child elements will display as the event's description
 * - Assuming date prop is in EST
 * - poster is the event's image name, including file ending (.jpg/.png/etc)
 * - If event is online, location will be the platform, with capital first letter and no trailing spaces
 *    - ie. location="Discord"
 * @todo
 * get Link component
 */
export function EventDescriptionCard({
  location,
  poster,
  name,
  short,
  startDate,
  endDate,
  online,
  registerLink,
  permaLink,
}: Props) {
  const Icon = getIcon(location);

  return (
    <article className={styles.card}>
      {poster && <Image className={styles.poster} src={poster} alt={name} />}

      <div className={styles.details}>
        <h1 className={styles.name}>{name}</h1>
        <h2 className={styles.setting}>
          <EventSetting
            startDate={startDate}
            endDate={endDate}
            online={online}
            location={location}
          />
        </h2>
        <p className={styles.desc}>{short}</p>
        <Link href={permaLink}>Learn more</Link>

        <footer>
          {registerLink && (
            <div className={styles.button}>
              <Button isLink={true} href={registerLink} size="small">
                Register
              </Button>
            </div>
          )}
          {online && Icon && (
            <div className={styles.logo}>
              <Icon color="blue" size="small" />
            </div>
          )}
        </footer>
      </div>
    </article>
  );
}

function getIcon(platform: string) {
  switch (platform) {
    case "Twitch":
      return Twitch;
    case "Discord":
      return Discord;
    case "Instagram":
      return Instagram;
    case "Facebook":
      return Facebook;
    default:
      return null;
  }
}
