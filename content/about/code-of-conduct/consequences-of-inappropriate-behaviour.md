---
title: Consequences of Inappropriate Behaviour
---

After having done so, the Handling Officer shall use their best judgment to determine if the complaint is valid and, if so, determine with the relevant Officers the appropriate action to ensure that the complainant feels welcome in the Computer Science Club and to avoid a subsequent incident:

- A warning.
- A suspension from the events and spaces governed by the Code of Conduct until the beginning of the next term. If the suspension would come into effect less than two full weeks from the end of classes in the current term, then the suspension applies to the subsequent term as well.
- If the incident is very serious, or the subject has a pattern of similar offences, expulsion from the Club.
- A formal complaint through University policy, such as 33, 34, and 42.

The Handling Officer shall inform the complainant of the resolution of the issue and inform both the complainant and the subject of their right to appeal the decision.
