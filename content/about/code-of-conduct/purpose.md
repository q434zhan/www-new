---
title: Purpose
---

One of the primary goals of the Computer Science Club of the University of Waterloo is the inclusion and support of all members of the University of Waterloo community who are interested in Computer Science. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sex, sexual orientation, ability, ethnicity, socioeconomic status, age, and religion or lack thereof.

We invite all those who participate in our events and who communicate with our Club at large to help us create a safe and positive experience for everyone involved.

- The Code of Conduct highlights our expectations for all individuals who participate in our Club, as well as the steps to handle unacceptable behaviour.
- The Code of Conduct should be signed by new Club members.
- The Code of Conduct is in addition to existing University Policies, such as [policies 33](https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-33), [34](https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-34), and [42](https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policies/policy-42-prevention-and-response-sexual-violence).
- The Code of Conduct does not cover criminal matters. Initiating a Code of Conduct complaint does not exclude other paths, such as going to Police Services. For criminal matters, threats or acts of physical violence, immediately contact directly UW Police at (519) 888-4911, or use the On Campus Extension x22222.
