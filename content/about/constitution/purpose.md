---
title: Purpose
---

1. The Club is organized and will be operated exclusively for educational and scientific purposes in furtherance of:
    1. promoting an increased knowledge of computer science and its applications;
    1. providing a means of communication between persons having interest in computer science.
    1. promoting a greater interest in computer science and its applications; and
1. The above purposes will be fulfilled by the organization of discussions and lectures with professionals and academics in the field of computer science and related fields, the maintenance of a library of materials related to computer science, the maintenance of an office containing the library as an aid to aim (1.3) above, and such other means as decided by the club membership.
