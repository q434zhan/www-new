---
title: Use of Club Resources
---

1. All resources under control of the Club are to be used in accordance with the aims of the Club.
2. The President and Vice-President are jointly responsible for the proper use of all Club resources, except as otherwise specified by the Executive or this Constitution.
3. The Systems Administrator is responsible for the proper use of all administrative access to Club computing resources.
4. The Executive, via the Systems Administrator and the Systems Committee, are responsible for the proper use of all Club computing resources.
5. Permission to use a resource is automatically granted to someone responsible for that resource, and optionally as determined by those responsible for a resource. Granting permission to someone to use a resource does not make that person responsible for the proper use of the resource, although the user is, of course, responsible to the person granting permission.
