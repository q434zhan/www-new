---
title: Amendments and Procedures
---

1. A constitutional amendment can be initiated by the Executive Council or any thirty (30) members. The initiation shall involve making the amendment available for viewing, including in the CSC office.
2. There shall be at least twenty-eight (28) days between the amendment's initiation and announcement of the finalized version. Members shall be provided an avenue to discuss the amendment throughout this time period.
3. The finalized version of the amendment shall be announced to all members, taking place through email to the members' mailing list.
4. A general meeting shall be held to consider the finalized version of the amendment at least seven (7) and no more than thirty (30) days after its announcement, which may be the regular meeting for the term, or a special meeting.
5. A constitutional amendment requires a 2/3 vote for adoption.
