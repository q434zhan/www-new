---
title: Duties of Officers
---

1. The duties of the President shall be:
    1. to call and preside at all general, special, and executive meetings of the Club, except during the election of officers;
    1. to appoint special committees of the Club and the membership and chairs of such committees, with the approval of the Executive Council;
    1. to audit, or to appoint a representative to audit, the financial records of the club at the end of each academic term; and
    1. with the approval of the Faculty Advisor, rule on any point of procedure under the constitution that arises outside of a meeting.
1. The duties of the Vice-President shall be:
    1. to assume the duties of the President in the event of the President's absence;
    1. to chair the Programme Committee;
    1. to appoint members to and remove members from the Programme Committee;
    1. to ensure that Club events are held regularly; and
    1. to assume those duties of the President that are delegated to them by the President.
1. The duties of the Assistant Vice-President shall be:
    1. to keep minutes of all Club meetings;
    1. to care for all Club correspondence; and
    1. to manage any persons appointed to internal positions by the Executive.
1. The duties of the Treasurer shall be:
    1. to collect dues and maintain all financial and membership records; and
    1. to produce a financial or membership statement when requested.
1. The duties of the System Administrator shall be:
    1. to chair the Systems Committee;
    1. to appoint members to and remove members from the Systems Committee;
    1. to ensure that the duties of the Systems Committee are performed; and
    1. to act as a liaison for the CSC with CSCF, IST, and similar organizations.
