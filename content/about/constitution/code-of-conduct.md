---
title: Code of Conduct
---

1. The Club has a [Code of Conduct](/about/code-of-conduct).
2. The [scope of the Code of Conduct](/about/code-of-conduct/scope-and-spaces) is specified by the Code of Conduct.
3. Changes to the Code of Conduct are governed by the same rules as changes to the Constitution.
