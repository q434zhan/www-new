---
name: Andrea Ma
role: Treasurer
---

Hi! I’m Andrea and I am in 3B of CSBBA. I’m excited to be both the CSC Treasurer and Events Co-lead to deliver another term of top-tier events. We plan to have great food and snacks purchased with our budget, as well as unique collaborations with other clubs and companies. In my free time, I like to play sports like volleyball, badminton, and ultimate frisbee. I also like trying new things, like UW Hip Hop this term, new cafes, and new recipes to make at home. Please say hi if you see me around :) 
