---
name: Codey
role: Mascot
---

The one, the only, Codey! Codey is a friendly and welcoming Shiba who uses they/them pronouns. Codey loves programming and playing on their laptop. You can often find Codey posing for event promo graphics, or chilling in the CSC discord.