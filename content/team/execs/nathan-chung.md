---
name: Nathan Chung
role: Systems Administrator
---

Howdy 🤠, I’m Nathan. At CSC, I help manage all club infrastructure and services, alongside with other amazing members of the Systems Committee and Terminal Committee. I love all things tech, especially everything AI 😶‍🌫️. I am currently in my 2🅰️ term and pursuing a URA related to AI safety and privacy. In my free time, I also enjoy gardening 🌱, playing chess puzzles♟️, skiing in the winter (used to be a ski instructor ⛷️) and a variety of other sports 🏌️🏸🏀. Feel free to hit me up for anything (on Discord or Instagram). (Also, FYI I did not GPT this blurb 🥹)
