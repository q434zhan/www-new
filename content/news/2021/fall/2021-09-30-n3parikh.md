---
author: 'n3parikh'
date: 'September 29 2021 00:00'
---
DSC and CSC are excited to announce that we’re bringing back the Project Program for Fall 2021!

Project Program is a month-long experience where you can plan and create amazing side projects as we support you! 🖥️

For Fall 2021, we will once again be pairing mentees with mentors who will help guide the mentees. 🏆 Together, you will let your creativity flow as you brainstorm ideas and create a final project that you’re passionate about. At the end, you’ll have the chance to present your project to win prizes! 

The applications for mentees are officially open! As a mentee, you’re responsible for creating a project, collaborating with your group, and learning along the way. ✨

If you’re interested in being a mentee, sign up today! 

 👉  Sign up at https://bit.ly/project-program-mentee-signup 

Alternatively, you can email us at exec@csclub.uwaterloo.ca with a short description about yourself, your experience, what types of projects you’d like to build, and who you’d like to work with!

📅 Deadline to Sign Up: October 1, 2021 
