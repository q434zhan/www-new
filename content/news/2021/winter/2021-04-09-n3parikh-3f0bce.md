---
author: 'n3parikh'
date: 'April 09 2021 01:00'
---

Thank you to all the people that were able to attend our Get Involved event! As promised, we are excited to announce that we are officially accepting applications for CSC's Executive committee.

The positions available for Spring 2021 term are:

- Event Coordinators
- Marketing Coordinators
- Discord Moderators
- Class Representatives
- Senior Frontend Developer 
- Graphic Designers
- Linux Server Administrator

<!-- -->

If you're passionate about making a positive change in our community, we'd love for you to apply! You can find more information about each role at this link: [http://bit.ly/uwcsclub-s21-roles](<http://bit.ly/uwcsclub-s21-roles>)!

**Application Deadline**: Friday, April 23rd at 11:59pm EDT

Apply at [http://bit.ly/uwcsclub-s21-apply](<http://bit.ly/uwcsclub-s21-apply>)! Alternatively, you can email us at exec@csclub.uwaterloo.ca from your UW email with an introduction of yourself, which positions you're interested in and any questions you might have.

Looking forward to reading your applications!