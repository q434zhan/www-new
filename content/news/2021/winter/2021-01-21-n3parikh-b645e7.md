---
author: 'n3parikh'
date: 'January 21 2021 00:00'
---

Winter 2021 elections have concluded. Here are your executives for the term:

- President: Kallen Tu (k4tu)
- VP: Gordon Le (g2le)
- AVP: Nakul Vijhani (nvijhani)
- Treasurer: Neil Parikh (n3parikh)
- Sysadmin: Max Erenberg (merenber)

<!-- -->

The unfilled positions this term are:

- Office Manager
- Librarian
- IMAPD

<!-- -->