---
author: 'merenber'
date: 'March 31 2021 01:00'
---

As part of an ongoing series to upgrade our email services, there will be some downtime for our email system over the course of the next few weeks. This affects mail.csclub.uwaterloo.ca and mailman.csclub.uwaterloo.ca. The downtime periods will only occur late at night (EDT) and should last no more than an hour at a time.

We apologize for any inconveniences this may cause. If you have any questions or concerns, please email syscom at csclub dot uwaterloo dot ca.