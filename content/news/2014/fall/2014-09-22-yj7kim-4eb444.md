---
author: 'yj7kim'
date: 'September 22 2014 01:00'
---

Elections for Fall 2014 have concluded. The following people were elected:

- President: Jinny Kim (`yj7kim`)
- Vice-president: Theo Belaire (`tbelaire`)
- Treasurer: Jonathan Bailey (`jj2baile`)
- Secretary: Shane Creighton-Young (`srcreigh`)

<!-- -->

The following people were appointed:

- Sysadmin: Sean Hunt (`scshunt`)
- Office Manager: Mark Farrell (`m4farrel`)
- Librarian: Gianni Gambetti (`glgambet`)

<!-- -->