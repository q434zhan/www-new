---
author: 'scshunt'
date: 'October 15 2014 01:00'
---

Our [free software mirror](<http://wiki.csclub.uwaterloo.ca/Mirror>) is currently down due to maintenance and multiple disk failure on the backup. We are working on bringing up a replacement. We do not have an ETA at this time for when service will be restored. Apologies for any inconvenience.