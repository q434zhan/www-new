---
author: 'n3parikh'
date: 'September 18 2018 01:00'
---

Here are the results from this term's elections:

- President: Zichuan Wei (z34wei)
- Vice-President: Uday Barar (ubarar)
- Treasurer: Alex Tomala (actomala)
- Assistant Vice-President: Neil Parikh (n3parikh)

<!-- -->

Additionally, the following positions were appointed:

- Systems Administrator: Jennifer Zhou (c7zou)
- Office Manager: Alexander Zvorygin (azvorygi)
- Librarian: Neil Parikh (n3parikh)

<!-- -->