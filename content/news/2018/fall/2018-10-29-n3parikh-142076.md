---
author: 'n3parikh'
date: 'October 29 2018 01:00'
---

CSC will be hosting Alt-Tab, a slightly longer version of lightning talks. It will be a night full of friendly talks during the week of November 26.

We need talks! Contact [exec@csclub.uwaterloo.ca](<mailto:exec@csclub.uwaterloo.ca>), with your tentative talk title and a short abstract if you would like to give a talk. Slides are not required. Please aim for a 10-15 minute talk. I'll send an email with the list of talks closer to the date of the event. Please send titles and abstract by November 19th.

Here are some topics that our members have presented before. The titles make them seem more technical than they actually were.

- Overview of SAT solvers
- Communication complexity
- Register allocation in compilers as graph colouring
- Web application security
- PXE booting
- UI and UX
- Train signalling and sensing
- Typed Racket
- Modern GPU architecture
- The comment that took Stack Exchange down and the algorithm that could have saved them
- Register Allocation With Graphs

<!-- -->

There will be food.