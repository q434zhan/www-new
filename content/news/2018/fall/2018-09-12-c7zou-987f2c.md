---
author: 'c7zou'
date: 'September 12 2018 01:00'
---

Nominations for the Fall 2018 exec are open. You can nominate candidates for President, Vice President, Assistant Vice President, or Treasurer by submitting their name, username, and position to the ballot box in the CSC, or by emailing [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>) with their name and position(s). Nominations close Monday, September 17th.

Elections will be held Tuesday, September 18th in the Comfy Lounge at 6:30PM. All CSC members that have paid Mathsoc fees are invited to come by and vote.

Nominations are listed [here](<https://csclub.uwaterloo.ca/elections/>).