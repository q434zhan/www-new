---
author: 'm2ellis'
date: 'September 04 2010 01:00'
---

Nominations for the fall term executives are now open. They will close on Sunday September 12. The election will be held on Tuesday the 14th. To enter a nomination write the name/position on the CSC office whiteboard or send it to [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>)