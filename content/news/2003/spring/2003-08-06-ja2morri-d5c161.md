---
author: 'ja2morri'
date: 'August 06 2003 01:00'
---

We've finally gotten around to disabling accounts. If you find your account has been improperly disabled please email [the executive](<mailto:exec@csclub.uwaterloo.ca>). We are keeping a back up of the files and mail for each disabled account for a short period of time.