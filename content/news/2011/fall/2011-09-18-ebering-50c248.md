---
author: 'ebering'
date: 'September 18 2011 01:00'
---

Nominations for the Fall 2011 exec are closed. Nominees are as follows:

- President: m4burns, kshyatt, ehashman
- Vice-president: m4burns, kshyatt
- Treasurer: j3parker, scshunt, sagervai
- Secretary: ehashman, kspaans, sagervai

<!-- -->

Elections will be held Monday, September 19th in the Comfy Lounge (MC 3001) from 4:30PM to 5:30PM. All CSC members that have paid Mathsoc fees are invited to come by and vote. If you are unable to attend but would like to vote please e-mail your ballot to [the CRO](<mailto:cro@csclub.uwaterloo.ca>). Voting will take place by approval voting, so vote for as many people in each race as you like.