---
author: 'sjdutoit'
date: 'April 15 2002 01:00'
---

Added the [membership list](</about/members>). [Old events](</events/old>) are working. Event terms, as well as the current term are determined automagically now. Lots of work done. Some more stubs up (office etc.). And I introduce cow - that is, the CSC Ontological Webalizer - which is just a wrapper around libxslt with some extra xpath functions. It is now what is being used to build the site, instead of xsltproc.