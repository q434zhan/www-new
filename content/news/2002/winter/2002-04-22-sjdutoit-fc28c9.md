---
author: 'sjdutoit'
date: 'April 22 2002 01:00'
---

Added [books](<http://library.csclub.uwaterloo.ca/>)! About 2.5 shelves are there, minus a whole lot that weren't readily accessible from the Library of Congress. Getting all of the books on there is going to be a tough job. These are, by the way, managed by good-old (or new?) CEO. Thanks to Ryan Golbeck and Petio for their hard work getting ISBN numbers onto disc.