---
author: 'sjdutoit'
date: 'September 18 2002 01:00'
---

We've changed to the new site! Please send your comments to the [webmaster](<mailto:webmaster@csclub.uwaterloo.ca>). The [old site](</old/>) is still available. A few things may not be working quite right yet, but I'm working on it.