---
author: 'a258wang'
date: 'May 5 2022 00:00'
---

Spring 2022 elections have concluded. Here are your executives for the term:

- President: Eden Chan (e223chan)
- Vice President: Bonnie Peng (b38peng)
- Assistant Vice President: Haley Song (h79song)
- Treasurer: Sat Arora (s97arora)
- Sysadmin: Raymond Li (r389li)

<!-- -->

Additionally, the following postions were appointed:

- Head Community Coordinator: Sat Arora (s97arora)
- Heads of Discord: Mark Chen (m375chen) and Alex Zhang (xc22zhan)
- Head of Design: Aaryan Shroff (a2shroff)
- Heads of Events: Bonnie Peng (b38peng) and Catherine Wan (c29wan)
- Head of External Affairs: Eric Liu (e59liu)
- Head of Marketing: Haley Song (h79song)
- Office Manager: Sat Arora (s97arora)

<!-- -->
