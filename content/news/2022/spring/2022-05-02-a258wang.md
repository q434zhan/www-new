---
author: 'a258wang'
date: 'May 02 2022 00:00'
---
CSC Cloud has been released!

Ever wanted a virtual private server (VPS) like GCP or AWS, but balked at the price?
Now you can get FREE cloud computing, included with your CSC membership!

Specs:
- 8 CPU cores
- 8 GB RAM
- 40 GB disk space

Split between up to 8 different VMs, all yours!

Read the documentation at https://docs.cloud.csclub.uwaterloo.ca/ to get started today!

Note: If you need additional resources, email syscom@csclub.uwaterloo.ca with your reasoning and we will do our best to accommodate you.
