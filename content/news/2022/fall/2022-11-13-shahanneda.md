---
author: 'shahanneda'
date: 'November 13 2022 00:00'
---
🎙️ Alt-Tab is back! Join CSC for a lightning tech talk series presented by students. Alt-Tab consists of 10 to 15-minute talks about anything related to tech. We will be holding Alt-Tab in-person in late November, most likely on Tuesday Nov. 29 in the evening.

👀 We are currently looking for speakers! If you're passionate about a technical topic and interested in public speaking, please apply by filling out the form below. You don't need to have prior experience, and Alt-Tab is a great way to get started with giving talks!

🌟 You can talk about cool new features, a fun project you made, or really anything related to technology or computer science that you are excited about! For inspiration, past talks have covered topics including TypeScript, Unicode, virtual escape rooms, quantum computers, competitive programming, cryptography, and automatic differentiation.

👉 Apply here: https://csclub.ca/alt-tab-speaker-form

📅 Deadline for speaker applications: Friday, November 18, 2022 at 11:55 PM ET