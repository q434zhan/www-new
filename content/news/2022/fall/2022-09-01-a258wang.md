---
author: 'a258wang'
date: 'September 1 2022 00:00'
---
📣 The Fall 2022 Bootcamp Event is looking for mentors to take on resume critiques, host mock interviews, and help prepare students for their co-op search! This is an awesome opportunity for anyone to give back to the Waterloo community and make an impact on a student’s co-op search.

📅 There is a resume review event happening on September 14th from 6:00pm-10:00 PM ET and a mock interview event on September 21st from 6:00pm-10:00 PM ET.

ℹ️ You can choose to participate at either event for a select number of hours!

❗️ All sessions will take place virtually on our Bootcamp Discord Server! Students will have a chance to meet with you 1 on 1 to discuss their resumes/conduct their interviews.

To sign up, please visit this link: https://bit.ly/bootcamp-mentor-signups

Alternatively, you can email us at exec@csclub.uwaterloo.ca with the year and program you’re in, along with interested job paths.

📅 Deadline to Apply for Resume Reviews: September 11th, 2022, 11:59PM ET
📅 Deadline to Apply for Mock Interviews: September 18th, 2022, 11:59PM ET
