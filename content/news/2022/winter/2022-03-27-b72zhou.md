---
author: 'b72zhou'
date: 'March 27 2022 00:00'
---
📣 CS Club’s Internal Committee is hiring for Spring 2022! 📣

Are you interested in organizing program-wide events, reaching out to industry professionals, or being a member of an impactful community? Are you passionate about making a difference in the UW CS and surrounding communities?

Apply for a role on CS Club's Internal Committee for Spring 2022! We are looking for people like you to join our Programme Committee and/or Systems Committee! 🙌

👀 Role descriptions can be found at https://bit.ly/uwcsclub-s22-roles.

⏲️ The form will close on Monday April 11, at 11:59PM EST, so apply ASAP! We'll reach out through email after this date for interview slots/questions.

👉  Apply at https://bit.ly/csc-s22-apply! Alternatively, you can email us at exec@csclub.uwaterloo.ca from your UW email with an introduction of yourself, which positions you're interested in and any questions you might have! 
