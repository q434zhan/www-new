---
author: 'kpwarr'
date: 'May 22 2015 01:00'
---

By-Elections for Spring 2015 have concluded. The following people were elected:

- Secretary: Keri Warr (`kpwarr`)
- Treasurer: Jonathan Bailey (`jj2baile`)

<!-- -->

In addition, the following people were appointed to non-elected positions:

- Librarian: Yomna Nasser (`ynasser`)
- Office Manager: Ilia Sergei Chtcherbakov (`ischtche`)
- SysAdmin: Nicholas Black (`nablack`)

<!-- -->