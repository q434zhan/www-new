---
author: 'n3parikh'
date: 'January 15 2020 00:00'
---

Winter 2020 elections have concluded. Here are your executives for the term.

- President: Richard Shi (r27shi)
- Vice President: Anastassia Gaikovaia (agaikova)
- Treasurer: Alex Tomala (actomala)
- Secretary/Assistant Vice President: Neil Parikh (n3parikh)
- Sysadmin: Amin Bandali (abandali)

<!-- -->

 The remaining appointed positions are - Office Manager: Alexander Zvorygin (azvorygi)
- Librarian: Anastassia Gaikovaia (agaikova)
- IMAPD: Richard Shi (r27shi)

<!-- -->