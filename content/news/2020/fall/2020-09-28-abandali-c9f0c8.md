---
author: 'abandali'
date: 'September 28 2020 01:00'
---

The CSC Systems Committee announces the availability of the CSC's own installation of the ZNC IRC bouncer for CSC members at [znc.csclub.uwaterloo.ca](<https://znc.csclub.uwaterloo.ca>), as the second of several steps it is taking to bring modern user freedom- and privacy-respecting communication tools to CSC members. Please see the [notice](<https://mailman.csclub.uwaterloo.ca/pipermail/csc-general/2020-September/000840.html>) sent to csc-general for the complete announcement.