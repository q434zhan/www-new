---
author: 'abandali'
date: 'May 28 2020 01:00'
---

Spring 2020 elections have concluded. Here are your executives for the term:

- President: Neil Parikh (n3parikh)
- Vice President: Anastassia Gaikovaia (agaikova)
- Sysadmin: Amin Bandali (abandali)

<!-- -->

The unfilled positions this term are:

- Treasurer
- Secretary/Assistant Vice President
- Office Manager
- Librarian
- IMAPD

<!-- -->