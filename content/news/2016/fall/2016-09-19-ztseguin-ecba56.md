---
author: 'ztseguin'
date: 'September 19 2016 01:00'
---

Here are the results from this term's elections:

- President: s455wang (Charlie Wang)
- Vice-President: b2coutts (Bryan Coutts)
- Treasurer: lhsong (Laura Song)
- Secretary: ubarar (Uday Barar)

<!-- -->

Additionally, the following positions were appointed:

- Systems Administrator: ztseguin (Zachary Seguin)
- Office Manager: j2sinn (James Sinn)
- Librarian: fbauckho (Felix Bauckholt)
- Imapd: ischtche (Ilia Chtcherbakov)

<!-- -->