---
author: 'a3thakra'
date: 'January 16 2019 00:00'
---

Here are the results from this term's elections:

- President: Marc Mailhot (mnmailho)
- Vice-President: Victor Brestoiu (vabresto)
- Treasurer: Tristan Hume (tghume)
- Assistant Vice President: Aditya Thakral (a3thakra)
- Systems Administrator: Charlie Wang (s455wang)

<!-- -->

The rest of the positions will be appointed at a later date.