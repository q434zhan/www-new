---
author: 'j3parker'
date: 'May 26 2009 01:00'
---

Upcoming events: - UNIX 101: Need to learn unix for a course, or just to be a ninja? Swing by MC 2037 on Thursday, June 2nd at 4:30PM for a tutorial session.
- Code Party: Have an assignment or project you need to work on? We will be coding from 7:00pm until 7:00am starting on Friday, June 5th in the Comfy lounge. Join us!

<!-- -->