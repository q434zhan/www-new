---
author: 'ehashman'
date: 'October 01 2013 01:00'
---

A number of talks on security and privacy have been planned for this term. Email [progcom](<mailto:progcom@csclub.uwaterloo.ca>) if you are interested in getting involved. See individual events for more detail.