---
author: 'ehashman'
date: 'October 23 2013 01:00'
---

It's been a while since we last ordered shirts for members. As such, we would like to order a new batch, and invite you to supply the design for the shirt! The winning candidate (and possibly some runner-ups) will receive a free shirt in the size of their choice. Please send your submissions to [the executive](<mailto:exec@csclub.uwaterloo.ca>).

The deadline will be Nov. 13 at midnight, and winners will be announced by Friday, Nov. 15. At that point, a signup sheet will be made available.