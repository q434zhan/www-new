---
author: 'ehashman'
date: 'September 29 2013 01:00'
---

Is there something that you're particularly passionate about and would like to share with others? Do you want to practice presenting on a topic of interest to the membership? We are accepting proposals for member talks for the Fall 2013 term. Tell us about a project you're really passionate about, or perhaps a theoretical problem you find interesting.

Please send a proposed abstract and some possible talk dates to [our programme committee](<mailto:progcom@csclub.uwaterloo.ca>) and we will get to work on getting your talk scheduled.