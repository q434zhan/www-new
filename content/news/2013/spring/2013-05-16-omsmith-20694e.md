---
author: 'omsmith'
date: 'May 16 2013 01:00'
---

Congratulations to this terms new executive!

- President: srcreigh
- Vice-president: vvijayan
- Treasurer: dchlobow
- Secretary: yj7kim

<!-- -->

Additionally, the following people were appointed.

- Sysadmin: a2brenna
- Office Manager: m4burns

<!-- -->

The position of librarian has not yet been filled.