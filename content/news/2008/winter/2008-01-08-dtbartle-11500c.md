---
author: 'dtbartle'
date: 'January 08 2008 00:00'
---

The CSClub mirror ([mirror.csclub.uwaterloo.ca](<http://mirror.csclub.uwaterloo.ca/>)) is now on the ResNet "don't count" list. This means that downloading software from our mirror will not count against your quota. [Click here](<http://wiki.csclub.uwaterloo.ca/Mirror>) for a list of Linux distributions and open-source software that we mirror.