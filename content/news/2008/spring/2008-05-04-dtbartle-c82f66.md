---
author: 'dtbartle'
date: 'May 04 2008 01:00'
---

Spring 2008 elections will be held on Tuesday May 13th at 4:30pm in the Comfy Lounge.

Get your nominations in as soon as possible on the CSC whiteboard!

Nominations close 24 hours before the polls open.

You can email your nominations to cro@csclub.uwaterloo.ca