---
title: CS Club Email
---

Members also receive a **[username@csclub.uwaterloo.ca](#)** email address.

- Mailboxes are considered as part of your disk quota, so your mailbox may grow up to the amount of disk quota you have.
- Attachments of any file size or type may be sent.
- Our mail server runs a POP3, IMAP, and SMTP server with SSL/TLS enabled.

You can also access your mail via a [Roundcube web mail client](https://mail.csclub.uwaterloo.ca/).
