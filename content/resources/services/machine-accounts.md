---
title: Machine Accounts
---

The main benefit of becoming a CS Club member is to get access to our various machines.

- We offer a large range of hardware, including Alpha, MIPS, UltraSPARC, i386, and AMD64. Our primary development machine, high-fructose-corn-syrup, is a 4x AMD Opteron (64 cores at 2.4 GHz) with 64 GiB of RAM, and it easily outperforms the Linux machines available through CSCF.
- Most of our machines are connected via gigabit ethernet. We offer 4 GB of disk quota that is accessible across all of our machines. Our wiki contains a [full machine list](https://wiki.csclub.uwaterloo.ca/wiki/Machine_List).

SSH key fingerprints for caffeine (our main server) can be found below:

```
RSA: 0c:a3:66:52:10:19:7e:d6:9c:96:3f:60:c1:0c:d6:24
ED25519: 9e:a8:11:bb:65:1a:31:23:38:6b:94:9d:83:fd:ba:b1
```

[SSH key fingerprints for other machines](/resources/services/ssh-key-fingerprints)

<button isLink size="small" href="/resources/machine-usage-agreement">Machine Usage Agreement</button>
