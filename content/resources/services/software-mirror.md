---
title: Software Mirror
---

The CSC runs a mirror of popular open source software. The [mirror](http://mirror.csclub.uwaterloo.ca/) has a list of available software. More information is available on our [wiki article](http://wiki.csclub.uwaterloo.ca/Mirror).
