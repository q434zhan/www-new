---
title: Club Web Hosting
---

If you're a club and looking for web space, the CS Club is the place go.

- Clubs have access to the same member services (e.g. disk quota, databases).
- We also offer club.uwaterloo.ca domain registration.

For more details, see the club hosting [wiki page](http://wiki.csclub.uwaterloo.ca/Club_Hosting).
