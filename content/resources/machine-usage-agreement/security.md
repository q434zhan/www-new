---
title: Security
---

Users may not attempt to gain access to accounts other than those which they have been permitted to use. Similarly, users may not attempt to access other users' private files, nor may they attempt to find out the password of any account.

An account may only be used by the person assigned to it. *Do not tell your password to anybody, or let anyone else use your account*. Users should consider the security implications of their actions. For example:

- Passwords for accounts on CSC machines should not be used on other machines
- Accounts not on CSCF or CSC machines should not be granted automatic access to CSC accounts (e.g. via .rhosts files).

The appropriate members of the systems committee must be notified immediately in the event that a security problem is found. Naturally, the problem should neither be exploited nor made public until it can be corrected.
