---
title: Usage Policy
---

Everyone who receives an account on one of the CS Club machines must sign the agreement in the final section. This document does not state who will be allowed accounts on CS Club machines, the normal expiry period of accounts, nor any other similar matters. Further, this policy does not, in general, guarantee any rights to users.

_Note: that in the following sections, the term "user" implies a user of a CS Club machine, unless otherwise specified._

The usage policy is divided into the following sections:

1. [Acceptable and Unacceptable Use](/resources/machine-usage-agreement/acceptable-and-unacceptable-use)
1. [User Responsibilities](/resources/machine-usage-agreement/user-responsibilities)
1. [Security](/resources/machine-usage-agreement/security)
1. [Rights of the Systems Committee and the CSC Executive](/resources/machine-usage-agreement/rights-of-syscom-and-exec)
