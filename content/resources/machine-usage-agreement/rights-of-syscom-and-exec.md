---
title: Rights of the Systems Committee and the CSC Executive
---

The Systems Committee may examine any files or programs believed to be out of control or in violation of the usage policies for the CSC network. Examination of a program includes examination of the running process and its binary. Files believed to be the data or source to the process may also be examined. The process may be killed, stopped or otherwise interrupted at the discretion of the Systems Committee. If the Systems Committee takes any of the above actions, the owner of the process will be notified.

The Systems Committee may at any time revoke a user's permission to access an account provided that a written (possibly electronic) explanation is given. Cause for removal of access to an account includes, but is not limited to, violation of the machine usage policy. In the event of a dispute, a user whose account has been revoked may appeal to the CSC Executive for its reinstatement, as per the [CSC Constitution](/about/constitution).

The Systems Committee may delete a user's CSC Cloud resources when their CSC account expires, at its sole discretion.

The CSC Executive has the right to update any policy, including this one, with reasonable notice.
