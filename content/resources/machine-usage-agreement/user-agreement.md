---
title: User Agreement
---

I have read and understood the CSC Usage Policy of 10 July 2022, and I agree to use my CSC account(s) in accordance with this policy. I am responsible for all actions taken by anyone using this account. Furthermore, I accept full legal responsibility for all of the actions that I commit using the CSC network according to any and all applicable laws.

I understand that with little or no notice machines on the CSC network and resources on these machines may become unavailable. Machines may shut down while users are using them, and I will not hold the CSC responsible for lost time or data.

```
Name:         ___________________________

Signature:    ___________________________

Office Staff: ___________________________

Signature:    ___________________________

Date:         ___________________________
```
