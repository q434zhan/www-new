---
title: Summary
---

This is a brief version of the usage policy. Everyone who receives an account on one of the CS Club machines must sign the full Machine Usage Agreement, and this summary lists the things that the users will agree to.

## Use of accounts

- One person per account.
- Usage intended for personal or course work.
- Don't abuse system resources.
- Use the machines in a respectful manner.

## Security

- Your `.rhosts` file should only contain your user IDs on CSC and CSCF machines.
- Don't use passwords that you use elsewhere, and *never* tell anyone your password.
- If you find security holes, report them to the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca). Intentional malpractice will not be tolerated.

## The Systems Committee may

- Examine programs that seem to be violating policy or security; this includes the following, *when necessary*,
- Remove accounts with short/no notice and provide an reasonable explanation to the user as to why their account was removed.

## General

- You are completely responsible for your actions.
- Don't do anything illegal, damaging, or unethical.
- The executive team can change their policies with reasonable notice.
- CS Club machines will not be up at all times, and may crash while you are using them.
- This document and its rules also apply to CSC Cloud resources.
