## Coop Advice

### Finding a co-op

#### How can I increase my chance of finding a good co-op?

Start looking for a co-op early. Although WaterlooWorks is quite reliable, there are many more opportunities outside of the job board. Being able to apply for jobs externally not only prepares you to look for jobs full time but it also provides a way to start your study term without having to worry about looking for a co-op.

<!-- TODO: ### Doing well on the interview -->

### First few weeks of co-op - Starting off strong so your team gives you more important projects to work on

#### What are some things I can do to better understand the context of my work?

If you have spent time diving into the codebase but you still are confused, schedule time with your mentor/coworkers to have a code base walk through. Write up questions to ask during the meeting and take notes of unclear parts of the code.

#### What are some easy steps to produce cleaner code?

Check over your code at least twice before submitting your code review. Reviewing the code a second time may help you catch minor issues and/or problematic code that your reviewers may or may not comment on. If you are unable to figure out a solution to an issue, then reach out to someone for help rather than implementing a hacky solution. You will be more aware of your coding quality, less prone to ignoring issues, and overall be a more responsible developer.

#### How can I improve the onboarding process?

Document any blockers you faced during onboarding, and how you overcame them because chances are others will face them too. These can be tips/advice you would give new hires. Feel free to share these findings with your team, because they want to make the onboarding process more efficient and up to date for future hires. Some examples of things to take note of are outdated/incorrect/missing documentation and the way the team does a specific task.

### Work life balance?

#### What can I do to feel less stressed from my co-op workday?

Create rituals for starting your day and ending your day. Studies have shown that not having a post work activity makes it harder to not think about work which leads to burn out and reduced productivity. Start your day by thinking about what you want to achieve and how you want to feel. End your day by doing an activity i.e exercising, listing todos for tomorrow, or even reflecting about the work day! This may help you have a more balanced lifestyle.

#### How can I stay on top of my tasks during a workday?

Set up a system to stay on top of your work. This can be as simple as setting up a to-do list ready for the day. The important thing is to be clear and intentional with your goals each day so you can optimize your focus on getting things done.

### Asking for help

#### How can I balance between solving problems on my own and asking for help?

To make the best use of your time, set a time limit on how long you spend on the problem (e.g. 1 hour before you ask for help). Asking for help on an issue you’ve been stuck on for some time can be beneficial. It’s much better to take an hour of your mentor/boss’ time than to be stuck for days without any results. The solution may be team/organization specific and asking can save a lot of time. Be sure to try your best to solve the problem on your own first to maximize your ability to learn.

### Getting feedback

#### How do I ask for feedback during a work term?

Asking for feedback from your manager/mentor throughout the term can go a long way. You can ask about your performance in certain areas and ways you can improve. These feedbacks can help determine what you should continue and/or change. For example, you can ask about their expectations and how you can achieve a specific rating on the employer co-op rating to set up specific goals.

### Performance evaluation

#### How can I ensure that I am satisfying my manager's expectations during a work term?

Around the middle of the term, ask to go over your co-op evaluation form with your manager. In doing so, you will be able to modify your current goals to match/exceed your manager’s expectations. This is especially helpful for you to determine how you can achieve the co-op rating you want.

### Networking

#### How can I network effectively during a coop term?

Meeting and networking with people in and outside your team is an amazing way to learn and meet new people. Coffee chats are a great way to learn about interesting roles and tasks others around the company perform. Try to set up coffee chats with others at your company as you might meet an amazing connection or learn about a really neat topic. This may lead to an idea of what you want to do in your future co-ops. A format you can use is: “Hey, I'm the new intern of [manager] and I was wondering if I could put something on your calendar so I can get to know you and your work a little better.”

### Code related advice

#### How can I efficiently ensure that my code is functional?

Aim to make most/all of your code testable. This will ensure the code is functioning properly and will save time debugging in the future. This is a useful skill to have as a developer.

#### How do I handle push requests on GitHub?

Each push request (PR) should focus on a very specific change/feature. Modularizing the changes will make reviewing the PR easier and quicker.

### Compensation

#### How can I negotiate compensation for an offer when I have other competing offers?

Negotiating compensation for an offer when you already have competing offers can be very beneficial for you and it’s normal to do. For a general guide, you can use the format:

> Hello [Name of recruiter],
>
> I am very interested in working [company name]. I have been given an opportunity at [another company name] that is offering [compensation]. Would it be possible for [the company name] to match/increase the compensation?
>
> Thank you,
>
> [Name]

#### How can I negotiate compensation when I do not have other competing offers?

If you do not have competing offers you can still try to negotiate using the format:

> Hello [Name of recruiter],
>
> Given my experiences, would it be possible to increase the compensation to [compensation]?
>
> Thank you,
>
> [Name]

Either way, it does not hurt to try as the worst they can say is no.
