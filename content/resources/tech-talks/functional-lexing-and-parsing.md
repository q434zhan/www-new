---
index: 39
title: 'Functional Lexing and Parsing'
presentors:
  - Dr. Prabhakar Ragde
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing.mpg'
    type: 'MPG'
---

This talk will describe a non-traditional functional approach to the classical problems of lexing (breaking a stream of characters into "words" or tokens) and parsing (identifying tree structure in a stream of tokens based on a grammar, e.g. for a programming language that needs to be compiled or interpreted). The functional approach can clarify and organize a number of algorithms that tend to be opaque in their conventional imperative presentation. No prior background in functional programming, lexing, or parsing is assumed.

The slides for this talk can be found [here](<http://mirror.csclub.uwaterloo.ca/csclub/pr-functional-lexing-parsing-slides.pdf>) as a pdf.
