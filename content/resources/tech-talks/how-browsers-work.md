---
index: 21
title: 'How Browsers Work'
presentors:
  - Ehsan Akhgari
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/how-browsers-work-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/how-browsers-work-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/how-browsers-work.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/how-browsers-work.mpg'
    type: 'Talk (MPG)'
---

Veteran Mozilla engineer Ehsan Akhgari presents a talk on the internals of web browsers. The material ranges from the fundamentals of content rendering to the latest innovations in browser design.

Web browsers have evolved. From their humble beginnings as simple HTML rendering engines they have grown and evolved into rich application platforms. This talk will start with the fundamentals: how a browser creates an on-screen representation of the resources downloaded from the network. (Boring, right? But we have to start somewhere.) From there we'll get into the really exciting stuff: the latest innovations in Web browsers and how those innovations enable — even encourage — developers to build more complex applications than ever before. You'll see real-world examples of people building technologies on top of these "simple rendering engines" that seemed impossible a short time ago.
