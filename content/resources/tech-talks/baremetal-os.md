---
index: 25
title: 'BareMetal OS'
presentors:
  - Ian Seyler
  - Return to Infinity
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/bare-metal-os-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/bare-metal-os-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bare-metal-os.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bare-metal-os.mpg'
    type: 'Talk (MPG)'
---

BareMetal is a new 64-bit OS for x86-64 based computers. The OS is written entirely in Assembly, while applications can be written in Assembly or C/C++. High Performance Computing is the main target application.
