---
index: 34
title: 'The Best Algorithms are Randomized Algorithms'
presentors:
  - Dr. Nick Harvey
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random.avi'
    type: 'Talk (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random.ogg'
    type: 'Talk (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random.mp4'
    type: 'Talk (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nick-harvey-random.mpg'
    type: 'Talk (MPG)'
---

For many problems, randomized algorithms are either the fastest algorithm or the simplest algorithm; sometimes they even provide the only known algorithm. Randomized algorithms have become so prevalent that deterministic algorithms could be viewed as a curious special case. In this talk I will describe some startling examples of randomized algorithms for solving some optimization problems on graphs.
