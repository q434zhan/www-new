---
index: 2
title: 'ALT-TAB - Manic PXE Dream Servers'
presentors:
  - Fatema Boxwala
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/fatema-manic-pxe-dream-servers-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/fatema-manic-pxe-dream-servers.mp4'
    type: 'Manic PXE Dream Servers (mp4)'
---

PXE stands for Pre-eXecution Environment. Fatema will talk about the motivation for using it, examples of industry uses and a brief overview of what it is and how it works.
