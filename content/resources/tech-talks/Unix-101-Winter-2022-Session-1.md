---
index: 63
title: 'Unix 101 Winter 2022 Session 1'
presentors:
  - CSC Systems Committee 
thumbnails:
  small: 'https://mirror.csclub.uwaterloo.ca/csclub/Unix-101-Winter-2022-Session-1.jpg'
links:
  - file: 'https://mirror.csclub.uwaterloo.ca/csclub/Unix-101-Winter-2022-Session-1.mp4'
    type: 'Talk (mp4)'
---
