---
index: 9
title: 'Starting a VN Indie Game Company as a UW Student'
presentors:
  - Alfe Clemencio
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/indie-game-dev-clemencio-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/indie-game-dev-clemencio.mp4'
    type: 'Talk (x264)'
---

Many people want to make games as signified by all the game development schools that are appearing everywhere. But how would you do it as a UW student? This talk shares the experiences of how making Sakura River Interactive was founded without any Angel/VC investment.

The talk will start off with inspiration drawn of Co-op Japan, to it's beginnings at Velocity. Then a reflection of how various game development and business skills was obtained in the unexpected ways at UW will follow. How the application of probabilities, theory of computation, physical/psychological attraction theories was used in the development of the company's first game. Finally how various Computer Science theories helped evaluate feasibility of several potential incoming business deals.

[From Sakura River interactive](<http://www.sakurariver.ca/>)
