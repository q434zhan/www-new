---
index: 18
title: 'In Pursuit of the Travelling Salesman'
presentors:
  - Bill Cook
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/bico_2014_travelling_salesman-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bico_2014_travelling_salesman.mp4'
    type: 'Talk (x264)'
---

The traveling salesman problem is easy to state: given a number of cities along with the cost of travel between each pair of them, find the cheapest way to visit them all and return to your starting point. Easy to state, but difficult to solve. Despite decades of research, in general it is not known how to significantly improve upon simple brute-force checking. It is a real possibility that there may never exist an efficient method that is guaranteed to solve every instance of the problem. This is a deep mathematical question: Is there an efficient solution method or not? The topic goes to the core of complexity theory concerning the limits of feasible computation and we may be far from seeing its resolution. This is not to say, however, that the research community has thus far come away empty-handed. Indeed, the problem has led to a large number of results and conjectures that are both beautiful and deep, and on the practical side solution methods are used to compute optimal or near-optimal tours for a host of applied problems on a daily basis, from genome sequencing to arranging music on iPods. In this talk we discuss the history, applications, and computation of this fascinating problem.
