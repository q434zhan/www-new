---
index: 10
title: 'Cory Doctorow - The War on General Purpose Computing'
presentors:
  - Cory Doctorow
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/cory-doctorow-f2015-thumb-small.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cory-doctorow-f2015.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cory-doctorow-f2015-hq.mp4'
    type: 'Talk (x246 Big File)'
---

No Matter Who's Winning the War on General Purpose Computing, You're Losing

If cyberwar were a hockey game, it'd be the end of the first period and the score would be tied 500-500. All offense, no defense.

Meanwhile, a horrible convergence has occurred as everyone from car manufacturers to insulin pump makers have adopted the inkjet printer business model, insisting that only their authorized partners can make consumables, software and replacement parts -- with the side-effect of making it a felony to report showstopper, potentially fatal bugs in technology that we live and die by.

And then there's the FBI and the UK's David Cameron, who've joined in with the NSA and GCHQ in insisting that everyone must be vulnerable to Chinese spies and identity thieves and pervert voyeurs so that the spy agencies will always be able to spy on everyone and everything, everywhere.

It's been fifteen years since the copyright wars kicked off, and we're still treating the Internet as a glorified video-on-demand service -- when we're not treating it as a more perfect pornography distribution system, or a jihadi recruitment tool.

It's all of those -- and more. Because it's the nervous system of the 21st century. We've got to stop treating it like a political football.

(Cory Doctorow is affiliated with the Canadian branch of EFF, the [Electronic Frontier Foundation](<https://www.eff.org/>))
