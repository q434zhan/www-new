---
index: 50
title: 'The Free Software Movement and GNULinux Operating System, a talk by Richard Stallman at UCSD'
presentors:
  - Richard M. Stallman
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rms_ucsd.ogg'
    type: 'Ogg Theora'
    size: '148MB'
---

Richard Stallman will speak about the goals and philosophy of the Free Software Movement, and the status and history the GNU Operating System, which in combination with the kernel Linux is now used by tens of millions of users world-wide.

Richard Stallman launched the development of the GNU operating system in 1984. GNU is free software: everyone has the freedom to copy it and redistribute it, as well as to make changes either large or small. The GNU/Linux system, basically the GNU operating system with Linux added, is used on tens of millions of computers today.

"The reason I care especially, is that there is a philosophy associated with the GNU project, and this philosophy is actually the reason why there is a system -- and that is that free software is not just convenient and not just reliable.... More important than convenience and reliability is freedom -- the freedom to cooperate. What I'm concerned about is not individual people or companies so much as the kind of way of life that we have. That's why I think it's a distraction to think about fighting Microsoft."

**Biography:** Stallman has received the ACM Grace Hopper Award, a MacArthur Foundation fellowship, the Electronic Frontier Foundation's Pioneer award, and the Takeda Award for Social/Economic Betterment, as well as several honorary doctorates.

The Question and Answer session (starting shortly after the hour and half mark) posed a number of interesting questions including, "Do you support the Creative Commons license?" and "Can I use ATI and NVIDIA drivers because Mesa isn't nearly as complete?".



 The talk is only available in Ogg Theora, in keeping with Richard Stallman's wishes.
