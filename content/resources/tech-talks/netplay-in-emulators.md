---
index: 0
title: 'Netplay in Emulators'
presentors:
  - Gregor Richards
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/gregor-talk-thumb.png'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/gregor-talk-thumb.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/gregor-talk.mp4'
    type: 'Netplay in Emulators (mp4)'
---

You've got a game, but you didn't write it. You're running it by emulating the machine it was meant to run on, and the machine it was meant to run on never had support for networking. Now, you want to play with your friend, over the Internet. Oh, and it's not acceptable to incur any latency between your controller and the game while we're at it. Surely that can't be possible, right? Wrong. This talk will discuss the re-emulation technique for netplay used commercially by a system called GGPO and freely in an emulator frontend called RetroArch, and how similar techniques can be applied to make networking work in other scenarios it was never meant for. This will be an unprepared, impromptu talk with no slides, so it should either be a fascinating dive into a little-heard-of technique, or an impenetrable mess of jargon and algorithms. Either way, it should be fun. Professor Richards is the maintainer of the netplay infrastructure for RetroArch, a popular emulator frontend for multiple platforms.
