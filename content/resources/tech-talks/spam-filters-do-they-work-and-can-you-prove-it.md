---
index: 56
title: 'Spam Filters: Do they work and Can you prove it'
presentors:
  - Dr. Gord Cormack
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam-xvid.avi'
    type: 'XviD'
    size: '473M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam.avi'
    type: 'DiVX'
    size: '473M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam.mpg'
    type: 'MPG'
    size: '472M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cormack-spam.ogg'
    type: 'Ogg/Theora'
    size: '481M'
---

Do spam filters work? Which is the best one? How might filters be improved? Without standards, one must depend on unreliable evidence, such as subjective impressions, testimonials, incomparable and unrepeatable measurements, and vendor claims for the answers to these questions.

You might think that your spam filter works well and couldn't be improved. Are you sure? You may think that the risk of losing important mail outweighs the benefit of using a filter. Could you convince someone who holds the other opinion? If I told you that my filter was 99-percent accurate, would you believe me? Would you know what I meant? Would you be able to translate that 99-percent into the risk of losing an important message?

Gord Cormack talks about the science, logistics, and politics of Spam Filter Evaluation.
