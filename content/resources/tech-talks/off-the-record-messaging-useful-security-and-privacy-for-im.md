---
index: 43
title: 'Off-the-Record Messaging: Useful Security and Privacy for IM'
presentors:
  - Ian Goldberg
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ian-goldberg-otr.mpg'
    type: 'MPG'
---

Instant messaging (IM) is an increasingly popular mode of communication on the Internet. Although it is used for personal and private conversations, it is not at all a private medium. Not only are all of the messages unencrypted and unauthenticated, but they are all routedthrough a central server, forming a convenient interception point for an attacker. Users would benefit from being able to have truly private conversations over IM, combining the features of encryption, authentication, deniability, and forward secrecy, while working within their existing IM infrastructure.

In this talk, I will discuss "Off-the-Record Messaging" (OTR), a widely used software tool for secure and private instant messaging. I will outline the properties of Useful Security and Privacy Technologies that motivated OTR's design, compare it to other IM security mechanisms, and talk about its ongoing development directions.
