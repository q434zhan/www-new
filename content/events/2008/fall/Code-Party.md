---
name: 'Code Party'
short: 'A fevered night of code, friends, fun, free energy drinks, and the CSC.'
startDate: 'October 24 2008 18:00'
online: false
location: 'Comfy Lounge'
---

Come join us for a night of coding. Get in touch with more experianced coders, advertize for/bug squash on your favourite open source project, write that personal project you were planning to do for a while but haven't found the time. Don't have any ideas but want to sit and hack? We can find something for you to do.

