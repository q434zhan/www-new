---
name: 'Linux Install Fest'
short: 'Come join the CSC in celebrating the new releases of Ubuntu Linux, Free BSD and Open BSD, and get a hand installing one of them on your own system.'
startDate: 'November 06 2008 10:00'
online: false
location: 'SLC Multipurpose Room'
---

Come join the CSC in celebrating the new releases of	Ubuntu Linux, Free BSD and Open BSD, and get a hand installing	one of them on your own system.

This is an event to celebrate the releases of new	versions of Ubuntu Linux, OpenBSD, and FreeBSD. CDs will be	available and everyone is invited to bring their PC or laptop	to get help installing any of these Free operating	systems. Knowledgeable CSC members will be available to help	with any installation troubles, or to troubleshooting any	existing problems that users may have.

This event will also promote gaming on Linux, as well as	FLOSS (Free/Libre and Open Source Software) in general. We may	also have a special guest (Ian Darwin, of OpenBSD and OpenMoko	fame).

