---
name: 'Game Sketching'
short: 'Juancho Buchanan, CTO Relic Entertainment'
startDate: 'October 03 2008 16:30'
online: false
location: 'MC2065'
---

In this talk I will give an overview of the history of Relic and our development philosophy. The Talk will then proceed to talk about work that is being pursued in the area of early game prototyping with the introduction of game sketching methodology.

Bio: Fired from his first job for playing Video Games Juancho Buchanan is currently the director of Technology for Relic Entertainment. Juancho Buchanan Wrote his first game in 1984 but then pursued other interests which included a master's in Program Visualization, A Doctorate in Computer Graphics, a stint as a professor at the University of Alberta where he pioneered early work in Non photo realistic rendering, A stint at Electronic Arts as Director, Advanced Technology, A stint at EA as the University Liaison Dude, A stint at Carnegie Mellon University where he researched the Game Sketching idea. His current role at Relic has him working with the soon to be released Dawn of War II.

