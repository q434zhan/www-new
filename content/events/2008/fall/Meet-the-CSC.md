---
name: 'Meet the CSC'
short: 'Come out and meet other CSC members, find out about the CSC, meet the executive nominees, and join if you like what you see. Nominees should plan on attending.'
startDate: 'September 12 2008 16:30'
online: false
location: 'Comfy Lounge'
---

Come out and meet other CSC members, find out about the CSC, meet the executive nominees, and join if you like what you see. Nominees should plan on attending.

