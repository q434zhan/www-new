---
name: 'General Meeting 2'
short: 'The second official general meeting of the term. Items on the adgenda are CSC Merch, upcoming talks, and other possible planned events, as well as the announcement of a librarian and planning of an office cleanout and a library organization day.'
startDate: 'October 02 2008 16:30'
online: false
location: 'MC4021'
---

The second official general meeting of the term. Items on the adgenda are CSC Merch, upcoming talks, and other possible planned events, as well as the announcement of a librarian and planning of an office cleanout and a library organization day.

