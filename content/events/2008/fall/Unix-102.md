---
name: 'Unix 102'
short: 'Want more from Unix? No problem, we''ll teach you to create and quickly edit high quality documents.'
startDate: 'October 09 2008 16:30'
online: false
location: 'MC2037'
---

This is a follow up to Unix 101, requiring basic knowledge of the shell. If you missed Unix101 but still know your way around you should be fine. Topics covered include: "real" editors, document typesetting with LaTeX (great for assignments!), bulk editing, spellchecking, and printing in the student environment and elsewhere. If you aren't interested or feel comfortable with these taskes, watch out for Unix 103 and 104 to get more depth in power programming tools on Unix. If you don't think you're ready go to Unix 101 on Tuesday to get familiarized with the shell environment.

