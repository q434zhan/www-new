---
name: 'CSC Programming Contest 1'
short: 'Yes, we know this is Valentine''s Day.'
startDate: 'February 14 2008 16:00'
online: false
location: 'MC2061'
---

Contestants will be writing an artificial intelligence to play Risk. The prize will be awarded to the intelligence which wins the most head-to-head matches against competing entries. We're providing easy APIs for several languages, as well as full documentation of the game protocol so contestants can write wrappers for any additional language they wish to work in.

We officially support entries in Scheme, Perl, Java, C, and C++. If you would like help developing an API for some other language contact us through the systems committee mailing list (we will require that your API is made available to all entrants).

To kick off the contest we're hosting an in-house coding session starting at 4:00PM on Thursday, February 14th in MC2061. Members of our contest administration team will be available to help you work out the details of our APIs, answer questions, and provide the necessities of life (ie, pizza). Submissions will open no later than 5:00PM on February 14th and will close no earlier than 12:00PM on February 17th.

Visit our contest site [ here!](<http://csclub.uwaterloo.ca/contest/>)

