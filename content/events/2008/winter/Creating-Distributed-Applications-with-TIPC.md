---
name: 'Creating Distributed Applications with TIPC'
short: 'Elmer Horvath'
startDate: 'January 23 2008 17:00'
online: false
location: 'MC 4020'
---

The problem: coordinating and communicating between multiple processors in a distributed system (possibly containing heterogeneous elements)

The open source TIPC (transparent interprocess communication) protocol has been incorporated into the Linux kernel and is available in VxWorks and, soon, other OSes. This emerging protocol has a number of advantages in a clustered environment to simplify application development while maintaining a familiar socket programming interface. The service oriented capabilities of TIPC help in applications easily finding required services in a system. The location transparent aspect of TIPC allows services to be located anywhere in the system as well as allowing redundant services for both load reduction and backup. Learn about the emerging cluster protocol.

