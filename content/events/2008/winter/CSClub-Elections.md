---
name: 'CSClub Elections'
short: 'Elections are scheduled for Tues, Jan 15 @ 4:30 pm in the comfy lounge. The nomination period closes on Mon, Jan 14 @ 4:30 pm. Candidates should not engage in campaigning after the nomination period has closed.'
startDate: 'January 15 2008 16:30'
online: false
location: 'Comfy Lounge'
---

Elections are scheduled for Tues, Jan 15 @ 4:30 pm in the comfy lounge. The nomination period closes on Mon, Jan 14 @ 4:30 pm. Candidates should not engage in campaigning after the nomination period has closed.

