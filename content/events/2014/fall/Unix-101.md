---
name: 'Unix 101'
short: 'Interested in Unix, but don''t know where to start? Then Come learn some basic topics with us including interaction with the shell, motivation for using it, some simple commands, and more.'
startDate: 'October 24 2014 17:00'
online: false
location: 'MC 3003'
---

Interested in Unix, but don't know where to start? Then Come learn some basic topics with us including interaction with the shell, motivation for using it, some simple commands, and more.

