---
name: 'Code Party 1'
short: 'We will be having our 2nd code party this term. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science.'
startDate: 'March 14 2014 19:00'
online: false
location: 'Comfy Lounge'
---

We will be having our 2nd code party this term. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science.

