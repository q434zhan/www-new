---
name: 'UNIX 101'
short: 'Learn the basics of using tools found commonly on UNIX-like operating systems. For students new to this topic, knowledge gained from UNIX 101 would be useful in coursework.'
startDate: 'February 13 2014 17:30'
online: false
location: 'MC 3003'
---

Learn the basics of using tools found commonly on UNIX-like operating systems. For students new to this topic, knowledge gained from UNIX 101 would be useful in coursework.

