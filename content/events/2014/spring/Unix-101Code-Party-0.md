---
name: 'Unix 101/Code Party 0'
short: 'Interested in Unix, but don''t know where to start? Then Come learn some basic topics with us including interaction with the shell, motivation for using it, some simple commands, and more.Afterwards we will be moving over to the MC Comfy Lounge for a fun night of hacking! The sysadmin position will also be ratified during a general meeting of the membership at this time. Come join us for an evening of fun, learning, and food!'
startDate: 'May 30 2014 17:30'
online: false
location: 'MC 3003, Comfy Lounge'
---

Interested in Unix, but don't know where to start? Then start in MC 3003 on Friday May 30 with basic topics including interaction with the shell, motivation for using it, some simple commands, and more.

Afterwards we will be moving over to the MC Comfy Lounge for a fun night of hacking! Work on a personal project, open source software, or anything you wish. Food will be available for your hacking pleasure. The Sysadmin position will also be ratified during a general meeting at this time. Come join us for an evening of fun, learning, and food!

