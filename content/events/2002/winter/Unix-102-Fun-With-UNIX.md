---
name: 'Unix 102: Fun With UNIX'
short: 'This the second in a series of UNIX tutorials. Simon Law and James Perry will be presenting some more advanced UNIX techniques. All are welcome. Accounts will be provided for those needing them.'
startDate: 'February 07 2002 18:00'
online: false
location: 'MC2037'
---

This is the second in a series of seminars that cover the use of the UNIX Operating System. UNIX is used in a variety of applications, both in academia and industry. We will provide you with hands-on experience with the Math Faculty's UNIX environment in this tutorial.

Topics that will be discussed include:

- Interacting with Bourne and C shells
- Editing text using the vi text editor
- Editing text using the Emacs display editor
- Multi-tasking and the screen multiplexer

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class.

