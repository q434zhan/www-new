---
name: 'UNIX 101: First Steps With UNIX'
short: ' This is the first in a series of seminars that cover the use of the UNIX Operating System. UNIX is used in a variety of applications, both in academia and industry. We will be covering the basics of the UNIX environment, as well as the use of PINE, an electronic mail and news reader. '
startDate: 'January 31 2002 18:00'
online: false
location: 'MC2037'
---

 This is the first in a series of seminars that cover the use of the UNIX Operating System. UNIX is used in a variety of applications, both in academia and industry. We will be covering the basics of the UNIX environment, as well as the use of PINE, an electronic mail and news reader. 