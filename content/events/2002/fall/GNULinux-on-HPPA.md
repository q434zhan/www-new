---
name: 'GNU/Linux on HPPA'
short: 'Carlos O''Donnell talks about "the last of the legacy processors to fall before the barbarian horde"'
startDate: 'October 26 2002 13:30'
online: false
location: 'MC2066'
---

This whirlwind talk is aimed at providing an overview of the GNU/Linux port for the HP PARISC processor. The talk will focus on the "intricacies" of the processor, and in particular the implementations of the Linux kernel and GNU Libc. After the talk you should be acutely aware of how little code needs to be written to support a new architecture! Carlos has been working on the port for two years, and enjoying the fruits of his labour on a 46-node PARISC cluster.

---

Carlos is currently in his 5th year of study at the University of Western Ontario. This is his last year in a concurrent Computer Engineering and Computer Science degree. His research interest range from distributed and parallel systems to low level optimized hardware design. He likes playing guitar and just bought a Cort NTL-20, jumbo body, solid spruce top with a mahogany back. Carlos hacks on the PARISC Linux kernel, GNU libc, GNU Debugger, GNU Binutils and various Debian packages.

