---
name: 'Trip to York University'
short: 'Going to visit the York University Computer Club'
startDate: 'November 16 2002 13:30'
online: false
location: 'York University'
---

YUCC and the UW CSC have having a join meeting at York University. Dave Makalsky, the President of YUCC, will be giving a talk on Design-by-contract and Eiffel. Stefanus Du Toit, Vice-President of the UW CSC, will be giving a talk on the evil depths of the black art known as C++.

Schedule

- 1:30pm: Leave UW
- 3:00pm: Arrive at York University.
- 3:30pm: The Evil side of C++
- 4:30pm: Design-by-Contract and Eiffel
- 6:00pm: Dinner
- 9:00pm: Arrive back at UW

<!-- -->

