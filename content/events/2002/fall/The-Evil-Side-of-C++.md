---
name: 'The Evil Side of C++'
short: 'Abusing template metaprogramming in C++; aka. writing a Mandelbrot generator that runs at compile time'
startDate: 'November 05 2002 16:30'
online: false
location: 'MC 2065'
---

Templates are a useful feature in C++ when it comes to writing type-independent data structures and algorithms. Relatively soon after their appearance it was realised that they could be used to do much more than this. Essentially it is possible to write certain programs in C++ that execute *completely at compile time* rather than run time. Combined with constant-expression optimisation this is an interesting twist on regular C++ programming.

This talk will give a short overview of the features of templates and then go on to describe how to "abuse" templates to perform complex computations at compile time. The speaker will present three programs of increasing complexity which execute at compile time. First a factorial listing program, then a prime listing program will be presented. Finally the talk will conclude with the presentation of a **Mandelbrot generator running at compile time**.

If you are interested in programming for the fun of it, the C++ language or silly tricks to do with languages, this talk is for you. No C++ knowledge should be necessary to enjoy this talk, but programming experience will make it more worthwile for you.

