---
name: 'Samba and You'
short: 'A talk by Dan Brovkovich, Mathsoc''s Computing Director'
startDate: 'November 21 2002 16:30'
online: false
location: 'MC2066'
---

Samba is a free implementation of the Server Message Block (SMB) protocol. It also implements the Common Internet File System (CIFS) protocol, used by Microsoft Windows 95/98/ME/2000/XP to share files and printers.

SMB was originally developed in the early to mid-80's by IBM and was further improved by Microsoft, Intel, SCO, Network Appliances, Digital and many others over a period of 15 years. It has now morphed into CIFS, a form strongly influenced by Microsoft.

Samba is considered to be one of the key projects for the acceptance of GNU/Linux and other Free operating systems (e.g. FreeBSD) in the corporate world: a traditional Windows NT/2000 stronghold.

We will talk about interfacing Samba servers and desktops with the Windows world. From a simple GNU/Linux desktop in your home to the corporate server that provides collaborative file/printer sharing, logons and home directories to hundreds of users a day.

