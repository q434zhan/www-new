---
name: 'UNIX 101'
short: 'First Steps with UNIX'
startDate: 'September 26 2002 17:30'
online: false
location: 'MC3006'
---

Get to know UNIX and be the envy of your friends!

This is the first in a series of seminars that cover the use	of the UNIX Operating System. UNIX is used in a variety of	applications, both in academia and industry. We will provide	you with hands-on experience with the Math Faculty's UNIX	environment in this seminar.

Topics that will be discussed include:

- Navigating the UNIX environment
- Using common UNIX commands
- Using the PICO text editor
- Reading electronic mail and news with PINE

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class.

