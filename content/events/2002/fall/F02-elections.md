---
name: 'F02 elections'
short: 'Come and vote for this term''s exec'
startDate: 'September 16 2002 17:30'
online: false
location: 'Comfy lounge'
---

Vote for the exec this term. Meet at the comfy lounge. There will be an opportunity to obtain or renew memberships. This term's CRO is Siyan Li (s8li@csclub.uwaterloo.ca).

