---
name: 'C++0x - An Overview'
short: 'Bjarne Stroustrup'
startDate: 'July 17 2007 19:00'
online: false
location: 'AL 116'
---

A good programming language is far more than a simple collection of features. My ideal is to provide a set of facilities that smoothly work together to support design and programming styles of a generality beyond my imagination. Here, I briefly outline rules of thumb (guidelines, principles) that are being applied in the design of C++0x. Then, I present the state of the standards process (we are aiming for C++09) and give examples of a few of the proposals such as concepts, generalized initialization, being considered in the ISO C++ standards committee. Since there are far more proposals than could be presented in an hour, I'll take questions.

