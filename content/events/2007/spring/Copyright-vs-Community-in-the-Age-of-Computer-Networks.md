---
name: 'Copyright vs Community in the Age of Computer Networks'
short: 'Richard Stallman'
startDate: 'July 06 2007 16:30'
online: false
location: 'AL 116'
---

Copyright developed in the age of the printing press, and was designed to fit with the system of centralized copying imposed by the printing press. But the copyright system does not fit well with computer networks, and only draconian punishments can enforce it.

The global corporations that profit from copyright are lobbying for draconian punishments, and to increase their copyright powers, while suppressing public access to technology. But if we seriously hope to serve the only legitimate purpose of copyright--to promote progress, for the benefit of the public--then we must make changes in the other direction.

The CSC would like to thank MEF and Mathsoc for funding this talk.

[The Freedom Software Foundation's description](<http://www.fsf.org/events/waterloo20070706>)


[FSF's anti-DRM campaign](<http://www.defectivebydesign.org>)


[Why you shouldn't use Microsoft Vista](<http://www.badvista.org>)


[The GNU's Not Unix Project](<http://www.gnu.org>)


