---
name: 'UW Software Start-ups: What Worked and What Did Not'
short: 'A talk by Larry Smith'
startDate: 'February 08 2007 16:30'
online: false
location: 'MC 2066'
---

A discussion of software start-ups founded by UW students and what they did that helped them grow and what failed to help. In order to share the most insights and guard the confidences of the individuals involved, none of the companies will be identified.

