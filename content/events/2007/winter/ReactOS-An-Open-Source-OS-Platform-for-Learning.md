---
name: 'ReactOS: An Open Source OS Platform for Learning'
short: 'A talk by Alex Ionescu'
startDate: 'February 26 2007 16:30'
online: false
location: 'DC 1350'
---

The ReactOS operating system has been in development for over eight years and aims to provide users with a fully functional and Windows-compatible distribution under the GPL license. ReactOS comes with its own Windows 2003-based kernel and system utilities and applications, resulting in an environment identical to Windows, both visually and internally.

More than just an alternative to Windows, ReactOS is a powerful platform for academia, allowing students to learn a variety of skills useful to software testing, development and management, as well as providing a rich and clean implementation of Windows NT, with a kernel compatible to published internals book on the subject.

This talk will introduce the ReactOS project, as well as the various software engineering challenges behind it. The building platform and development philosophies and utilities will be shown, and attendees will grasp the vast amount of effort and organization that needs to go into building an operating system or any other similarly large project. The speaker will gladly answer questions related to his background, experience and interests and information on joining the project, as well as any other related information.

**Speaker Bio**

Alex Ionescu is currently studying in Software Engineering at Concordia University in Montreal, Quebec and is a Microsoft Technical Student Ambassador. He is the lead kernel developer of the ReactOS Project and project leader of TinyKRNL. He regularly speaks at Linux and Open Source conferences around the world and will be a lecturer at the 8th International Free Software Forum in Brazil this April, as well as providing hands-on workshops and lectures on Windows NT internals and security to various companies.

