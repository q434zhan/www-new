---
name: 'Network Security -- Intrusion Detection'
short: 'A talk by Reg Quinton'
startDate: 'January 31 2007 16:00'
online: false
location: 'MC 4041'
---

IST monitors the campus network for vulnerabilities and scans systems for security problems. This informal presentation will look behind the scenes to show the strategies and technologies used and to show the problem magnitude. We will review the IST Security web site with an emphasis on these pages

[http://ist.uwaterloo.ca/security/vulnerable/](<http://ist.uwaterloo.ca/security/vulnerable/>)


[http://ist.uwaterloo.ca/security/security-wg/reports/20061101.html](<http://ist.uwaterloo.ca/security/security-wg/reports/20061101.html>)


[http://ist.uwaterloo.ca/security/position/20050524/](<http://ist.uwaterloo.ca/security/position/20050524/>)


