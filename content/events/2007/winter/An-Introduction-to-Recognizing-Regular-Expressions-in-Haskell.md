---
name: 'An Introduction to Recognizing Regular Expressions in Haskell'
short: 'A talk by James deBoer'
startDate: 'February 15 2007 16:30'
online: false
location: 'MC 2065'
---

This talk will introduce the Haskell programming language and and walk through building a recognizer for regular languages. The talk will include a quick overview of regular expressions, an introduction to Haskell and finally a line by line analysis of a regular language recognizer.

