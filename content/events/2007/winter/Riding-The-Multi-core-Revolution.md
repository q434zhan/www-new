---
name: 'Riding The Multi-core Revolution'
short: 'How a Waterloo software company is changing the way people program computers. A talk by Stefanus Du Toit'
startDate: 'February 07 2007 16:30'
online: false
location: 'MC 4041'
---

For decades, mainstream parallel processing has been thought of as inevitable. Up until recent years, however, improvements in manufacturing processes and increases in clock speed have provided software with free Moore's Law-scale performance improvements on traditional single-core CPUs. As per-core CPU speed increases have slowed to a halt, processor vendors are embracing parallelism by multiplying the number of cores on CPUs, following what Graphics Processing Unit (GPU) vendors have been doing for years. The Multi- core revolution promises to provide unparalleled increases in performance, but it comes with a catch: traditional serial programming methods are not at all suited to programming these processors and methods such as multi-threading are cumbersome and rarely scale beyond a few cores. Learn how, with hundreds of cores in desktop computers on the horizon, a local software company is looking to revolutionize the way software is written to deliver on the promise multi-core holds.

Refreshments (and possible pizza!) will be provided.

