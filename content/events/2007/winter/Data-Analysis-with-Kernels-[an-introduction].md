---
name: 'Data Analysis with Kernels: [an introduction]'
short: 'A talk by Michael Biggs. This talk is RESCHEDULED due to unexpected circumstances'
startDate: 'April 04 2007 16:00'
online: false
location: 'MC 1056'
---

I am going to take an intuitive, CS-style approach to a discussion about the use of kernels in modern data analysis. This approach often lends us efficient ways to consider a dataset under various choices of inner product, which is roughly comparable to a measure of "similarity". Many new tools in AI arise from kernel methods, such as the infamous Support Vector Machines for classification, and kernel-PCA for nonlinear dimensionality reduction. I will attempt to highlight, and provide visualization for some of the math involved in these methods while keeping the material at an accessible, undergraduate level.

