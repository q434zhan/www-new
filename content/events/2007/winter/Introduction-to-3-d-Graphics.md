---
name: 'Introduction to 3-d Graphics'
short: 'A talk by Chris "The Prof" Evensen'
startDate: 'February 09 2007 16:30'
online: false
location: 'MC 4041'
---

A talk for those interested in 3-dimensional graphics but unsure of where to start. Covers the basic math and theory behind projecting 3-dimensional polygons on screen, as well as simple cropping techniques to improve efficiency. Translation and rotation of polygons will also be discussed.

