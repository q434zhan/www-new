---
name: 'Putting the fun into Functional Languages and Useful Programming with OCaml/F#'
short: 'Brennan Taylor'
startDate: 'October 02 2007 16:30'
online: false
location: 'MC4061'
---

A lecture on why functional languages are important, practical applications, and some neat examples. Starting with an introduction to basic functional programming with ML syntax, continuing with the strengths of OCaml and F#, followed by some exciting examples. Examples include GUI programming with F#, Web Crawlers with F#, and OpenGL/GTK programming with OCaml. This lecture aims to display how powerful functional languages can be.

