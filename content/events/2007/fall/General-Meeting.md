---
name: 'General Meeting'
short: 'There is a general meeting scheduled for Friday, October 19, 2007 at 17:00.This is a chance to bring out any ideas and concerns about CSC happenings into the open, as well as a chance to make sure all CSC staff is up to speed on current CSC doings. The current agenda can be found at [http://wiki.csclub.uwaterloo.ca/wiki/Friday\_19\_October\_2007.](<http://wiki.csclub.uwaterloo.ca/wiki/Friday_19_October_2007>)'
startDate: 'October 19 2007 17:00'
online: false
location: 'MC4058'
---

There is a general meeting scheduled for Friday, October 19, 2007 at 17:00.

This is a chance to bring out any ideas and concerns about CSC happenings into the open, as well as a chance to make sure all CSC staff is up to speed on current CSC doings. The current agenda can be found at [http://wiki.csclub.uwaterloo.ca/wiki/Friday\_19\_October\_2007.](<http://wiki.csclub.uwaterloo.ca/wiki/Friday_19_October_2007>)

