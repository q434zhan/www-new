---
name: 'More Haskell functional programming fun!'
short: 'Andrei Barbu'
startDate: 'November 22 2007 16:30'
online: false
location: 'MC 4041'
---

 Haskell is a modern lazy, strongly typed functional language with type inferrence. This talk will focus on multiple monads, existential types, lambda expressions, infix operators and more. Along the way we'll see a parser and interpreter for lambda calculus using monadic parsers. STM, software transactional memory, a new approach to concurrency, will also be discussed. Before the end we'll also see the solution to an ACM problem to get a hands on feeling for the language. Don't worry if you haven't seen the first talk, you should be fine for this one anyway! 