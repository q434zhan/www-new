---
name: 'Programming Contest'
short: 'Win Prizes!'
startDate: 'December 01 2007 13:30'
online: false
location: 'MC 2037'
---

The Computer Science club is holding a programming contest from 1:00 to 6:30 open to all! C++,C,Perl,Scheme are allowed.	Prizes totalling in value of $75 will be distributed. You can participate online! For more information, including source files visit [http://www.csclub.uwaterloo.ca/contest](<http://www.csclub.uwaterloo.ca/contest>)

And Free Pizzaa for all who attend!

