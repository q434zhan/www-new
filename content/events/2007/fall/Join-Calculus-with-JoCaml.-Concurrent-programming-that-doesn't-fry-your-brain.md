---
name: 'Join-Calculus with JoCaml. Concurrent programming that doesn''t fry your brain'
short: 'Brennan Taylor'
startDate: 'October 09 2007 16:45'
online: false
location: 'MC 4060'
---

A lecture on the fundamentals of Pi-Calculus followed by an introduction to Join-Calculus in JoCaml with some great examples. Various concurrent control structures are explored, as well as the current limitations of JoCaml. The examples section will mostly be concurrent programming, however some basic distributed examples will be explored. This lecture focuses on how easy concurrent programming can be.

