---
name: 'CSC and WiCS Go Outside'
short: 'The CSC and WiCS (Women in Computer Science) are co-hosting a social event on Thursday, October 13th (the day after reading week). We will be Going Outside to the Columbia Lake 2 Fire Pit; there will be a campfire, s''mores, lots of food, frisbees, grass, etc. Bring your friends!'
startDate: 'October 13 2016 18:00'
online: false
location: 'Columbia Lake 2 Fire Pit'
---

The CSC and WiCS (Women in Computer Science) are co-hosting a social event on Thursday, October 13th (the day after reading week). We will be Going Outside to the Columbia Lake 2 Fire Pit (see [map](<https://uwaterloo.ca/economics/sites/ca.economics/files/uploads/files/firepit_map_oct_2012.pdf>)). There will be a campfire, s'mores, lots of food, frisbees, grass, etc. Bring your friends!

