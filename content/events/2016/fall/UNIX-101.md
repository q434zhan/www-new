---
name: 'UNIX 101'
short: 'The CSC is having its next event, UNIX 101, on Wednesday November 9th at 6 PM in MC 3003 (the mac lab across from the CSC). UNIX 101 is a tutorial where we teach the basics of using a command-line (terminal) environment in UNIX. Knowing how to use the command-line and UNIX is an invaluable skill in CS, and helps prepare you for future projects and co-ops.'
startDate: 'November 09 2016 18:00'
online: false
location: 'MC 3003'
---

The CSC is having its next event, UNIX 101, on Wednesday November 9th at 6 PM in MC 3003 (the mac lab across from the CSC). UNIX 101 is a tutorial where we teach the basics of using a command-line (terminal) environment in UNIX. Knowing how to use the command-line and UNIX is an invaluable skill in CS, and helps prepare you for future projects and co-ops.

