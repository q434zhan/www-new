---
name: 'General Meeting'
short: 'This general meeting will be held to discuss changes to our Code of Conduct.'
startDate: 'November 16 2016 20:30'
online: false
location: 'M3 1006'
---

The Code of Conduct and the amended version can be found below:

- [Proposed CoC](<https://www.csclub.uwaterloo.ca/~exec/proposed-amendment/about/code-of-conduct>)
- [Diff between current and proposed CoC](<https://www.csclub.uwaterloo.ca/~exec/proposed-amendment.patch>)

<!-- -->

