---
name: 'Steve Bourque and Mike Patterson Network Infrastructure talk'
short: 'Steve Bourque and Mike Patterson of IST will give a brief overview of campus network connectivity and interconnectivity.'
startDate: 'March 09 2016 18:30'
online: false
location: 'MC4058'
---

Steve Bourque and Mike Patterson of IST will give a brief overview of campus network connectivity and interconnectivity. Steve will describe the general connections, and Mike will talk about specific security measures in place. We'll have refreshments!

