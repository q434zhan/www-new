---
name: 'Tea and Study'
short: 'It''s midterms season, and everyone has to study. So why not come study with the CS Club? Everyone welcome, especially new members! There will be tea and delicious snacks and outlets. Plus our delightful company. See you there!'
startDate: 'March 03 2016 18:00'
online: false
location: 'MC Comfy'
---

Come join CSC at our Tea and Study event! Everyone welcome, especially new members! There will be tea and delicious snacks and outlets. Plus our delightful company.

