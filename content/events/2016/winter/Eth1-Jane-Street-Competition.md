---
name: 'Eth1: Jane Street Competition'
short: 'eth1: a day-long programming contest. Form teams and hack together a trading bot to compete against others and the markets.'
startDate: 'January 23 2016 11:00'
online: false
location: 'TBA'
---

eth1: a day-long programming contest. Form teams and hack together a trading bot to compete against others and the markets.

Brought to you by: CSC and Jane Street.

Each member of the winning team will receive $1000 USD.

There'll be lots of (free) food and drink available.

Absolutely no special math, OCaml, or finance knowledge is required; you can use any language you like. The contest is entirely technical in nature and you won't need any visual design skills.

The exact details of the hackathon aren't released until the competition begins. The one thing you can do ahead of time to prepare is familiarize yourself with the libraries for writing TCP clients in your programming language of choice.

[Sign up!](<https://docs.google.com/a/janestreet.com/forms/d/1I7UukJDH9ZAVWpLl-2vwmvPWzbWBFjj8g973hidn8eE/viewform>)

The contest will be on Saturday, January 23rd, from 11:00AM - 11:00PM. Signups will close on Monday, January 18th at 11:59PM, and we'll send out confirmations to participants on the 20th.

For any other queries, email: eth1-waterloo@janestreet.com

Further details will be announced closer to the event. Teams of up to three will be accepted, but you don't have to have a team to sign up — feel free to turn up as a singleton and we'll form teams on the fly.

