---
name: 'On Surrounding a Polygon'
short: 'Come out to a talk on surrounding a polygon by Dr. Craig Kaplan! Also stay afterwards for our EOT and socialize :) food will be provided!'
startDate: 'April 04 2016 17:00'
online: false
location: 'MC Comfy'
---

Come out to a talk on surrounding a polygon by Dr. Craig Kaplan! Also stay afterwards for our EOT and socialize :) food will be provided! The prof talk will be on Surrounding a Polygon: Dr. Craig Kaplan will explore the problem of surrounding a polygon with copies of itself. This problem raises a number of fascinating mathematical questions, and we can use software as an experimental tool to probe the answers to those questions.



 He'll also present known mathematical and computational results related to surrounds of polygons, and discuss what they say about larger open questions in tiling theory. Finally, he will also show how the task of surrounding individual polygons can make for fun and challenging puzzles, and say a bit about his experience creating an app based on those puzzles. 