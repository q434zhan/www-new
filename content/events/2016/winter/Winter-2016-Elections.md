---
name: 'Winter 2016 Elections'
short: 'Interested in Linux, but don''t know where to start? Come learn some basic topics with us including interaction with the shell, motivation for using it, some simple commands, and more! (Cookies after)'
startDate: 'January 14 2016 19:00'
online: false
location: 'MC 3001 (Comfy)'
---

The Computer Science Club will be holding elections for the Winter 2016 term on Thursday, January 14th in MC Comfy (MC 3001) at 19:00. During the meeting, the president, vice-president, treasurer and secretary will be elected, the sysadmin will be ratified, and the librarian and office manager will be appointed.

If you'd like to run for any of these positions or nominate someone, you can write your name on the whiteboard in the CSC office (MC 3036/3037) or send me (Charlie) an email at cro@csclub.uwaterloo.ca. Every effort will be made to note down whiteboard nominations, but it is highly recommended to send me an email in addition to writing on the whiteboard. You can also deposit nominations in the CSC mailbox in MathSoc or present them to me in person. Nominations will close at 18:00 on Wednesday, January 13th. All members are welcome to run!

