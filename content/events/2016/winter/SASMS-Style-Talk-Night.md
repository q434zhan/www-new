---
name: 'SASMS Style Talk Night'
short: 'The CSC is hosting the first annual member talk series! This is a chance for anyone to come and give a short talk on any relevant topic. Some talks already arranged are on topics ranging from modern Javascript, to the Linux Kernel.'
startDate: 'March 29 2016 18:00'
online: false
location: 'MC 4021'
---

The CSC is hosting the first annual member talk series. This is a chance for anyone to come and give a short talk on any relevant topic. We already have some talks arranged, on topics ranging from modern JavaScript, and the Linux Kernel. More speakers are welcome. If you are interested, please email tbelaire@uwaterloo.ca or signup here:http://goo.gl/forms/zNYbDEQSFU There will be a break for food halfway through.

