---
name: 'Richard Mann Prof Talk and EOT'
short: 'Join us on Monday, July 25th at 6pm in the MC Comfy Lounge for an	exciting prof talk by Richard Mann on Open Source Computer Sound	Measurement. The abstract for the talk is below. We will follow	this up by an EOT event with dinner and board games!	Last event of the term, get hype.'
startDate: 'July 25 2016 18:00'
online: false
location: 'MC Comfy Lounge'
---

An ideal computer audio system should faithfully reproduce signals of all frequencies in the audible range (20 to 20,000 cycles per second). Real systems, particularly mobile devices and laptops, may still produce acceptable quality, but often have a limited response, particularly at the low (bass) frequencies. Sound/acousic energy refers to time varying pressure waves in air. When recording sound, the acoustic signal will be picked up by microphone, which converts it to electrical signals (voltages). The signal is then digitized (analog to digital conversion) and stored as a stream of numbers in a data file. On playback the digital signal is converted to an electrical signal (digital to analog conversion) and finally returned as an acoustic signal by a speaker and/or headphones. In this talk I will present open source software (Octave/Linux) to measure the end-to-end frequency response of an audio system using the Discrete Fourier Transform. I will demonstrate the software using a standard USB audio interface and a consumer grade omnidirectional microphone. This is joint work with John Vanderkooy, Distinguished Professor Emeritus, Department of Physics and Astronomy.

