---
name: 'scp talks'
short: 'Listen to cool 15-20 lightning talks by CSC members on a variety of computer science and related topics.'
startDate: 'June 08 2016 18:00'
online: false
location: 'MC 5479'
---

Come on out to the CSC Short Contemplation Period Talk night on Wednesday, featuring many short (20 minute) talks from our members. From Automata to Zip files, any topic is welcome. Come on out and give a talk, or just learn things. Talks start at 6:00PM and runs till 9, with a break for dinner, which will be provided.

