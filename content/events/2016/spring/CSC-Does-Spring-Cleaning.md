---
name: 'CSC Does Spring Cleaning'
short: 'Come out and help make the office slightly less messy! We will bribe... uh, provide you with food for helping. :)'
startDate: 'May 25 2016 18:00'
online: false
location: 'MC 3036 (CSC Office)'
---

It's that time of the year - spring cleaning. And if you haven't noticed, our office needs it. Help us clean it and we will give you food to eat. Pretty good deal if you ask me.

Our office manager will also be providing office training to interested members before the event.

