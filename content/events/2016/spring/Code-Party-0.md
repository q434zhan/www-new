---
name: 'Code Party 0'
short: 'Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food.'
startDate: 'June 02 2016 18:30'
online: false
location: 'STC 0010'
---

Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food.

