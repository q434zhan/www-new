---
name: 'UNIX 101 and 102'
short: 'Continuing the popular Unix Tutorials with a rerun of 101 and the debut of 102.'
startDate: 'February 05 2009 17:30'
online: false
location: 'MC2062 and MC2063'
---

Unix 101 is an introduction to the Unix shell environment, both on the student servers and on other Unix environments. Topics covered include: using the shell, both basic interaction and advanced topics like scripting and job control, the filesystem and manipulating it, and ssh.

Unix 102 is a follow up to Unix 101, requiring basic knowledge of the shell. If you missed Unix101 but still know your way around you should be fine. Topics covered include: "real" editors, document typesetting with LaTeX (great for assignments!), bulk editing, spellchecking, and printing in the student environment and elsewhere.

If you aren't interested or feel comfortable with these taskes, watch out for Unix 103 and 104 to get more depth in power programming tools on Unix.

