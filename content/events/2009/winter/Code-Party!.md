---
name: 'Code Party!'
short: 'There is a CSC Code Party starting at 7:00PM (19:00). Come out and enjoy some good old programming and meet others interested in writing code! Free energy drinks and snacks for all. Plus, we have lots of things that need to be done if you''re looking for a project to work on!'
startDate: 'February 06 2009 19:00'
online: false
location: 'Comfy Lounge'
---

Code Party. Awesome. Need we say more?

