---
name: 'Joel Spolsky'
short: 'Joel Spolsky, of [Joel on Software](<http://www.joelonsoftware.com>) will be giving a talk entitled "Computer Science Education and the Software Industry".'
startDate: 'January 22 2009 12:00'
online: false
location: 'MC5136'
---

**About Joel Spolsky:** Joel Spolsky is a globally-recognized expert on the software development process. His website *Joel on Software* ([www.joelonsoftware.com](<http://www.joelonsoftware.com/>)) is popular with software developers around the world and has been translated into over thirty languages. As the founder of [Fog Creek Software](<http://www.fogcreek.com/>) in New York City, he created [FogBugz](<http://www.fogcreek.com/FogBugz>), a popular project management system for software teams. He is the co-creator of [Stack Overflow](<http://stackoverflow.com/>), a programmer Q&A site. Joel has worked at Microsoft, where he designed VBA as a member of the Excel team, and at Juno Online Services, developing an Internet client used by millions. He has written [four books](<http://www.joelonsoftware.com/BuytheBooks.html>): *User Interface Design for Programmers* (Apress, 2001), *Joel on Software* (Apress, 2004), *More Joel on Software *(Apress, 2008), and *Smart and Gets Things Done: Joel Spolsky's Concise Guide to Finding the Best Technical Talent *(Apress, 2007). He also writes a monthly column for** ***[Inc Magazine](<http://www.inc.com/>). *Joel holds a BS from Yale in Computer Science. Before college he served in the Israeli Defense Forces as a paratrooper, and he was one of the founders of Kibbutz Hanaton.

