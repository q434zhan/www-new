---
name: 'Artificial Intelligence Contest'
short: 'Come out and try your hand at writing a computer program that	plays Minesweeper Flags, a two-player variant of the classic	computer game, Minesweeper. Once you''re done, your program	will compete head-to-head against the other entries in a	fierce Minesweeper Flags tournament. There will be a contest	kick-off session on Thursday March 19 at 4:30 PM in room	MC3036. Submissions will be accepted until Saturday March 28.'
startDate: 'March 19 2009 16:30'
online: false
location: 'MC2061'
---

Come out and try your hand at writing a computer program that	plays Minesweeper Flags, a two-player variant of the classic	computer game, Minesweeper. Once you're done, your program	will compete head-to-head against the other entries in a	fierce Minesweeper Flags tournament. There will be a contest	kick-off session on Thursday March 19 at 4:30 PM in room	MC3036. Submissions will be accepted until Saturday March 28.

