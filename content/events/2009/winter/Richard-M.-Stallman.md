---
name: 'Richard M. Stallman'
short: 'The Free Software Movement and the GNU/Linux Operating System'
startDate: 'January 29 2009 18:30'
online: false
location: 'Modern Languages Theatre'
---

Richard Stallman will speak about the Free Software Movement, which campaigns for freedom so that computer users can cooperate to control their own computing activities. The Free Software Movement developed the GNU operating system, often erroneously referred to as Linux, specifically to establish these freedoms.

**About Richard Stallman:** Richard Stallman launched the development of the GNU operating system (see [www.gnu.org](<http://www.gnu.org>)) in 1984. GNU is free software: everyone has the freedom to copy it and redistribute it, as well as to make changes either large or small. The GNU/Linux system, basically the GNU operating system with Linux added, is used on tens of millions of computers today. Stallman has received the ACM Grace Hopper Award, a MacArthur Foundation fellowship, the Electronic Frontier Foundation's Pioneer award, and the the Takeda Award for Social/Economic Betterment, as well as several honorary doctorates.

