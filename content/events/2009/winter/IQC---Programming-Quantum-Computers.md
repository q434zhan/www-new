---
name: 'IQC - Programming Quantum Computers'
short: 'A brief intro to Quantum Computing and why it matters,	followed by a talk on programming quantum computers. Meet at	the CSC at 4:00PM for a guided walk to the RAC.'
startDate: 'March 12 2009 17:00'
online: false
location: 'RAC2009'
---

Raymond Laflamme is the director of the Institute for Quantum	Computing at the University of Waterloo and holds the Canada	Research Chair in Quantum Information. He will give a brief	introduction to quantum computing and why it matters, followed	by a talk on programming quantum computers. There will be	tours of the IQC labs at the end, and pizza will be provided	back at the CSC for all attendees.

