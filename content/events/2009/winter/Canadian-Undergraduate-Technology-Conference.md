---
name: 'Canadian Undergraduate Technology Conference'
short: 'See [cutc.ca](<http://www.cutc.ca>) for more details.'
startDate: 'March 12 2009 08:00'
online: false
location: 'Toronto Hilton'
---

The Canadian Undergraduate Technology Conference is Canada's largest student-run conference. From humble roots it has emerged as a venue that offers an environment for students to grow socially, academically, and professionally. We target to exceed our past record of 600 students from 47 respected institutions nationwide. The event mingles ambitious as well as talented students with leaders from academia and industry to offer memorable experiences and valuable opportunities.

