---
name: 'History of CS Curriculum at UW'
short: 'This talk provides a personal overview of the evolution of the undergraduate computer science curriculum at UW over the past forty years, concluding with an audience discussion of possible future developments.'
startDate: 'July 07 2009 15:00'
online: false
location: 'DC 1302'
---

This talk provides a personal overview of the evolution of the undergraduate computer science curriculum at UW over the past forty years, concluding with an audience discussion of possible future developments.

