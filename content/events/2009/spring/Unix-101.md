---
name: 'Unix 101'
short: 'Need to use the UNIX environment for a course, want to overcome your fears of the command line, or just curious? Come and learn the arcane secrets of the UNIX command line interface from CSC mages. After this tutorial you will be comfortable with the essentials of navigating, manipulating and viewing files, and processing data at the UNIX shell prompt.'
startDate: 'June 02 2009 16:30'
online: false
location: 'MC 2037'
---

Need to use the UNIX environment for a course, want to overcome your fears of the command line, or just curious? Come and learn the arcane secrets of the UNIX command line interface from CSC mages. After this tutorial you will be comfortable with the essentials of navigating, manipulating and viewing files, and processing data at the UNIX shell prompt.

