---
name: 'AI Programming Contest sponsored by Google'
short: 'Come learn how to write an intelligent game-playing program. No past experience necessary. Submit your program using the [online web interface](<http://csclub.uwaterloo.ca/contest/>) to watch it battle against other people''s programs. Beginners and experts welcome! Prizes provided by google, including the delivery of your resume to google recruiters.'
startDate: 'September 28 2009 16:30'
online: false
location: 'MC3003'
---

Come learn how to write an intelligent game-playing program. No past experience necessary. Submit your program using the [online web interface](<http://csclub.uwaterloo.ca/contest/>) to watch it battle against other people's programs. Beginners and experts welcome!

The contest is sponsored by Google, so be sure to compete for a chance to get noticed by them.

Prizes for the top programs:

- $100 in Cash Prizes
-  Google t-shirts
- Fame and recognition
- Your resume directly to a Google recruiter

<!-- -->

