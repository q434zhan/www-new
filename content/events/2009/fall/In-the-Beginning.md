---
name: 'In the Beginning'
short: 'To most CS students an OS kernel is pretty low level. But there is something even lower, the instructions that must be executed to get the CPU ready to accept a kernel. That is, if you look at any processor''s reference manual there is a page or two describing the state of the CPU when it powered on. This talk describes what needs to happen next, up to the point where the first kernel instruction executes.'
startDate: 'November 05 2009 16:30'
online: false
location: 'MC2065'
---

To most CS students an OS kernel is pretty low level. But there is something even lower, the instructions that must be executed to get the CPU ready to accept a kernel. That is, if you look at any processor's reference manual there is a page or two describing the state of the CPU when it powered on. This talk describes what needs to happen next, up to the point where the first kernel instruction executes.

This part of execution is extremely architecture-dependent. Those of you who have any experience with this aspect of CS probably know the x86 architecture, and think it's horrible, which it is. I am going to talk about the ARM architecture, which is inside almost all mobile phones, and which allows us to look at a simple implementation that includes all the essentials.

