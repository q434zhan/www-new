---
name: 'Elections'
short: 'Nominations are open now, either place your name on the nominees board or e-mail [the CRO](<mailto:cro@csclub.uwaterloo.ca>) to nominate someone for a position. Come to the Comfy Lounge to elect your fall term executive. Contact [the CRO](<mailto:cro@csclub.uwaterloo.ca>) if you have questions.'
startDate: 'September 15 2009 17:00'
online: false
location: 'Comfy Lounge'
---

Nominations are open now, either place your name on the nominees board or e-mail [the CRO](<mailto:cro@csclub.uwaterloo.ca>) to nominate someone for a position. Come to the Comfy Lounge to elect your fall term executive. Contact [the CRO](<mailto:cro@csclub.uwaterloo.ca>) if you have questions.

