---
name: 'UNIX 103'
short: 'In this long-awaited third installment of the popular Unix Tutorials the friendly experts of the CSC will teach you the simple art of version control. You will learn the purpose and use of two different Version Control Systems (git and subversion). This tutorial will advise you in the discipline of managing the source code of your projects and enable you to quickly learn new Version Control Systems in the work place -- a skill that is much sought after by employers.'
startDate: 'October 08 2009 16:30'
online: false
location: 'MC3003'
---

In this long-awaited third installment of the popular Unix Tutorials the friendly experts of the CSC will teach you the simple art of version control. You will learn the purpose and use of two different Version Control Systems (git and subversion). This tutorial will advise you in the discipline of managing the source code of your projects and enable you to quickly learn new Version Control Systems in the work place -- a skill that is much sought after by employers.

