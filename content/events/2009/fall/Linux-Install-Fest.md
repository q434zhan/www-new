---
name: 'Linux Install Fest'
short: 'Interested in trying Linux but don''t know where to start? Come to the Linux install fest to demo Linux, get help installing it on your computer, either stand alone or a dual boot, and help setting up your fresh install. Have lunch and hang around if you like, or just come in for a CD.'
startDate: 'October 03 2009 10:00'
online: false
location: 'DC1301 FishBowl'
---

Interested in trying Linux but don't know where to start? Come to the Linux install fest to demo Linux, get help installing it on your computer, either stand alone or a dual boot, and help setting up your fresh install. Have lunch and hang around if you like, or just come in for a qick install.

