---
name: 'Code Party!!11!!'
short: 'A fevered night of code, friends, fun, energy drinks, and the CSC. Facebook will be around to bring some food and hang out.'
startDate: 'November 27 2009 19:00'
online: false
location: 'Comfy Lounge'
---

Come join us for a night of coding. Get in touch with more experianced coders, advertize for/bug squash on your favourite open source project, write that personal project you were planning to do for a while but haven't found the time. Don't have any ideas but want to sit and hack? Try your hand at the Facebook puzzles, write a new app, or just chill and watch scifi.

