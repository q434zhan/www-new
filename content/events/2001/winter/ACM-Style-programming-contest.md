---
name: 'ACM-Style programming contest'
short: 'Practice for the ACM international programming contest'
startDate: 'January 27 2001 10:30'
online: false
location: 'MC3006'
---

Our ACM-Style practice contests involve answering five questions in three hours. Solutions are written in Pascal, C or C++. Seven years in a row, Waterloo's teams have been in the top ten at the world finals. For more information, see [the contest web page](<http://plg.uwaterloo.ca/~acm00/>).

### Easy Question:

A palindrome is a sequence of letters that reads the same backwards and forwards, such as \`\`Madam, I'm Adam'' (note that case doesn't matter and only letters are important). Your task is to find the longest palindrome in a line of text (if there is a tie, print the leftmost one).

<pre>Input:                              Output:

asfgjh12dsfgg kj0ab12321BA wdDwkj   abBA
abcbabCdcbaqwerewq                  abCdcba
</pre>

### Hard Question:

An anagram is a word formed by reordering the letters of another word. Find all sets of anagrams that exist within a large dictionary. The input will be a sorted list of words (up to 4000 words), one per line. Output each set of anagrams on a separate line. Each set should be in alphabetical order, and all lines of sets should be in alphabetical order. A word with no anagrams is a set of anagrams itself, and should be displayed with no modifications.

<pre>Input:      Output:

post        post pots stop
pots        start
start
stop
</pre>

