---
name: 'Meeting #2'
short: 'Second CSC meeting for Winter 2001.'
startDate: 'January 22 2001 15:30'
online: false
location: 'MC3036'
---

### Proposed agenda

<dl><dt>Book purchases</dt><dd><p>They haven't been done in 2 terms. We have an old list of books to buy. Any suggestions from uw.csc are welcome.</p></dd><dt>CD Burner</dt><dd><p>For doing Linux burns. It was allocated money on the budget request - about $300. We should be able to get a decent 12x burner with that (8x rewrite).</p><p>The obvious things to sell are Linux Distros and BSD variants. Are there any other software that we can legally burn and sell to students?</p></dd><dt>Unix talks</dt><dd><p>Just a talk of the topics to be covered, when, where, whatnot. Mike was right on this one, this should have been done earlier in the term. Oh well, maybe we can fix this for next fall term.</p></dd><dt>Game Contest</dt><dd><p>We already put a bit of work into planning the Othello contest before I read Mike's post. I still think it's viable. I've got at least 2 people interested in writing entries for it. This will be talked about more on Monday. Hopefully, Rory and I will be able to present a basic outline of how the contest is going to be run at that time.</p></dd><dt>Peri's closet cleaning</dt><dd><p>Current sysadmin (jmbeverl) and I (kvijayan) and President (geduggan) had a nice conversation about this 2 days ago, having to do with completely erasing all of peri, installing a clean stable potato debian on it, and priming it for being a gradual replacement to calum. We'll probably discuss how much we want to get done on this front on Monday.</p></dd></dl>

Any [comments](<nntp://news.math.uwaterloo.ca/uw.csc/8305>) from [the newsgroup](<news:uw.csc>) are welcome.

