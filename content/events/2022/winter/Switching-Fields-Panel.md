---
name: 'Switching Fields Panel'
short: 'Listen in as our amazing panelists discuss their experiences with shifting career fields when searching for co-op.'
startDate: 'March 11 2022 18:00'
online: true
location: 'Zoom'
poster: 'images/events/2022/winter/Switching-Fields-Panel.png'
registerLink: https://bit.ly/3HazgpU
---

Want to move away from web development for your next co-op? Need advice on a general career switch? CSC is proud to be hosting our Switching Fields Panel! Listen in as our amazing panelists discuss their experiences with shifting career fields when searching for co-op.

Registration is not required to attend but we’ll be sending you an email reminder if you register and inviting you to the calendar event!

📅 Event Date: Firday March 11th, 6-7pm EST on Zoom

👉 Register using this link: https://bit.ly/3HazgpU

Hope to see you at the event! 🤗
