---
name: "CSC Circles Kick-Off & Valentine's Day Speed-Friending"
short: "Attend to get details about CSC Circles, a new initiative to match participants in groups of 4-6 to create recurring social circles. There will also be a Valentine's Day themed Speed-Friending event after the info session."
startDate: 'February 12 2022 16:00'
online: true
location: 'Zoom'
poster: 'images/events/2022/winter/CSC-Circles-Kick-Off.png'
registerLink: https://bit.ly/csc-circles-kickoff-signup
---

📢 This term, CSC will be kickstarting a NEW event called CSC Circles! We’ll be matching participants in groups of 4-6 to create recurring social circles based on your interests 🎮, availability ⏰, location 📍, and more!

👀 Members of CSC Circles will be a part of a vibrant community within CSC, in which you have the chance to build meaningful connections over the course of a semester and hopefully beyond 🚀 !

📌 The details of the program will be discussed during our kickoff event, so if you’re interested in participating, be sure to attend! You’ll have the chance to ask questions, meet the coordinators, and participate in a Valentine’s Day themed Speed-Friending event after the info session is done 💗.

📅 Event Date: February 12th, 2022 at 4:00 PM EST online. Sign up for the kickoff event at https://bit.ly/csc-circles-kickoff-signup.

👉 Sign up for CSC Circles at https://bit.ly/csc-circles-signup by February 17th, 2022 at 11:59 PM EST. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.
