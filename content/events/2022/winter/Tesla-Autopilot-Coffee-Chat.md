---
name: "Tesla Autopilot and Tesla Bot Coffee Chat"
short: "Join 3 special events with Tesla engineers and learn more about internships and new grad roles in AI, Autopilot, and Vehicle software!"
startDate: 'March 9 2022 19:00'
online: true
location: 'Online'
poster: 'images/events/2022/winter/Tesla-Autopilot-Coffee-Chat.png'
registerLink: https://forms.office.com/Pages/ResponsePage.aspx?id=9MUmkNCGn0u9ObfU0PtGdG9a0mm6j6BFvW5CeQngudlUOUczS1ZWQkJTRTVTQzA5MlA1N005UjVaTy4u
---

🎉 It's that time of year again! CSC is bringing back our partnership with Tesla for multiple coffee chat events!

🚘 Join 3 special events with Tesla engineers and learn more about internships and new grad roles in AI, Autopilot, and Vehicle software!

The **Autopilot and Tesla Bot** coffee chat will be on March 9th from 7-8PM EST. All students interested in autonomy, robotics, embedded systems and AI/ML are welcome to join!

✨ A private application link will be added for each coffee chat per team to sign up for internships for Fall 2022!

🍴 All attendees will also have a chance to win a $25 Uber Eats gift card!

👉 Sign up at: https://forms.office.com/Pages/ResponsePage.aspx?id=9MUmkNCGn0u9ObfU0PtGdG9a0mm6j6BFvW5CeQngudlUOUczS1ZWQkJTRTVTQzA5MlA1N005UjVaTy4u
