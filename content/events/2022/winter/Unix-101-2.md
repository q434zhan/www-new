---
name: "Unix 101: FS/Perms & vim"
short: "Join CSC's Systems Committee (syscom) for the second session of an introductory series on GNU/Linux!"
startDate: "March 26 2022 14:00"
endstartDate: "March 26 2022 15:00"
online: true
location: "Twitch"
poster: "images/events/2022/winter/Unix-101-2.png"
---

Do you want to start editing with vim? Looking to learn about the Unix file system? You’ve come to the right place! ✍ CSC’s Systems Committee (syscom) is back with a workshop on File Systems, Permissions, and a tutorial on vim for our 2nd session of Unix 101! 💻

🙆‍♂️ Unix 101 is a 4-part, beginner-friendly series on the Unix operating system. Attendance in previous sessions is not required to understand the proceeding ones, but is strongly encouraged to make sure you get the most out of our workshops! ✨

👆 We additionally recommend joining CSC to access our machines for more hands-on activity. Learn more about how you can do so at https://csclub.uwaterloo.ca/get-involved/!

🧠 Head over to our Twitch on March 26th from 2-3PM ET for our second session of Unix 101!

📅 Event Date: Saturday March 26th @ 2-3PM ET on Twitch
🔗 Twitch: https://www.twitch.tv/uwcsclub
