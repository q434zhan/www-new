---
name: 'CSC Circles Kickoff'
short: 'Come to CSC Circles this term! You can meet new people and create recurring social circles based on your interest!'
startDate: 'October 6 2022 20:00'
endDate: 'October 6 2022 22:00'
online: false
location: 'DC 1351'
poster: 'images/events/2022/fall/CSC-Circles-Kickoff.jpg'
registerLink: 'https://forms.gle/ucTjXd1GN8Gn4PLr6'
---

📢 CSC will be kickstarting one of our most anticipated events: CSC Circles! We’ll be matching participants in groups of 4-6 to create recurring social circles based on your interests 🎮, availability ⏰, location 📍, and more!

👀 Members of CSC Circles will be a part of a vibrant community within CSC, in which you have the chance to build meaningful connections over the course of a semester and hopefully beyond 🚀 !

📌 If you sign up, your group will be revealed during our kickoff event, so if you’re interested in participating, be sure to sign up and attend! You’ll have the chance to ask questions, meet the coordinators, and socialize with your group for the first time. There will also be food 🍕
and boardgames 🎲!

📆 When? Oct 6th 2022 at 8-10pm EST, in DC 1351.


❗ Sign ups are due on September 30th, 2022. 

👉 Register at  https://forms.gle/ucTjXd1GN8Gn4PLr6 . Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up.
