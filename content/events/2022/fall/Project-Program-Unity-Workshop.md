---
name: 'Project Program Unity Workshop'
short: 'Join us to learn more about Unity, a 2D & 3D game engine that has been around since 2005!'
startDate: 'November 15 2022 19:00'
endDate: 'November 15 2022 21:00'
online: false
location: 'QNC 2502'
poster: 'images/events/2022/fall/Project-Program-Unity-Workshop.png'
---
🎉 We are organizing a Unity workshop for Project Program! 

⌨️ What is Unity? Unity is a 2D & 3D game engine that has been around since 2005. You’ll be able to learn more about it if you haven’t heard of it before!

👉 In addition, you'll be learning about how to create create a game like Rocket League!!

🗓️ Event date: November 15th

🕛 Time: 7PM to 9PM.

📍 Location: QNC 2502

Registration is not required for this event. Hope to see you there!