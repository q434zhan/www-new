---
name: 'CSC Beginning of Term Kickoff!'
short: 'Kick off the fall term with CSC’s BOT event and meet others in the CS community!'
startDate: 'September 13 2022 20:00'
endDate: 'September 13 2022 22:00'
online: false
location: 'AHS EXP 1689'
poster: 'images/events/2022/fall/BOT.png'
registerLink: https://forms.gle/Y48k2p8ZX4JPcALx5
---

📢 Kick off the fall term 🍂with CSC’s BOT event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Come to our first event of this term!

🎉Come join us for a night of fun games, arts and crafts, and a WILD goose chase! You’ll also be able to learn more about what CSC has planned for the year!

📆 When? September 13th 2022 at 8:00 - 10:00pm EST, in AHS EXP 1689

👉 Register from this link: https://forms.gle/Y48k2p8ZX4JPcALx5