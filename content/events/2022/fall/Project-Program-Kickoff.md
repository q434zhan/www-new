---
name: 'Project Program Kickoff'
short: 'Project Program is back for Fall 2022, and we’re excited to see mentors support you to create a month-long project!'
startDate: 'September 28 2022 19:30'
endDate: 'September 28 2022 20:30'
online: false
location: 'STC 0020'
poster: 'images/events/2022/fall/Project-Program-Kickoff.png'
registerLink: 'https://forms.gle/jQrntPiNUi1CdwRj6'
---

📢 Project Program is back for Fall 2022, and we’re excited to see mentors support you to create a month-long project!

UW DSC, UW CSC, and Laurier CS are collaborating to help you create your side project by guiding your group of mentees through brainstorming project ideas, creating roadmaps with milestones and achievements, and finally presenting your project for the chance to win prizes! 🏆

📌 The details of the program will be discussed during this event, so if you’re interested in participating, be sure to attend!

📅 Event Date: Wednesday, September 28th from ~~6:00-7:00pm~~ 7:30-8:30pm EDT in STC 0020. 

👉  Register at https://forms.gle/jQrntPiNUi1CdwRj6 . Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.