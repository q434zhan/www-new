---
name: 'Rico Mariani - Ask Me Anything'
short: 'Want to hear from a software engineer at Meta and a former CSC president at Waterloo? Join us in CSC’s Ask Me Anything where former CSC president Rico Mariani is coming in and taking questions!'
startDate: 'October 17 2022 16:00'
endDate: 'October 17 2022 18:00'
online: false
location: 'MC 4045'
poster: 'images/events/2022/fall/Rico-Ask-Me-Anything.png'
---

📢 Want to hear from a software engineer at Meta and a former CSC president at Waterloo? Join us in CSC’s Ask Me Anything where former CSC president Rico Mariani is coming in and taking questions! 🤩 You will have the chance to hear the perspective of an alumni, as well as a CSC member from a different generation!

📆 Event Date: Oct 17, 2022 from 4-6 PM

📌 Location: MC 4045

See you then! 👋