---
name: "Prof talk with Prof Shi Cao"
short: "Join us for an informative talk about cognitive modelling, Al and human behavior."
startDate: "November 17 2022 17:00"
endDate: "November 17 2022 18:30"
online: false
location: "MC 5501"
poster: "images/events/2022/fall/Prof-Talk-Shi-Cao.png"
registerLink: "https://forms.gle/Rbptj4KhvK7dTSSH6e"
---
Prof. Shi Cao is giving an informative and interactive talk about cognitive modelling and how AI can predict human behavior. He will be talking about his research and the current state of the field.

Food will be served after the event. Come join to learn more about this field or simply learn something new! 💯

📍 Location: MC 5501

📅 Event Date: Thursday, Nov. 17th, 5:00PM - 6:30PM
