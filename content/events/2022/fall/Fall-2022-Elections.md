---
name: 'Fall 2022 Elections and General Meeting'
short: 'CS Club will be holding elections for the Fall 2022 term on Monday, September 12th at 7:00 pm in MC 2017.'
startDate: 'September 12 2022 19:00'
online: false
location: 'MC 2017'
poster: 'images/events/2022/fall/F22-Elections-And-General-Meeting.png'
---

🗳 The CS Club will be holding elections for the Fall 2022 term on Monday, September 12 at 7:00PM in MC 2017.

👉 Come to learn more about CSC, sign up for membership, and vote on our new execs! The president, vice-president, treasurer, and assistant vice-president will be elected, and the sysadmin will be appointed.

❓If you have any questions about elections, please email cro@csclub.uwaterloo.ca.