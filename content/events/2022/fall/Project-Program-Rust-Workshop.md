---
name: 'Project Program Rust Workshop'
short: 'Join us to learn more about Rust, a new multi-paradigm, general-purpose programming language!'
startDate: 'November 17 2022 19:00'
endDate: 'November 17 2022 21:00'
online: false
location: 'PHY 150'
poster: 'images/events/2022/fall/Project-Program-Rust-Workshop.png'
---
🎉 We are organizing a Rust workshop for Project Program! 

⌨️ What is Rust? Rust is a new multi-paradigm, general-purpose programming language. You’ll be able to learn more about it if you haven’t heard of it before! 

👉 In addition, you'll be learning about how to create a command line interface and will be able to create a command line interface program by the end of the workshop.

📅 Date: November 17th, from 7PM to 9PM

📍 Location: PHY 150 and also available remote on Zoom. The zoom link will be provided on the day on our social media and our discord. 