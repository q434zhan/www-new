---
name: 'Capture The Flag Contest'
short: 'The CTF Club in association with CSC is running a Capture The Flag contest! '
startDate: 'July 15 2022 18:00'
endDate: 'July 17 2022 12:00'
online: true
location: 'Online'
poster: 'images/events/2022/spring/Ctf.png'
registerLink: 'https://ctf.uwaterloo.ca/s22'
---
👀 Looking for a challenge that may win you money?
🚩 The CTF Club in association with CSC is running a Capture The Flag contest this term! Participants will work individually or in teams of up to 3 (division-dependent) to solve information security (InfoSec), penetration testing (pentesting) and cryptography challenges. 

🤩 There is a total of $110 of prize money to be won! Winners from the noob category can win $20 and the pros can win up to $90. 

🗓 This contest will be held virtually from July 15th to 17th, so be sure to sign up from https://ctf.uwaterloo.ca/s22 !