---
name: 'BOT: In-person Speed-Friending + CSC Circles Intro'
short: 'Kick off the spring term with CSC’s BOT event and meet others in the CS community!'
startDate: 'May 12 2022 19:00'
online: false
location: 'DC 1350'
poster: 'images/events/2022/spring/BOT.png'
registerLink: https://bit.ly/s22-bot-signup
---

📢 Kick off the spring term with CSC’s BOT event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community now that school is finally in-person? Come to our FIRST event of this term: In-person Speed-Friending event and the CSC circles kickoff!

📌 You will be able to meet others in groups of 3 or 4, with around 10 minutes to talk. 🎤  An optional prompt will act as an icebreaker, but feel free to discuss anything you want! Before the speed-friending event, the details of CSC Circles will be discussed along with our quick intro, so if you’re interested in participating or learning more about CSC, be sure to attend! Even if you don’t want to be part of CSC Circles, you should join us for the speed-friending event 🥰

👀 Just like last term, CSC Circles will consist of groups of 4-6 participants who will be matched to create recurring social circles based on your interests. CSC Circles will help you build meaningful connections over the course of a semester and beyond 🚀

📆 When? May 12th 2022 at 7-9pm EST, in DC 1350

👉 Register at  https://bit.ly/s22-bot-signup. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up.
