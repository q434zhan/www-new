---
name: 'Bootcamp: Mock Interviews'
short: 'Come to practice your interview skills with experienced mentors!'
startDate: 'May 21 2022 18:00'
online: true
location: 'Discord'
poster: 'images/events/2022/spring/Bootcamp-Mentee-Applications.png'
registerLink: https://bit.ly/s22-bootcamp-mentee-signup!
---

📢 Applications for Bootcamp are now OPEN! 📢 CSC is bringing back Bootcamp to gear you up for your next recruiting season, partnered with @uwaterloodsc, @uwblueprint, @uwaterloowics, @uwaterloopm, @uw_ux, and @techplusuw! 💻 Mock interviews take place May 21st 6:00 - 10:00 PM EST.

💁‍♀️ Sign up as a mentee, and join our experienced mentors in Resume Reviews and Mock Interviews (virtual 1:1 sessions) to receive feedback from various tech backgrounds 📃 You will be paired with a mentor who is knowledgeable in the same or a similar career path to yours to ensure relevant feedback! 👌

A mentor will be paired with you based on your career interests to provide insightful feedback and advice to rock your job search - don’t miss out! If you’re interested, please sign up! We would love to help you feel ready and confident for the upcoming job hunt. After signing up, you’ll soon receive a link to the Discord server in which this event takes place. Our collaborating clubs are excited to bring you this opportunity to sharpen your job hunting skills 🧠 If you’re interested, please apply!

👉  Apply using this link https://bit.ly/s22-bootcamp-mentee-signup!

Alternatively, you can email us at exec@csclub.uwaterloo.ca with the year and program you’re in, along with interested job paths.

📅 Deadline to Apply: May 12th 2022, 11:59 PM EST
