---
name: 'CSC: Soiree: Movie Night'
short: "Bring your friends for this chill movie night with CSC!"
startDate: 'June 10 2022 19:00'
endDate: 'June 10 2022 22:00'
online: false
location: 'AL 116'
poster: 'images/events/2022/spring/MovieNight.png'
registerLink: https://bit.ly/s22-movienight-signup 
---

📣 Looking to destress before midterm week starts? ✨CSC is hosting the perfect event for you! Come hang out at CSC Soiree: Movie Night! 🎬

🍿Bring your friends for this chill movie night from 7-10 PM! 👀 There will be time for a mini social afterwards so make sure to come if you want to meet other CSC members!

📆 Event Date: June 10 from 7 PM - 10 PM

📌 Location: AL 116

👉 Sign-up through this link: https://bit.ly/s22-movienight-signup

We can’t wait to see you there!