---
name: "End Of Term + Speed Frending"
short: "Surprise, it's almost end of term!"
startDate: "July 21 2022 19:00"
endDate: "July 21 2022 21:00"
online: false
location: "DC 1351"
poster: "images/events/2022/spring/End-Of-Term.png"
registerLink: https://forms.gle/USNvFgAPCk8MR4me8
---

🎊 Surprise, it's almost end of term!

🎉 Join us from 7-9PM at DC 1351 for a fun night of speed-friending, games, painting & more!

🎲 Some potential games that we might play include: Tug of War, Red Light Green Light, etc.

🥳 If you want to socialize and have some fun before exams, make sure to attend!

📆 Event Date: July 21st from 7-9PM ET at DC 1351

P.S There's free food and Coco's vouchers! 🧋

👉 Register at the link: https://forms.gle/USNvFgAPCk8MR4me8
