---
name: 'Prof Talk with Anton Mosunov'
short: "Are you interested in learning more about parallel computation? CSC is hosting a Prof Talk with the amazing Professor Anton Mosunov to talk about his awe-inspiring research."
startDate: 'July 13 2022 17:00'
endDate: 'July 13 2022 18:30'
online: false
location: 'MC 5479'
poster: 'images/events/2022/spring/Prof-Talk-Anton-Mosunov.png'
registerLink: https://bit.ly/s22-prof-talks
---
📣 Are you interested in learning more about parallel computation? CSC is hosting a Prof Talk with the amazing Professor Anton Mosunov to talk about his awe-inspiring research. 🤩 He will deliver a captivating talk about parallel computation. If you don’t yet know what that is yet or want to learn more about it, make sure to be there! 💻 Make sure to bring a laptop!

📆 Event Date: July 13th from 5 - 6:30 PM

📌 Location: ~~DC 1350~~ MC 5479

👉 Sign-up through this link:  https://bit.ly/s22-prof-talks
