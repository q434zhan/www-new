---
name: 'Physical Security Workshop'
short: 'Physical Security Workshop on Tuesday, Feb 11th, at 6:00 PM in MC 4045'
startDate: 'February 11 2020 18:00'
online: false
location: 'MC 4045'
---

In this physical security workshop, students will learn about the theoretical considerations of what makes a system secure. Then, we will break into teams for a hands-on exercise, and a screening of the legendary movie "Hackers".

Snacks and drinks will be provided.

