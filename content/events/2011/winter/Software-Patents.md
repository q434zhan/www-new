---
name: 'Software Patents'
short: '*by Stanley Khaing*. What are the requirements for obtaining a patent? Should software be patentable?'
startDate: 'March 17 2011 16:30'
online: false
location: 'MC2034'
---

Stanley Khaing is a lawyer from Waterloo whose areas of practice are software and high technology. He will be discussing software patents. In particular, he will be addressing the following questions:

- What are the requirements for obtaining a patent?
- Should software be patentable?

<!-- -->

