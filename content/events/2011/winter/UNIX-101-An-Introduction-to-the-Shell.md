---
name: 'UNIX 101: An Introduction to the Shell'
short: '*by Calum T. Dalek*. New to Unix? No problem, we''ll teach you to power use circles around your friends!'
startDate: 'January 26 2011 16:30'
online: false
location: 'MC3003'
---

This first tutorial is an introduction to the Unix shell environment, both on the student servers and on other Unix environments. Topics covered include: using the shell, both basic interaction and advanced topics like scripting and job control, the filesystem and manipulating it, and ssh. If you feel you're already familiar with these topics don't hesitate to come to Unix 102 to learn about documents, editing, and other related tasks, or watch out for Unix 103 and 104 that get much more in depth into power programming tools on Unix.

