---
name: 'CSC Open Data Code Party'
short: '*by Calum T. Dalek*. The Computer Science Club is teaming up with the UW Open Data Initiative to bring you our third code party of the term! Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.'
startDate: 'November 18 2011 19:00'
online: false
location: 'Comfy Lounge'
---

We're teaming up with the UW Open Data Initiative to host our next code party on Friday, November 18 at 7PM in the MC Comfy Lounge.

As always, you're welcome to work on your own projects, but we'll be hacking on some open data related projects:

1. Design and build UW APIs.
2. Applications using university data that is currently available.

<!-- -->

If you'd like to discuss your ideas for these proposed projects, check out the newsgroup, uw.csc

