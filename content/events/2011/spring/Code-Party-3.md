---
name: 'Code Party 3'
short: 'The final Code Party of the term is here! Come hack on some code,	solve some puzzles, and have some fun. The event starts in the evening and will run	all night. You can show up for any portion of it. You should bring a laptop, and	probably have something in mind to work on, though you''re welcome with neither.Snacks will be provided. Everyone is welcome.Please note this date is postponed from the originally scheduled date due to	conflicts with [Kitchener Ribfest & Craft Beer Show](<http://www.kitchenerribandbeerfest.com/>)'
startDate: 'July 22 2011 19:00'
online: false
location: 'Comfy Lounge'
---

The final Code Party of the term is here! Come hack on some code,	solve some puzzles, and have some fun. The event starts in the evening and will run	all night. You can show up for any portion of it. You should bring a laptop, and	probably have something in mind to work on, though you're welcome with neither.

Snacks will be provided. Everyone is welcome.

Please note this date is postponed from the originally scheduled date due to	conflicts with [Kitchener Ribfest & Craft Beer Show](<http://www.kitchenerribandbeerfest.com/>)

