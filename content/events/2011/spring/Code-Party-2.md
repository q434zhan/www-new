---
name: 'Code Party 2'
short: 'The second Code Party of the term takes place this Friday! Come hack on some code,	solve some puzzles, and have some fun. The event starts in the evening and will run	all night. You can show up for any portion of it. You should bring a laptop, and	probably have something in mind to work on, though you''re welcome with neither.Snacks will be provided.'
startDate: 'June 24 2011 19:00'
online: false
location: 'Comfy Lounge'
---

The second Code Party of the term takes place this Friday! Come hack on some code,	solve some puzzles, and have some fun. The event starts in the evening and will run	all night. You can show up for any portion of it. You should bring a laptop, and	probably have something in mind to work on, though you're welcome with neither.

Snacks will be provided.

