---
name: 'Elections Nominees List'
short: 'CSC Elections, final list of nominations for Spring 2011'
startDate: 'May 09 2011 17:31'
online: false
location: 'Comfy Lounge'
---

The nominations are:

- President: jdonland, mimcpher, mthiffau
- Vice-President: jdonland, mimcpher
- Treasurer: akansong, kspaans
- Secretary: akansong, jdonland

<!-- -->

