---
name: 'An Introduction to Steganography'
short: 'As part of the CSC member talks series, Yomna Nasser will be presenting an introduction to steganography.	Steganography is the act of hiding information such that it can only be found by its intended recipient.	It has been practiced since ancient Greece, and is still in use today.This talk will include an introduction to the area, history, and some basic techniques for hiding information	and detecting hidden data. There will be an overview of some of the mathematics involved, but nothing too	rigorous.'
startDate: 'July 20 2011 16:30'
online: false
location: 'MC 2038'
---

As part of the CSC member talks series, Yomna Nasser will be presenting an introduction to steganography.	Steganography is the act of hiding information such that it can only be found by its intended recipient.	It has been practiced since ancient Greece, and is still in use today.

This talk will include an introduction to the area, history, and some basic techniques for hiding information	and detecting hidden data. There will be an overview of some of the mathematics involved, but nothing too	rigorous.

