---
name: 'GracefulTavi'
short: 'Wiki software in PHP+MySQL'
startDate: 'November 18 2004 17:00'
online: false
location: 'MC 2066'
---

GracefulTavi is an open source wiki programmed by Net Integration Technologies Inc. It is used internally by more than 25 people, and is the primary internal wiki for NITI's R&D and QA.

I'll start with a very brief introduction to wikis in general, then show off our special features: super-condensed formatting syntax, hierarchy management, version control, highlighted diffs, SchedUlator, the Table of Contents generator. As part of this, we'll explain the simple plugin architecture and show people how to write a basic wiki plugin.

As well, I will show some of the "waterloo specific" macros that have been coded, and explain future plans for GracefulTavi.

If time permits, I will explain how gracefulTavi can be easily used for a personal calendar and notepad system on your laptop.

