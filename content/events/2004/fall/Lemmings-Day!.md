---
name: 'Lemmings Day!'
short: 'Everyone else is doing it!'
startDate: 'November 12 2004 14:30'
online: false
location: 'MC 4063'
---

Does being in CS make you feel like a lemming? Is linear algebra driving you into walls? Do you pace back and forth , constantly , regardless of whatever's in your path? Then you should come out to CSC Lemmings Day!



- Play some old-skool Lemmings, Amiga-style
- Live-action lemmings
- Lemmings look-alike contest
- Enjoy classic Lemmings tunes

<!-- -->

Everyone else is doing it!

