---
name: 'Distributed programming for CS and Engineering
   students'
short: 'A talk by Simon Law'
startDate: 'March 16 2004 18:00'
online: false
location: 'MC4058'
---

If you've ever worked with other group members, you know how difficult it is to code simultaneously. You might be working on one part of your assignment, and you need to send your source code to everyone else. Or you might be fixing a bug in someone else's part, and need to merge in the change. What a mess!

This talk will explain some Best Practices for developing code in a distributed fashion. Whether you're working side-by-side in the lab, or developing from home, these methods can apply to your team. You will learn how to apply these techniques in the Unix environment using GNU Make, CVS, GNU diff and patch.

