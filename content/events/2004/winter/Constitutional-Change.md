---
name: 'Constitutional Change'
short: 'Vote to change the CSC Constitution'
startDate: 'February 05 2004 15:30'
online: false
location: 'MC4041'
---

During the General Meeting on 19 January 2004, a proposed constitution change was passed around. This change is in response to a change in the MathSoc Clubs Policy (Policy 4, Section 3, Sub-section f).

This general meeting is called to vote on this proposed change. We must have quorum of 15 Full Members vote on this change. The following text was presented at the CSC Winter 2004 Elections.

<pre>We propose to make a Constitutional change on this day, 19 January 2004.
The proposed change is to section 3.1 of the constitution which
currently reads:

    In compliance with MathSoc regulations and in recognition of the
    club being primarily targeted at undergraduate students, full
    membership is open to all undergraduate students in the Faculty of
    Mathematics and restricted to the same.

Since MathSoc has changed its requirements for club membership, we
propose that it be changed to:

    In compliance with MathSoc regulations and in recognition of the
    club being primarily targeted at undergraduate students, full
    membership is open to all Social Members of the Mathematics Society
    and restricted to the same.</pre>

