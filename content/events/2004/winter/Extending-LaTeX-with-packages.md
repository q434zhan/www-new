---
name: 'Extending LaTeX with packages'
short: 'A talk by Simon Law'
startDate: 'March 23 2004 18:00'
online: false
location: 'MC4058'
---

LaTeX is a document processing system. What this means is you describe the structure of your document, and LaTeX typesets it appealingly. However, LaTeX was developed in the late-80s and is now showing its age.

How does it compete against modern systems? By being easily extensible, of course. This talk will describe the fundamentals of typesetting in LaTeX, and will then show you how to extend it with freely available packages. You will learn how to teach yourself LaTeX and how to find extensions that do what you want.

As well, there will be a short introduction on creating your own packages, for your own personal use.

