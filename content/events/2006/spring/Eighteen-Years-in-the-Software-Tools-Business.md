---
name: 'Eighteen Years in the Software Tools Business'
short: 'Eighteen Years in the Software Tools Business at Microsoft, a talk by Rico Mariani, (BMath CS/EEE 1988)'
startDate: 'May 25 2006 16:00'
online: false
location: 'MC 4060'
---

Rico Mariani, (BMath CS/EEE 1988) now an (almost) 18 year Microsoft veteran but then a CSC president comes to talk to us about the evolution of software tools for microcomputers. This talk promises to be a little bit about history and perspective (at least from the Microsoft side of things) as well as the evolution of software engineers, different types of programmers and their needs, and what it's like to try to make the software industry more effective at what it does, and sometimes succeed!

A video of the talk is available for download in our [media](<media/>) section.

