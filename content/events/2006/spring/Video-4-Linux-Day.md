---
name: 'Video 4 Linux Day'
short: ' We don''t know enough about V4L'
startDate: 'May 13 2006 13:00'
online: false
location: 'CSC'
---

We don't know Video 4 Linux, but increasingly people are wanting to do interesting stuff with our webcam which could benefit from a better understanding of Video 4 Linux. So, this Saturday a number of us will be trying to learn as much as possible about Video 4 Linux and doing weird things with webcam(s).

