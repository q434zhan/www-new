---
name: 'LaTeX: Reports'
short: 'Writing reports that look good.'
startDate: 'February 13 2003 18:30'
online: false
location: 'MC1085'
---

Work term reports, papers, and other technical documents can be typeset in L<sup><small>A</small></sup>

T<small>E</small>

X to great effect. In this session, I will provide examples on how to typeset tables, figures, and references. You will also learn how to make tables of contents, bibliographies, and how to create footnotes.

I will also examine various packages of L<sup><small>A</small></sup>

T<small>E</small>

X that can help you meet requirements set by users of inferior typesetting systems. These include double-spacing, hyphenation and specific margin sizes.

