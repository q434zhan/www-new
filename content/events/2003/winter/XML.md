---
name: 'XML'
short: 'Give your documents more markup'
startDate: 'March 13 2003 18:30'
online: false
location: 'MC1085'
---

XML is the <q>eXtensible Markup Language,</q>

 a standard maintained by the World Wide Web Consortium. A descendant of IBM's SGML. It is a metalanguage which can be used to define markup languages for semantically describing a document.

This talk will describe how to generate correct XML documents, and auxiliary technologies that work with XML.

