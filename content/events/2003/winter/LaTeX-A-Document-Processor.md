---
name: 'LaTeX: A Document Processor'
short: 'Typesetting beautiful text'
startDate: 'February 06 2003 18:30'
online: false
location: 'MC1085'
---

Unix was one of the first electronic typesetting platforms. The innovative AT&T *troff* system allowed researches at Bell Labs to generate high quality camera-ready proofs for their papers. Later, Donald Knuth invented a typesetting system called T<small>E</small>

X, which was far superior to other typesetting systems in the 1980s. However, it was still a typesetting language, where one had to specify exactly how text was to be set.

L<sup><small>A</small></sup>

T<small>E</small>

X is a macro package for the T<small>E</small>

X system that allows an author to describe his document's function, thereby typesetting the text in an attractive and correct way. In addition, one can define semantic tags to a document, in order to describe the meaning of the document; rather than the layout.

