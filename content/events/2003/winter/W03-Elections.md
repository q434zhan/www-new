---
name: 'W03 Elections'
short: 'Come out and vote for the new exec!'
startDate: 'January 13 2003 18:00'
online: false
location: 'MC3001'
---

This term's elections will take place on Monday, January 13 at 6:00 PM in the MC "comfy lounge" (MC3001). Nominations are open from now on (Thursday, January 2) until 4:30 PM of the day before elections (Sunday, January 12). In order to nominate someone you can either e-mail me directly, by depositing a form with the required information in the CSC mailbox in the Mathsoc office or by writing the nomination and clearly marking it as such on the large whiteboard in the CSC office. E-mail is probably the best choice. Please include the name of the person to be nominated as well as the position you wish to nominate them for.

Candidates must be full members of the club. This means they must have paid their membership for the given term and (due to recent changes in the constitution) must be full-time undergraduate math students. The same requirements hold for those voting. Please bring your Watcard to the elections so that I can verify this. I will have a list of members with me also.

The positions open are:

**President** \-- appoints all committees of the club, calls and presides at all meetings of the club and audits the club's financial records. Really, this is the person in charge.

**Vice President** \-- assumes President's duties in case he/she is absent, plans and coordinates events with the programmes committee and assumes any other duties delegated by the President. This is a really fun job if you enjoy coordinating events!

**Secretary** \-- keeps minutes of the meetings and cares for any correspondence. A fairly light job, good choice if you just want to see what being an exec is all about.

**Treasurer** \-- maintains all the finances of the club. If you like money and keeping records, this is the job for you!

Additionally a Systems Administrator will be picked by the new executive.

Last term was a great term for the CSC -- many events, some office renovations and a much improved image were all part of it. I hope to see the next term's exec continue this. If you're interested in seeing this happen, do consider going for a position, or helping out as office staff or on one of the committees.

Anyways, hopefully I'll see many of you at the elections. Remember: Monday, January 13, 6:00 PM, MC3001/Comfy Lounge.

If you have any further questions don't hesitate to contact the CRO, Stefanus Du Toit [by e-mail](<mailto:sjdutoit@uwaterloo.ca>).

