---
name: 'Sh'
short: 'Metaprogramming your way to stunning effects.'
startDate: 'July 17 2003 16:30'
online: false
location: 'MC4064'
---

Modern graphics processors allow developers to upload small "shader programs" to the GPU, which can be executed per-vertex or even per-pixel during the rendering. Such shaders allow stunning effects to be performed in real-time, but unfortunately aren't very easy to program since one generally has to write them at the assembly level.

Recently a few high-level languages for shader programming have become available. Sh, a result of research at UW, is one such language. It allows programming powerful shaders in simple and intuitive ways. Sh is particularly interesting because of the way it is implemented. Instead of coming up with a language grammar and writing a full-fledged compiler, Sh is implemented as a C++ library, and shader programs are effectively written in C++. The actual compilation then takes place in a manner similar to JIT (Just-in-time) compilers. This has many advantages over the traditional approach, including C++'s familiar syntax for users, and much less work for the Sh implementers.

In this talk I will give an overview of GPUs and the Sh language as well as some interesting details on how Sh was implemented.

<!-- Is there a bio tag -->

 Stefanus Du Toit is a research assistant at the University of Waterloo. He has implemented the current version of Sh from scratch and is actively developing it under supervision of Michael McCool, the original designer of the language.

