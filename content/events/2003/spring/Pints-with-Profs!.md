---
name: 'Pints with Profs!'
short: 'Get to know your profs and be the envy of your friends!'
startDate: 'June 09 2003 05:00'
online: false
location: 'The Grad House'
---

Come out and meet your professors!! This is a great opportunity to meet professors for Undergraduate Research jobs or to find out who you might have for future courses. One and all are welcome!

Best of all... free food!!!

