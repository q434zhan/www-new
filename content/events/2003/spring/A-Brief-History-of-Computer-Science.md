---
name: 'A Brief History of Computer Science'
short: 'War, insanity, espionage, beauty, domination, sacrifice, and tragic death... not what one might associate with the history of computer science. In this talk I will focus on the origin of our discipline in the fields of engineering, mathematics, and science, and on the complicated personalities that shaped its evolution. No advanced technical knowledge is required.'
startDate: 'June 10 2003 16:30'
online: false
location: 'MC2066'
---

War, insanity, espionage, beauty, domination, sacrifice, and tragic death... not what one might associate with the history of computer science. In this talk I will focus on the origin of our discipline in the fields of engineering, mathematics, and science, and on the complicated personalities that shaped its evolution. No advanced technical knowledge is required.

