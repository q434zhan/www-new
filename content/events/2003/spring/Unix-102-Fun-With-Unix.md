---
name: 'Unix 102: Fun With Unix'
short: 'Talking to your Unix can be fun and profitable'
startDate: 'June 05 2003 16:30'
online: false
location: 'MC2037'
---

This is the second in a series of seminars that cover the use of the Unix Operating System. Unix is used in a variety of applications, both in academia and industry. We will provide you with hands-on experience with the Math Faculty's Unix environment in this tutorial.

Topics that will be discussed include:

- Interacting with Bourne and C shells
- Editing text with the vi text editor
- Editing text with the Emacs display editor
- Multi-tasking and the screen multiplexer

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class

