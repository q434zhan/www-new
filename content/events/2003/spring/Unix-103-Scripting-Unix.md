---
name: 'Unix 103: Scripting Unix'
short: 'You too can be a Unix taskmaster'
startDate: 'June 12 2003 16:30'
online: false
location: 'MC2037'
---

This is the third in a series of seminars that cover the use of the Unix Operating System. Unix is used in a variety of applications, both in academia and industry. We will provide you with hands-on experience with the Math Faculty's Unix environment in this tutorial.

Topics that will be discussed include:

- Shell scripting
- Searching through text files
- Batch editing text files

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class

