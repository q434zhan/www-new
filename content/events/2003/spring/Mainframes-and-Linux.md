---
name: 'Mainframes and Linux'
short: 'A talk by Jim Elliott. Jim is responsible for IBM''s in Open Source activities and IBM''s mainframe operating systems for Canada and the Carribbean.'
startDate: 'July 08 2003 16:00'
online: false
location: 'MC2065'
---

Linux and Open Source have become a significant reality in the working world of Information Technology. An indirect result has been a "rebirth" of the mainframe as a strategic platform for enterprise computing. In this session Jim Elliott, IBM's Linux Advocate, will provide an overview of these technologies and an inside look at IBM's participation in the community. Jim will examine Linux usage on the desktop, embedded systems and servers, a reality check on the common misconceptions that surround Linux and Open Source, and an overview of the history and current design of IBM's mainframe servers.

Jim Elliott is the Linux Advocate for IBM Canada. He is responsible for IBM's participation in Linux and Open Source activities and IBM's mainframe operating systems in Canada and the Caribbean. Jim is a popular speaker on Linux and Open Source at conferences and user groups across the Americas and Europe and has spoken to over 300 organizations over the past three years. Over his 30 years with IBM he has been the co-author of over 15 IBM publications and he also coordinated the launch of Linux on IBM mainframes in the Americas. In his spare time, Jim is addicted to reading historical mystery novels and travel to their locales.

[Slides](<http://www.vm.ibm.com/devpages/jelliott/events.html>)

