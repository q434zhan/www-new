---
name: 'June 12 Exec Meeting'
short: 'Have an issue that should be brought up? We''d love to hear it!'
startDate: 'June 12 2003 15:30'
online: false
location: 'MC3036 CSC Office'
---

<pre>
Budget: All the money we requested
	--No money from Pints from Profs
	--MathSoc has promised us $1250

Feedback from Completed Events
	UNIX Talks: 17 people for first
			--12 people for second
			--Things going well
			--Last talk today
			--VI next week
	IPsec
	--Sparse crowd
	--People Jim didn't know talked to him for 1/2 hour

	History of CSC talk went well
	--Good variety of people

	Pints with Profs
	--NO CS Profs
	--Only 1 E&amp; CE prof
	--Only 2 Math profs
	--Jim will harass the profs at the School of CS Council meeting.

	We're starting to fall behind in planning

	RoShamBo rules
	--Got a web site up
	--Might have to move RSB back
	--International site has a few test samples
	--Stefanus had some ideas
	--Coding will probably take an afternoon/evening
	--We need volunteers to run the competition
	--We have volunteers to code: Phil and Stefanus

	ACTION ITEM: Phil and Stefanus
	--code whatever you volunteered to code for.

	--Mike intends to visit classes and directly advertise
	--Email Christina Hotz

	--GH guy: Mike has an abstract, will have posters by tomorrow

	CSC Movie Night
	--Mathnet, Hackers, Wargames, Tron
	--Mike will get a room
	--Will be closed member

	Mike McCool is offering rooms for showing SIGGRAPH
	ACTION ITEM: Jim
	-check with Mike McCool.

	ACTION ITEM: Mike
	-Make posters for Movie Nights

	When is other movie night? (Will plan some time in July)

	Who is our foreign speaker?
	Action Item: jelliot@ca.ibm.com (Check name first) about
	getting a foreign speaker -- Note: Has already been contacted.

	Simon got money from Engsoc

	Cass needs coloured paper (CSC is out)

	ACTION ITEM: Cass and Mark
	--get labelmaker tape, masking tape,
	whiteboard makers, coloured paper
	--keep receipts for CSC office expenses

	NOTICE: Mike is now Imapd

	Simon distributed budget list
	Mark got the money from Mathsoc for last budget, deposited it.

	ACTION ITEM:Mark
	--Get MEF funding by July 4th (equipment)
	ACTION ITEM: Simon
	--Get WEEF funding by June 27th (book)

	Jim still working on allowing executives and voters to be
	non-math members

	We get free photocopying from MathSoc
	ACTION ITEM: Mike
	--write down code for free photocopying from MathSoc

	Simon has been able to get into the cscdisk account, still
	looking into getting into the cscceo account.

	Damien got an e-mail stating that the files for cscdisk are
	out of date.

	ACTION ITEM: Simon
	--provide SSH key to Phil for getting into cscdisk, cscceo, etc...
	--Renumber bootup scripts for sugar and powerpc so that they
	boot up happily.

	ACTION ITEM: Mike needs to do all the plantops stuff again.

	ACTION ITEM: Mike -- "Stapler if you say please" sign.

	CVS Tree for CEO has been exported.
	Damien has volunteered to finish CEO (found by Cass)

	All books with barcodes have been scanned
	All books without barcodes need to be bar-coded.

	ACTION ITEM: Mark
	--Find a Credit-card with a $500 or less limit.

	Note: There needs to be a private section in the
	CSC Procedures Manual. (Only accessible by shell)

	Stefanus Wanted to mention that we should talk to
	Yolanda, Craig or Louie about a EYT event for Frosh Week.
  </pre>

