---
name: 'Unix 101: First Steps With Unix'
short: 'Learn Unix and be the envy of your friends!'
startDate: 'May 29 2003 16:30'
online: false
location: 'MC2037'
---

This is the first in a series of seminars that cover the use of the Unix Operating System. Unix is used in a variety of applications, both in academia and industry. We will provide you with hands-on experience with the Math Faculty's Unix environment in this seminar.

Topics that will be discussed include:

- Navigating the Unix environment
- Using common Unix commands
- Using the PICO text editor
- Reading electronic mail and news with PINE

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class.

