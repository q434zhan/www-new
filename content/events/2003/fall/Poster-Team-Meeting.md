---
name: 'Poster Team Meeting'
short: 'Join the Poster Team and get Free Pizza!'
startDate: 'October 06 2003 16:00'
online: false
location: 'MC3001 (Comfy)'
---

- Do you like computer science?
- Do you like posters?
- Do you like free pizza?

<!-- -->

If the answer to one of these questions is yes, then come out to the first meeting of the Computer Science Club Poster Team! The CSC is looking for interested students to help out with promotion and publicity for this term's events. We promise good times and free pizza!

