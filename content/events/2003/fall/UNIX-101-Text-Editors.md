---
name: 'UNIX 101: Text Editors'
short: 'vi vs. emacs: The Ultimate Showdown'
startDate: 'October 02 2003 16:00'
online: false
location: 'MC2037'
---

Have you ever wondered how those cryptic UNIX text editors work? Have you ever woken up at night with a cold sweat wondering "Is it CTRL-A, or CTRL-X CTRL-A?" Do you just hate pico with a passion?

Then come to this tutorial and learn how to use vi and emacs!

Basic UNIX commands will also be covered. This tutorial will be especially useful for first and second year students.

