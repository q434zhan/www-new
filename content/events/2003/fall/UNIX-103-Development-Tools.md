---
name: 'UNIX 103: Development Tools'
short: 'GCC, GDB, Make'
startDate: 'October 16 2003 16:00'
online: false
location: 'MC2037'
---

This tutorial will provide you with a practical introduction to GNU development tools on Unix such as the gcc compiler, the gdb debugger and the GNU make build tool.

This talk is geared primarily at those mostly unfamiliar with these tools. Amongst other things we will introduce:

- gcc options, version differences, and peculiarities
- using gdb to debug segfaults, set breakpoints and find out what's wrong
- tiny Makefiles that will compile all of your 2nd and 3rd year CS projects.

<!-- -->

If you're in second year CS and unfamiliar with UNIX development it is highly recommended you go to this talk. All are welcome, including non-math students.

Arrive early!

