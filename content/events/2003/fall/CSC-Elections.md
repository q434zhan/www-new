---
name: 'CSC Elections'
short: 'CSC Fall 2003 Elections'
startDate: 'September 17 2003 16:30'
online: false
location: 'MC3001 (Comfy)'
---

Elections will be held on Wednesday, September 17, 2003 at 4:30 PM in the Comfy Lounge, MC3001.

I invite you to nominate yourself or others for executive positions, starting immediately. Simply e-mail me at cro@csclub.uwaterloo.ca with the name of the person who is to be nominated and the position they're nominated for.

Nominees must be full-time undergraduate students in Math. Sorry!

Positions open for elections are:

- President: Organises the club, appoints committees, keeps everyone busy. If you have lots of ideas about the club in general and like bossing people around, go for it!
- Vice President: Organises events, acts as the president if he's not available. If you have lots of ideas for events, and spare time, go for it!
- Treasurer: Keeps track of the club's finances. Gets to sign cheques and stuff. If you enjoy dealing with money and have ideas on how to spend it, go for it!
- Secretary: Takes care of minutes and outside correspondence. If you enjoy writing things down and want to use our nifty new letterhead style, go for it!

<!-- -->

Nominations will be accepted until Tuesday, September 16 at 4:30 PM.

Additionally, a Sysadmin will be appointed after the elections. If you like working with Unix systems and have experience setting up and maintaining them, go for it!

I hope that lots of people will show up; hopefully we'll have a great term with plenty of events. We always need other volunteers, so if you want to get involved just talk to the new exec after the meeting. Librarians, webmasters, poster runners, etc. are always sought after!

There will also be free pop.

Memberships can be purchased at the elections or at least half an hour prior to at the CSC. Only undergrad math members can vote, but anyone can become a member.

