---
name: 'Oh No! More Lemmings Day!'
short: 'Come out for some retro Amiga-style Lemmings gaming action!'
startDate: 'March 15 2005 16:30'
online: false
location: 'MC 4060'
---

Does being in CS make you feel like a lemming? Is linear algebra driving you into walls? Do you pace back and forth, constantly, regardless of whatever's in your path? Then you should come out to CSC Lemmings Day. This time, we're playing the pseudo-sequel: Oh No! More Lemmings!

- Old-skool retro gaming, Amiga-style (2 mice, 2 players!)
- Projector screen: the pixels are man-sized!
- Live-Action Lemmings (the rules are better this time)
- Lemmings look-alike contest
- Enjoy classic Lemmings tunes

<!-- -->

