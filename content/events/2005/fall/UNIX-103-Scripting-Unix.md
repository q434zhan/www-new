---
name: 'UNIX 103:  Scripting Unix'
short: 'You Too Can Be a Unix Taskmaster'
startDate: 'October 11 2005 16:30'
online: false
location: 'MC 2037'
---

This is the third in a series of seminars that cover the use of the UNIX Operating System. UNIX is used in a variety of applications, both in academia and industry. We will provide you with hands-on experience with the Math Faculty's UNIX environment in this tutorial.

Topics that will be discussed include:

- Shell scripting
- Searching through text files
- Batch editing text files

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class.

