---
name: 'CSC Codeylympics II'
short: 'Codeylympics is back! Race in teams across campus while completing fun CS-themed challenges to become the Codeylympics champions!'
startDate: 'March 13 2024 18:00'
endDate: 'March 13 2024 21:00'
online: false
location: 'STC 0040 & 0050'
poster: 'images/events/2024/winter/1711581158973--Codeylympics2.png'
---

🏃 Join us for the second official Codeylympics for a fun night of CS-themed challenges such as LeetCode Relay, Code Your Friends, Find the Bug, and more! Compete in teams as you complete these challenges to earn points for a chance to win some free CSC merch!

📢 Sign up via the link in our bio to register for the event either individually or with a team of friends! Those who sign up individually will be assigned teams on the day of the event.

📆 When? March 13, 2023 at 6:00 - 9:00pm EST

📍 Where? STC 0040 and 0050
