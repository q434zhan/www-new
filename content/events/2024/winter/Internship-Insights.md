---
name: 'Internship Insights'
short: 'Internship Insights kicks off this week!'
startDate: 'February 29 2024 20:00'
online: false
location: 'MC 4063'
poster: 'images/events/2024/winter/1711580675968--Internship-Insights.png'
---

🚨 Internship Insights kicks off this week! Secure your spot for personalized, in-person resume reviews, and mock interviews with experienced upper-year students.

There are limited weekly slots available as it is first-come-first-serve, so make sure to sign up by today! We will email those who got in beforehand.

💡 Signups open every Tuesday at 8pm.
