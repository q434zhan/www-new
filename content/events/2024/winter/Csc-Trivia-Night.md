---
name: 'CSC Trivia Night'
short: 'Elimination bracket style event for trivia! Compete in teams of up to 4 for $300 worth of prizes!'
startDate: 'March 07 2024 18:30'
endDate: 'March 07 2024 20:30'
online: false
location: 'QNC 2502'
poster: 'images/events/2024/winter/1711580930660--CSC-Trivia.png'
---

🚀 Calling all tech wizards and code connoisseurs! UWCSC presents an epic Trivia Night where computer science meets brainpower, and over $300 in prizes await the cleverest minds!

🤔With questions covering a wide range of topics, not just Computer Science, this event is sure to keep you on your toes. So gather your friends, bring your thinking caps, and get ready to win!

👉 Register at the link in our bio. See you there!

📆 Event Date: March 7, 6:30-8:30 PM

📌 Location: QNC 2502
