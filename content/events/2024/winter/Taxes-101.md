---
name: 'Taxes 101'
short: 'Join us online or in-person to learn how you can get your tax refund in minutes!'
startDate: 'March 20 2024 19:00'
online: false
location: 'E7 4053'
poster: 'images/events/2024/winter/1711581598854--Taxes-101.png'
---

Tax season is here ✨ Join us online or in-person at E7 RM#4053, 7 pm, (pizza provided 🍕) to learn how you can get your tax refund in minutes! Presented by Wealthsimple Tax, hosted by EngSoc and yours truly, CSC 😎. Whether you're filing for the first time or need a few pointers, Wealthsimple Tax has you covered with their free, simple, and comprehensive tool. Feel free to bring any tax questions! #taxseason 💵

Location: E7 4053

Time: March 20, 7 pm ET
