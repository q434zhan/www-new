---
name: 'Spring 2017 Elections'
short: 'The Computer Science Club will be holding elections for the Spring 2017 President, Vice-President, Secretary and Treasurer. Additionally, the Systems Administrator, Office Manager and Librarian will be appointed.'
startDate: 'May 17 2017 18:00'
online: false
location: 'MC Comfy Lounge'
---

The Computer Science Club will be holding elections for the Spring 2017 term on Wednesday, May 17th at 6:00pm in the MC Comfy Lounge (MC 3001).

The following positions will be elected: President, Vice-President, Treasurer and Secretary. The following positions will be appointed: Systems Administrator (to be ratified at the meeting), Office Manager and Librarian. Additionally, we will be looking for members to join the Programme Committee.

If you would like to run or nominate someone for any of the elected positions, you can put your name in a special box in the CSC office (MC 3036/3037) or by sending an email to the Chief Returning Officer (Zachary) at [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>). Please note that executive positions are restricted to MathSoc social members. We welcome the participation of first years. A list of current nominations will be available on the whiteboard in the office and at [https://csclub.uwaterloo.ca/elections](<https://csclub.uwaterloo.ca/elections>).

Nominations will close at 6:00pm on Tuesday, May 16th (24 hours prior to the start of elections). Voting will be done in a heads-down, hands-up manner and is restricted to MathSoc social members. A full description of the roles and the election procedure are listed in our Constitution, available at [ https://csclub.uwaterloo.ca/about/constitution ](<https://csclub.uwaterloo.ca/about/constitution>). Any questions related to the election can be directed to [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>).

