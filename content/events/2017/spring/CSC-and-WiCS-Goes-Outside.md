---
name: 'CSC and WiCS Goes Outside'
short: 'Come join Women in Computer Science and the Computer Science Club outdoors!'
startDate: 'July 05 2017 19:00'
online: false
location: 'Laurel Creek Firepit'
---

Come hang out with the Women in Computer Science and the Computer Science Club! We have Marshmallows and other treats. Also fire. And a creek. Let's enjoy the outdoors!

