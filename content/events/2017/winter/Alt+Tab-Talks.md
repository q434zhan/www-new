---
name: 'Alt+Tab Talks'
short: 'Come watch (or give!) interesting short talks by CS Club members.	Talks include "Stepping into math: building a step-by-step algebra solver" and "Online database migrations at scale", but more are welcome (email [tghume@csclub.uwaterloo.ca](<mailto:tghume@csclub.uwaterloo.ca>))! Click the link to the event detail page for more info. (Note: date was moved to Thursday)'
startDate: 'March 09 2017 18:00'
online: false
location: 'MC 4042'
---

Come watch (and/or give!) interesting short talks by CS Club members.	Talks include "How your text editor does syntax highlighting", "Online database migrations at scale", "Stitching Spaces in Subdivision Surfaces", "Theory of Computation" and "Stepping into math: building a step-by-step algebra solver", but more are welcome!

Each talk can be 5-15 minutes long on any computer-related topic of interest.	If you're interested in giving a talk (please do!) email [tghume@csclub.uwaterloo.ca](<mailto:tghume@csclub.uwaterloo.ca>).

The event was previously scheduled for Wednesday but was moved to Thursday the 9th due to a conflict with a WICS event.

