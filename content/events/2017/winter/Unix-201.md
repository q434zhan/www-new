---
name: 'Unix 201'
short: 'A talk and demo about more advanced Unix tricks and tools than are taught in our regular Unix 101 events. Topics may include customizing your prompt, the ranger console file manager, fancy shells, htop, rsync and using terminal escape sequences in your programs.'
startDate: 'March 22 2017 18:00'
online: false
location: 'MC 4045'
---

A talk and demo about more advanced Unix tricks and tools than are taught in our regular Unix 101 events. Topics may include customizing your prompt, the ranger console file manager, fancy shells, htop, rsync and using terminal escape sequences in your programs.

List of things talked about can be found [here](<https://gist.github.com/trishume/fb1c80f61c9a62426a6565a9f661e449>).

