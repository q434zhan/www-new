---
name: 'Unix 101'
short: 'Interested in Linux, but don''t know where to start? Come learn some basic topics with us including interaction with the shell, motivation for using it, some simple commands, and more! (Snacks after)'
startDate: 'October 12 2017 17:30'
online: false
location: 'MC 3003'
---

New to the Linux computing environment? If you seek an introduction, look no further (you can if you want we're not the police). Topics that will be covered include basic interaction with the shell and the motivations behind using it, and an introduction to compilation. You'll have to learn this stuff in CS 246 anyways, so why not get a head start!

If you're interested in attending, make sure you can log into the Macs on the third floor, or show up to the CSC office (MC 3036) 20 minutes early for some help.

