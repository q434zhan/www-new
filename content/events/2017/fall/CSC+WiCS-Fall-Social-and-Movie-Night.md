---
name: 'CSC+WiCS Fall Social and Movie Night'
short: 'Join other CSC and WiCS members for a social event featuring free food, board games and a showing of Wonder Woman.'
startDate: 'November 02 2017 19:00'
online: false
location: 'MC 4059 and MC 4061'
---

Join other CSC and WiCS members for a social event featuring free food, board games and a showing of Wonder Woman.

