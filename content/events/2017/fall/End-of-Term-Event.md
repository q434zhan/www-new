---
name: 'End of Term Event'
short: 'Join fellow CSC members for our end of term social. There will be food and	good company. We can grab board games from Mathsoc.'
startDate: 'December 01 2017 18:00'
online: false
location: 'MC Comfy'
---

Join fellow CSC members for our end of term social. There will be food and	good company. We can grab board games from Mathsoc.

