---
name: 'Code Party 1'
short: 'The CS Club is hosting our second Code Party of the term from 6:00pm until late in the evening in STC 0050!Come code with us, eat some food, do some things.'
startDate: 'November 15 2017 17:30'
online: false
location: 'STC 0050'
---

The CS Club is hosting our second Code Party of the term from 5:30pm until late in the evening in STC 0050!

Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 1 and do it, with great company and great food.

Come any time after 5:30pm.

