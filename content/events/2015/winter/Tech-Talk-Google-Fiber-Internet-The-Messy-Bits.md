---
name: 'Tech Talk: Google Fiber Internet: The Messy Bits'
short: 'Our speaker, Avery Pennarun, will share some not-very-secret secrets from	the team creating GFiber''s open source router firmware, including some	discussion of wifi, marketing truthiness, the laws of physics, something	about coaxial cables, embedded ARM processors, queuing theory, signal	processing, hardware design, and kernel driver optimization. If you''re lucky,	he may also rant about poor garbage collector implementations. Also, there	will be at least one slide containing one of those swooshy circle-and-arrow	lifecycle diagrams, we promise.Please RSVP here: http://bit.ly/GoogleFiberTalk.'
startDate: 'January 15 2015 18:00'
online: false
location: 'MC 2065'
---

Google Fiber's Internet service offers 1000 Mbps internet to a few cities:	that's 100x faster than a typical home connection. The problem with going	so fast is it moves the bottleneck around: for the first time, your Internet	link may be faster than your computer, your wifi, or even your home LAN.

Our speaker, Avery Pennarun, will share some not-very-secret secrets from	the team creating GFiber's open source router firmware, including some	discussion of wifi, marketing truthiness, the laws of physics, something	about coaxial cables, embedded ARM processors, queuing theory, signal	processing, hardware design, and kernel driver optimization. If you're lucky,	he may also rant about poor garbage collector implementations. Also, there	will be at least one slide containing one of those swooshy circle-and-arrow	lifecycle diagrams, we promise.

About Avery Pennarun:	Avery graduated from the University of Waterloo in Computer Engineering,	started some startups and some open source projects, and now works at Google	Fiber on a small team building super fast wifi routers, TV settop boxes, and	the firmware that runs on them. He lives in New York.

Please RSVP here: http://bit.ly/GoogleFiberTalk.

