---
name: 'Runtime Type Inference in Dynamic Languages - Day 1'
short: 'Javascript is fast. In some cases, very close to compiled-language fast. How is this even possible? How do we know what types our variables have? How can we optimize it well? Kannan Vijayan will be talking about the historical advances in JIT-compilation of dynamically typed programs over two days. Of course, both of those talks will have free food.'
startDate: 'March 09 2015 18:00'
online: false
location: 'MC 4040'
---

How do we make dynamic languages fast? Today, modern Javascript engines have demonstrated that programs written in dynamically typed scripting lan- guages can be executed close to the speed of programs written in languages with static types. So how did we get here? How do we extract precious type information from programs at runtime? If any variable can hold a value of any type, then how can we optimize well? 


 This talk covers a bit of the history of the techniques used in this space, and tries to summarize, in broad strokes, how those techniques come together to enable efficient jit-compilation of dynamically typed programs. To do the topic justice, Kannan Vijayan will be talking the Monday and Tuesday March 9th and 10th. 


 Does that mean two consecutive days of free food? Yes it does.

