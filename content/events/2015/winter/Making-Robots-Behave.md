---
name: 'Making Robots Behave'
short: 'Part of the Cheriton School of CS'' Distinguished Lecture Series, MIT''s Leslie Kaelbling will discuss robotic AI applied to the messy real world. We make a number of approximations during planning but regain robustness and effectiveness through a continuous state estimation and replanning process. This allows us to solve problems that would otherwise be intractable to solve optimally.'
startDate: 'February 05 2015 15:30'
online: false
location: 'DC 1302'
---

The fields of AI and robotics have made great improvements in many individual subfields, including in motion planning, symbolic planning, probabilistic reasoning, perception, and learning. Our goal is to develop an integrated approach to solving very large problems that are hopelessly intractable to solve optimally. We make a number of approximations during planning, including serializing subtasks, factoring distributions, and determinizing stochastic dynamics, but regain robustness and effectiveness through a continuous state estimation and replanning process. This approach is demonstrated in three robotic domains, each of which integrates perception, estimation, planning, and manipulation.

