---
name: 'Racket''s Magical match'
short: 'Theo Belaire, a fourth-year CS student, will be talking about Racket''s match'' function. Bug resistant, legible, and super powerful! Especially useful for CS 241 in writing compilers, but all-round a joy to write.'
startDate: 'February 02 2015 18:00'
online: false
location: 'MC 4063'
---

Come learn how to use the power of the Racket match construct to make your code easier to read, less bug-prone and overall more awesome!

Theo Belaire, a fourth-year CS student, will show you the basics of how this amazing function works, and help you get your feet wet with some code examples and advanced use cases.

If you're interested in knowing about the more powerful features of Racket, then this is the talk for you! The material covered is especially useful for students in CS 241 who are writing their compiler in Racket, or are just curious about what that might look like.

