---
name: 'Infrasound is all around us'
short: 'Ambient infra sound surrounds us. Richard Mann presents his current research and equipment on measuring infra sound, and samples of recorded infra sound.'
startDate: 'July 08 2015 18:00'
online: false
location: 'MC 4060'
---

Infra sound refers to sound waves below the range of human hearing. Infra sound comes from a number of natural phenomena including weather changes, thunder, and ocean waves. Common man made sources include heating and ventilation systems, industrial machinery, moving vehicle cabins (air, trains, cars), and energy generation (wind turbines, gas plants). 


 In this talk Richard Mann will present equipment he has built to measure infra sound, and analyse some of the infra sound he has recorded. 


 Note: In Winter 2016 Richard Mann will be offering a new course, in Computer Sound. The course will appear as CS489/CS689 ("Topics in Computer Science"). This is a project-based course (60% assignments, 40% project, no final). Details at his web page, [\~mannr](<http://www.cs.uwaterloo.ca/~mannr>).

