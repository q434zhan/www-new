---
name: 'WiCS and CSC Go Outside'
short: 'Come hang out with the Women in Computer Science and the Computer Science Club! There will be s''mores and frozen yogurt. Also fire. And a creek. Let''s enjoy the outdoors!'
startDate: 'June 26 2015 19:00'
online: false
location: 'Laurel Creek Firepit'
---

Come hang out with the Women in Computer Science and the Computer Science Club! There will be s'mores and frozen yogurt. Also fire. And a creek. Let's enjoy the outdoors!

