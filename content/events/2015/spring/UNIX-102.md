---
name: 'UNIX 102'
short: 'n things SCS hasn''t told you about the shell'
startDate: 'June 19 2015 18:00'
online: false
location: 'MC 3003'
---

This is a continuation of the Unix10X series of seminars that cover the use of \*nix environments, largely through interacting with a command line shell. In this instalment we will be covering some of what the School of Computer Science has left out of their introduction to the Command Line / Bash (from cs246), as well as a brief introduction to having a useful prompt. 


 Topics to be discussed include:

- Lost Bash: fancy expansion, arrays, and shopt
- The File System is scary: your file names contain white space and newlines
- Where Am I: A brief introduction to prompt customization

<!-- -->

