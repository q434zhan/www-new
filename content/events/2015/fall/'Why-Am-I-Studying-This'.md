---
name: '''Why Am I Studying This?'''
short: 'Big-O, the Halting Problem, Finite State Machines, and more are concepts that get even more interesting in the real world. Come and hear Tom Rathborne talk about how theory hits reality (often with a bang!) at Booking.com.'
startDate: 'October 02 2015 19:30'
online: false
location: 'MC 4040'
---

- Data Structures
- Finite State Machines
- big-O
- Queuing theory
- Race conditions
- Compilers
- The Halting Problem
- etc.

<!-- -->

These things get even more interesting in the real world. Come and hear Tom Rathborne talk about how theory hits reality (often with a bang!) at Booking.com, the biggest not-a-technology-company on the Internet.

Food and drinks will be provided!

