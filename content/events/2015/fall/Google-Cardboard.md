---
name: 'Google Cardboard'
short: 'Come for a talk from Rob Suderman on Cardboard, Google''s recent exploration in affordable, cereal box based Virtual Reality.'
startDate: 'September 17 2015 18:00'
online: false
location: 'MC 2065'
---

Come for a talk from Rob Suderman on Cardboard, Google's recent exploration in affordable, cereal box based Virtual Reality.

Learn about the tools available to make your own application, some of the pitfalls to avoid, and an overview of rendering virtual reality content with some tips and tricks on high performance rendering. The talk will contain content for everyone interested!

