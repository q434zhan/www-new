---
name: '''Static Analysis and Program Optimization Using Dataflow Analysis'''
short: 'An introduction to some basic issues with optimization of imperative programs, by Sean Harrap'
startDate: 'November 23 2015 18:00'
online: false
location: 'MC 4041'
---

An introduction to some basic issues with optimization of imperative programs by Sean Harrap, beginning with traditional methods such as tree traversals.

This will be followed by a more powerful solution to these problems, providing an overview of its mathematical foundations, and then describing how it can be used to express optimizations simply and elegantly.

Some familiarity with the second year CS core (CS245, CS241, MATH239) will be assumed.

