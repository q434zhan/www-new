---
name: 'CSC Codey-lympics'
short: 'Join us for a fun night of CS-themed challenges such as Code Your Friends, LeetCode Relay, Tech Skribbl.io, and more!'
startDate: 'October 18 2023 18:00'
endDate: 'October 18 2023 21:00'
online: false
location: 'TBD'
poster: 'images/events/2023/fall/1707695568525--Codeylympics.png'
---

🏃 Join us for a fun night of CS-themed challenges such as Code Your Friends, LeetCode Relay, Tech Skribbl.io, and more! Compete in teams as you complete these challenges to earn points for a chance to win some free CSC merch!

📢 Sign up via the link in our bio to register for the event either individually or with a team of friends! Those who sign up individually will be assigned teams on the day of the event.

📆 When? October 18th, 2023 at 6:00 - 9:00pm EST

📍 Where? Room TBD – check the email you used to register, as well as our Discord and Insta story for updates



