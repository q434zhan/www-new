---
name: 'Acing the Technical Interview w/ ICPC Coach Christian Lim'
short: 'Gain insight into the tech interview process directly from Christian Lim, a former Google software engineer and current ICPC coach at Columbia University!'
startDate: 'October 04 2023 18:00'
endDate: 'October 04 2023 19:30'
online: true
location: 'Zoom'
poster: 'images/events/2023/fall/1700095053678--Christian-Lim-Talk.png'
---

🚀 Want an inside scoop on how to ace the technical interview? Interested in learning about competitive programming? Then look no further! Join us for a workshop hosted by Christian Lim: former Google software engineer, Two Sigma quant developer, and current ICPC coach at Columbia University.

📢 Sign up via the link in our bio to register for the event and receive your invitation!

📆 When? October 4th, 2023 at 6:00 - 7:30pm EDT

📍 Where? Online via Zoom

