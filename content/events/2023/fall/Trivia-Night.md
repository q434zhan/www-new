---
name: 'Trivia Night'
short: 'Join us for a trivia single elimination bracket with over 400$ in prizes!'
startDate: 'November 22 2023 18:00'
endDate: 'November 22 2023 20:00'
online: false
location: 'STC 0050'
poster: 'images/events/2023/fall/1707443520699--CSC-Trivia-Night.png'
---

🚀 Calling all tech wizards and code connoisseurs! UWCSC presents an epic Trivia Night, where over $400 in prizes await the cleverest minds! 🤖💻 Join the coding quest, solve mind-bending puzzles, and emerge victorious to claim your tech-tastic rewards. It's not just trivia, it's a byte-sized battle for glory! 🏆✨ Let the games begin! 🌟

👉 Register at the link in our bio. See you there!

📆 Event Date: November 22nd, 6-8PM

📌 Location: STC0050
