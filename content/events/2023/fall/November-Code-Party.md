---
name: 'November Code Party'
short: 'Midterm destress and come socialize! Free food and drink will be provided.'
startDate: 'November 17 2023 19:00'
endDate: 'November 17 2023 21:00'
online: false
location: 'STC 0040'
poster: 'images/events/2023/fall/1707681572946--november_code_party.png'
---
Attention CSC members!
Code party is back! Come join us and work on assignments or side projects, study, or just hang out with your friends :D 🌭

We’re serving free Chung Chun rice dogs for those who come to hang out (first come first serve, must be a CSC member!)

📆 Event Date: Friday November 17, 7-9pm

📌 Location: STC 0040
