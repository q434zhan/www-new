---
name: 'Fall 2023 Elections'
short: 'The CS Club will be holding elections for the Fall 2023 term on Tuesday, September 12 at 6:00PM in MC 2035.'
startDate: 'September 12 2023 18:00'
online: false
location: 'MC 2035'
poster: 'images/events/2023/fall/1707695256725--F23-Elections.png'
---

🗳 The CS Club will be holding elections for the Fall 2023 term on Tuesday, September 12 at 6:00PM in MC 2035.

👉 Come to learn more about CSC, sign up for membership, and vote on our new execs! The president, vice-president, assistant vice-president, and treasurer will be elected, and the sysadmin will be appointed.

✋ If you'd like to run for any of these positions or nominate someone, you can send an email to cro@csclub.uwaterloo.ca, or present them in-person to the CRO, Sat Arora, or write your name on the whiteboard in the CSC office (MC 3036/3037). Nominations will close on September 11 at 6:00PM. Nominees will be reached out to for their platforms.

❗If you are unable to attend the elections in person, please email cro@csclub.uwaterloo.ca by September 11 at 11:59pm in order to request an absentee ballot and vote remotely.

❓ If you have any questions about elections, please email cro@csclub.uwaterloo.ca.

