---
name: "April Fool's Day Code Party"
short: "Come hang out to work on your assignments, side projects and board games. There will be snacks, refreshments, and fun April Fool’s pranks throughout the evening!"
startDate: "March 31 2023 18:00"
endDate: "March 31 2023 20:00"
online: false
location: "DC 1351"
poster: "images/events/2023/winter/April-Code-Party.jpg"
---

📣📣 Attention CSC Members! Come join us for our 🎊 April Fool’s 🎊 themed code party! There will be free pizza 🍕, free Codey Coins 🤫, some fun pranks, and the chance to win some prizes… So come hang out, study, and get bamboozled with us! 

📅 Event Date: March 31, 6-8 pm

📌 Location : DC 1351

See you there!!