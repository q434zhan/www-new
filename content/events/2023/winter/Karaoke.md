---
name: 'CSC Karaoke'
short: 'CSC Karaoke has returned! 😊🎶 Come out for a night of post-midterm destress and sing all your troubles away 🎤.'
startDate: 'March 3 2023 19:00'
endDate: 'March 3 2023 21:00'
online: false
location: 'STC 0010'
poster: 'images/events/2023/winter/Karaoke.png'
registerLink: 'https://docs.google.com/forms/d/1mqaBe_4XqZS3HjowGtPCZobw4ErCTq-mTnJwHTeh4Vs/edit'
---

CSC Karaoke has returned! 😊🎶 Come out for a night of post-midterm destress and sing all your troubles away 🎤. There will be free food and hot chocolate ☕ to enjoy while singing your heart out, so come out and have fun with other CSC members!

✅ Click the link for registration & song requests

🗓 Event Details: Friday, March 3rd from 7-9 PM 

📍 Location: STC 0010