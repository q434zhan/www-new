---
name: 'Winter 2023 Elections'
short: 'The CS Club will be holding elections for the Winter 2023 term on Monday, January 12 at 6:00PM in MC 4064.'
startDate: 'January 12 2023 18:00'
endDate: 'January 12 2023 19:00'
online: false
location: 'MC 4064'
poster: 'images/events/2023/winter/Winter-2023-Elections.png'
---

🗳 The CS Club will be holding elections for the Winter 2023 term on Monday, January 12 at 6:00PM in MC 4064.

👉 Come to learn more about CSC, sign up for membership, and vote on our new execs! The president, vice-president, treasurer, and assistant vice-president will be elected, and the sysadmin will be appointed. Furthermore, we will vote on some changes to the constitution outlined here: https://csclub.ca/constitutionChanges

❗If you are unable to attend the elections in person, please email cro@csclub.uwaterloo.ca in order to request an absentee ballet and vote remotely.

❓ If you have any questions about elections, please email cro@csclub.uwaterloo.ca.
