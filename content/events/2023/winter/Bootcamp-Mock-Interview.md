---
name: 'Bootcamp: Mock Interviews'
short: 'Participate in mock interviews with upper-year mentors to practice and refine your interview skills!'
startDate: 'January 21 2023 18:00'
endDate: 'January 22 2023 20:00'
online: true
location: 'Discord'
poster: 'images/events/2023/winter/Bootcamp.jpg'
---

📣 Applications to the Winter 2023 Bootcamp are now open! CSC is joined by @techplusuw, @uwaterloodsc, @uwaterloowics, @watonomous, and @uwblueprint to help you put your best foot forward in your next co-op hunt.
 
⭐️ As a Bootcamp mentee, you will be able to receive valuable feedback on your resume and sharpen your interviewing skills through our mock interviews. You will be matched by field/career interest with one of our many experienced mentors to bring you guidance from various backgrounds!
 
📅 The Resume Reviews will take place on the weekend of January 14th and 15th, 2023 from 6:00-10:00 p.m. ET both days, and the Mock Interviews will take place on the weekend of January 21st and 22nd, 2023 from 6:00-10:00 p.m. ET both days. Both events will be held virtually on our Bootcamp Discord Server.
 
⏳ The Resume Reviews and Mock Interviews are both drop-in events, which means you can show up at whichever times work best for you within the event period!
 
⭐️ To sign up as a mentee, please fill out the Mentee Sign-Up form at the link in our bio!
 
⚠️ The deadline to sign up is January 12th, 2023 at 11:59 p.m. ET.
 
🤩 Don’t miss out on this amazing opportunity! We hope to see you at Bootcamp!!
