---
name: 'CSC Olympics'
short: 'Join us to have fun outdoors participating in team activities and touching grass now that midterms are over. 🌱'
startDate: 'July 7 2023 19:00'
endDate: 'July 7 2023 21:00'
online: false
location: 'CIF Field' 
poster: 'images/events/2023/spring/CSC-Olympics.png'
---

☀️ Looking for a chance to meet others, get active, and enjoy Summerloo? Join us to have fun outdoors participating in team activities and touching grass now that midterms are over. 🌱

🚩 CSC Olympics is going to be a medley of everyone’s favourite outdoor activities, like capture the flag, volleyball, a relay race, and other fun team competitions.🏃🏐

🍹 Snacks and will be provided!

📅 Friday, July 7th, 2023, 7-9 PM

📍 CIF Field (beside CIF and beach volleyball courts)
