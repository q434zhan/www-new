---
name: 'Jobs and Career Panel'
short: 'Join us for an informative evening with tech industry professionals! You will hear about a day in the life of a UX designer, software developer, and data scientist.'
startDate: 'June 21 2023 18:00'
endDate: 'June 21 2023 20:00'
online: false
location: 'DC 1351'
poster: 'images/events/2023/spring/career-panel.png'
---

🎤 Join us for an informative evening with tech industry professionals! You will hear about a day in the life of a UX designer, software developer, and data scientist. The guest speakers will also give tips for job search, guidance for different paths, and more. There will be an opportunity for Q&A and networking. 🤝

⚠️ Don’t miss out on these valuable career insights!

📅 Date: Wednesday, June 21st, 2023

📍 Location: DC 1351

😋 There will be free refreshments and snacks for all CSC members attending!
