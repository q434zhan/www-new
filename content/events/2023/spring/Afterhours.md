---
name: 'Afterhours'
short: 'Take part in candid conversations on topics that university students typically overlook.'
startDate: 'August 01 2023 20:00'
endDate: 'August 01 2023 22:00'
online: false
location: 'SLC Black & Gold Room'
poster: 'images/events/2023/spring/Afterhours.png'
---

📣 Afterhours is back!!

😌 Come join us for a chill, informal group discussion about a variety of topics, including finding balance, building self-confidence, dealing with imposter syndrome and burnout, and any other topics you’d like to bring into the conversation!

🤩 You’ll get the chance to hear personal stories from moderators and other attendees, as well as share your own experiences in a close-knit, non-judgmental environment.

🥰 Snacks and drinks will be provided for CSC members.

📆 Date: Tuesday, August 1st, 8-10 PM, in the SLC Black and Gold Room

Hope to see you all there!