---
name: 'EOT Carnival'
short: 'CSC x DSC Fair: Meet, Socialize and Have Fun!'
startDate: 'July 29 2023 16:00'
endDate: 'July 29 2023 19:00'
online: false
location: 'M3 Green'
poster: 'images/events/2023/spring/EOT-Carnival.png'
---

🎉 Join us for a fun-filled day of games, food, and socializing with the computer science and data science community.

🎁 Don't miss out on the chance to redeem food and prizes.

📍 And we'll be meeting on the M3 green, which is the open/green area outside of SLC.

🗓️ Saturday, July 29th from 4-7 PM

👋🏼 See you there!