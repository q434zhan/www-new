---
name: "UW Blueprint x CSC: Engineering Night (React Workshop and Social)"
short: "Interested in getting started with front-end development? Join us for an Intro to React workshop! There will be free food & social after."
startDate: "July 10 2023 19:00"
endDate: "July 10 2023 22:00"
online: false
location: "E7 Ideas Clinic"
poster: "images/events/2023/spring/React-Workshop.png"
---

📣 📣 Attention CSC members!

Join CSC and UW Blueprint for an exciting night! We’ll start off with a React workshop for those looking to explore Front-End development (bring your laptops) then end the night with a social and food!

Come join us to learn a new skill, meet some new people, and have some fun 😎

📆 Event Date: Monday July 10th, 7-10pm

📌 Location: E7 Ideas Clinic
