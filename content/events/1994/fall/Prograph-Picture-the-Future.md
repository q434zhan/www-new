---
name: "Prograph: Picture the Future"
short: ""
startDate: 'October 13 1994 18:00'
online: false
location: "DC 1302"
---

What is the next step in the evolution of computer languages? Intelligent agents? Distributed objects? or visual languages?

Visual languages overcome many of the drawbacks and limitations of the textual languages that software development is based on today. Do you think about programming in a linear fashion? Or do you draw a mental picture of your algorithm and then linearize it for the benefit of your compiler? Wouldn't it be nice if you could code the same way you think?

Visual C++ and Visual BASIC aren't visual languages, but Prograph is. Prograph is a commercially available, visual, object-oriented, data-flow language. It is well suited to graphical user interface development, but is as powerful for general-purpose programming as any textual language.

The talk will comprise a discussion of the problems of textual languages that visual languages solve, a live demonstration of Prograph, and some of my observations of the applications of Prograph to software development.
