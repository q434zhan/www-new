---
name: 'Movie Outing: Brainstorm'
short: '	No description available.	'
startDate: 'September 13 1994 21:00'
online: false
location: 'Princess Cinema'
---

The first of this term's CSC social events, we will be going to see	the movie \`\`Brainstorm'' at the Princess Cinema. This outing is	intended primarily for the new first-year students.

The Princess Cinema is Waterloo's repertoire theatre. This month	and next, they are featuring a \`\`Cyber Film Festival''. Upcoming	films include:

- Brazil
- Bladerunner (director's cut)
- 2001: A Space Odyssey
- Naked Lunch

<!-- -->

Admission is $4.25 for a Princess member, $7.50 for a non-member.	Membership to the Princess is $7.00 per year.

