---
name: 'Realising the Next Generation Internet'
short: 'By Frank Clegg of Microsoft Canada'
startDate: 'September 25 2000 14:30'
online: false
location: 'DC1302'
---

### Vitals

<dl><dt>By</dt><dd>Frank Clegg</dd><dd>President, Microsoft Canada</dd><dt>Date</dt><dd>Monday, September 25, 2000</dd><dt>Time</dt><dd>14:30 - 16:00</dd><dt>Place</dt><dd>DC 1302</dd><dd>(Davis Centre, Room 1302, University of Waterloo)</dd><dt>Cost</dt><dd>$0.00</dd><dt>Pre-registration</dt><dd>Recommended</dd><dd><a href="http://infranet.uwaterloo.ca:81/infranet/semform.htm">http://infranet.uwaterloo.ca:81/infranet/semform.htm</a></dd><dd>(519) 888-4004</dd></dl>

### Abstract

The Internet and the Web have revolutionized our communications, our access to information and our business methods. However, there is still much room for improvement. Frank Clegg will discuss Microsoft's vision for what is beyond browsing and the dotcom. Microsoft .NET (pronounced "dot-net") is a new platform, user experience and set of advanced software services planned to make all devices work together and connect seamlessly. With this next generation of software, Microsoft's goal is to make Internet-based computing and communications easier to use, more personalized, and more productive for businesses and consumers. In his new position of president of Microsoft Canada Co., Frank Clegg will be responsible for leading the organization toward the delivery of Microsoft .NET. He will speak about this new platform and the next generation Internet, how software developers and businesses will be able to take advantage of it, and what the .NET experience will look like for consumers and business users.

### The Speaker

Frank Clegg was appointed president of Microsoft Canada Co. this month. Prior to his new position, Mr. Clegg was vice-president, Central Region, Microsoft Corp. from 1996 to 2000. In this capacity, he was responsible for sales, support and marketing activities in 15 U.S. states. Mr. Clegg joined Microsoft Corp. in 1991 and headed the Canadian subsidiary until 1996. During that time, Mr. Clegg was instrumental in introducing several key initiatives to improve company efficiency, growth and market share. Mr. Clegg graduated from the University of Waterloo in 1977 with a B. Math.

### For More Information

<address> Shirley Fenton
 The infraNET Project
 University of Waterloo
 519-888-4567 ext. 5611
<a href="http://infranet.uwaterloo.ca/">http://infranet.uwaterloo.ca/</a></address>

