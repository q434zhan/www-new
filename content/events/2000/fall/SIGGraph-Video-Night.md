---
name: 'SIGGraph Video Night'
short: ' SIGGraph Video Night Featuring some truly awesome computer animations from Siggraph ''99. '
startDate: 'September 14 2000 19:00'
online: false
location: 'DC1302'
---

Interested in Computer Graphics?

Enjoy watching state-of-the-art Animation?

Looking for a cheap place to take a date?

SIGGraph Video Night -	Featuring some truly awesome computer animations from Siggraph '99.

Come out for the Computer Science Club general elections at 6:00	pm, right before SIGGraph!

