---
name: 'CSC Elections'
short: 'Fall 2000 Elections for the CSC.'
startDate: 'September 14 2000 18:00'
online: false
location: 'DC1302'
---

Would you like to get involved in the CSC? Would you like to have a	say in what the CSC does this term? Come out to the CSC Elections!	In addition to electing the executive for the Fall term, we will be	appointing office staff and other positions. Look for details in	uw.csc.

Nominations for all positions are being taken in the CSC office, MC	3036.

