---
name: "Enterprise Java APIs and Implementing a Web Portal (1)"
short: ""
startDate: 'March 30 2000 17:30'
online: false
location: "DC1304"
---

Real World J2EE - Design Patterns and architecture behind the yet to be released J2EE portal: theserverside.com

This talk will feature an exclusive look at the architecture behind the new J2EE portal: theserverside.com. Join Floyd Marinescu in a walk-through of the back-end of the portal, while learning about J2EE and its real world patterns, applications, problems and benefits.
