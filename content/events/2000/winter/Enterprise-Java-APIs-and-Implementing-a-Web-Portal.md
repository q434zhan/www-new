---
name: "Enterprise Java APIs and Implementing a Web Portal"
short: ""
startDate: 'March 24 2000 17:30'
online: false
location: "DC1304"
---

### by Floyd Marinescu

The first talk will be an introduction to the Enterprise Java API's: Servlets, JSP, EJB, and how to use them to build eCommerce sites.

The second talk will be about how these technologies were used to implement a real world portal. The talk will include an overview of the design patterns used and will feature architectural information about the yet to be release portal (which I am one of the developers) called theserverside.com.
