---
name: 'Spring 2019 Elections'
short: 'The Computer Science Club will be holding elections for the Spring 2019 term on Thursday, May 16th in MC Comfy (MC 3001) at 7:00 PM.'
startDate: 'May 16 2019 19:00'
online: false
location: 'MC Comfy (MC 3001)'
---

During the meeting, the president, vice-president, treasurer and assistant vice-president (formerly secretary) will be elected, the sysadmin will be ratified, and the librarian and office manager will be appointed.

If you'd like to run for any of these positions or nominate someone, you can write your name on the whiteboard in the CSC office (MC 3036/3037) or send an email to cro@csclub.uwaterloo.ca. You can also deposit nominations in the CSC mailbox in MathSoc or present them to the CRO, Charlie Wang, in-person. Nominations will close at 6:00 PM on Wednesday, May 15th.

