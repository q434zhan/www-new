---
name: 'CSC + WiCS Movie Night'
short: 'CSC + WiCS Movie Night on March 21th, 2019, at 6 PM in MC Comfy'
startDate: 'March 21 2019 18:00'
online: false
location: 'MC Comfy'
---

CSC + WiCS Movie Night on March 21th, 2019, at 6 PM in MC Comfy

Join us and bring all your friends to CSC and WiCS' movie night! We will be showing your fave tech-related movies and serving some delicious snacks.

