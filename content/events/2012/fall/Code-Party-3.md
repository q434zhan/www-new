---
name: 'Code Party 3'
short: 'The Computer Science Club is running our third, and last, code party of the term! Whether you''re a hacking guru or a newbie to computer science, you''re welcome to attend; there will be activities for all. Syed Albiz will be presenting a tutorial on implementing a [ray-tracer](<http://en.wikipedia.org/wiki/Ray-tracing>) in C and Scheme.'
startDate: 'November 23 2012 19:00'
online: false
location: 'MC 3001'
---

The Computer Science Club is running our third, and last, code party of the term! Whether you're a hacking guru or a newbie to computer science, you're welcome to attend; there will be activities for all. Syed Albiz will be presenting a tutorial on implementing a [ray-tracer](<http://en.wikipedia.org/wiki/Ray-tracing>) in C and Scheme. Everyone is welcome, so please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.

