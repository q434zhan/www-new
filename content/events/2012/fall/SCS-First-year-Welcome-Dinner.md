---
name: 'SCS First-year Welcome Dinner'
short: 'The School of Computer Science is hosting a dinner event for incoming first-year students. You''ll get to meet us, some of the faculty, and other new undergraduates. Food will be provided.'
startDate: 'September 06 2012 17:00'
online: false
location: 'Math 3 Atrium'
---

The School of Computer Science is hosting a dinner event for incoming first-year students. You'll get to meet us, some of the faculty, and other new undergraduates. Food will be provided.

