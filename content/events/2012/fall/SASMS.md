---
name: 'SASMS'
short: '*by PMC.*The Pure Mathematics, Applied Mathematics, Combinatorics & Optimization Club is hosting the Fall 2012 Short Attention Span Math Seminars (SASMS).'
startDate: 'November 21 2012 16:30'
online: false
location: 'MC 5136B'
---

The Pure Mathematics, Applied Mathematics, Combinatorics & Optimization Club is hosting the Fall 2012 Short Attention Span Math Seminars (SASMS).

All talks will be 25 minutes long, and everyone is welcome to give a talk. Applications for speaking are open until the day of the event. For event details, see [the PMC event page.](<http://pmclub.uwaterloo.ca/?q=content/sasms-fall-2012>)

