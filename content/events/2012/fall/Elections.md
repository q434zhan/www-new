---
name: 'Elections'
short: 'CSC Elections have begun for the Fall 2012 term, nominations are open!'
startDate: 'September 18 2012 16:00'
online: false
location: 'Comfy Lounge'
---

It's elections time again! On Tuesday September 18th at 4:00PM, come to the Comfy Lounge on the 3rd floor of the MC to vote in this term's President, Vice-President, Treasurer and Secretary. The sysadmin, librarian, and office manager will also be chosen at this time.

Nominations are open until 4:00PM on Monday September 17th, and can be written on the CSC office whiteboard (yes, you can nominate yourself). All CSC members can vote and are invited to drop by. You may also send nominations to the [ Chief Returning Officer](<mailto:cro@csclub.uwaterloo.ca>). A full list of candidates will be posted when nominations close.

Good luck to our candidates!

