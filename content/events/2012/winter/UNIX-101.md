---
name: 'UNIX 101'
short: '*by Calum T. Dalek*. New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a series of tutorials on using Unix, beginning with Unix 101 this upcoming Thursday. Topics that will be covered include basic interaction with the shell and the motivations behind using it, and an introduction to compilation. You''ll have to learn this stuff in CS 246 anyways, so why not get a head start!'
startDate: 'February 09 2012 17:00'
online: false
location: 'MC 3003'
---

New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a series of tutorials on using Unix, beginning with Unix 101 this upcoming Thursday. Topics that will be covered include basic interaction with the shell and the motivations behind using it, and an introduction to compilation. You'll have to learn this stuff in CS 246 anyways, so why not get a head start!

If you're interested in attending, make sure you can log into the Macs on the third floor, or show up to the CSC office (MC 3036) 20 minutes early for some help. If you're already familiar with these topics, don't hesitate to come to Unix 102, planned to be held after Reading Week.

