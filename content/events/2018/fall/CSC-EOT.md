---
name: 'CSC EOT'
short: 'CSC End of term celebrations on December 3rd, at 7 PM in MC Comfy.'
startDate: 'December 03 2018 07:00'
online: false
location: 'MC Comfy'
---

CSC End of term celebrations on December 3rd, at 7 PM in MC Comfy.

The term is coming to an end, and we will be celebrating as such on Monday December 3rd 7-9pm in the MC Comfy.

Come hang out with fellow CSC members, and enjoy food and cake!

