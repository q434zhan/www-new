---
name: 'CSC & WiCS & MathSoc go outside!'
short: 'We will be having a bonfire this Wednesday, Oct 3rd 7-10pm, at Columbia Lake Firepit 2 (NW of CIF), co-hosted with WiCS and MathSoc. Smores and snacks will be provided!'
startDate: 'October 03 2018 19:00'
online: false
location: 'Columbia Lake Firepit 2'
---

We will be having a bonfire this Wednesday, Oct 3rd 7-10pm, at Columbia Lake Firepit 2 (NW of CIF), co-hosted with WiCS and MathSoc. Smores and snacks will be provided!

Here's a map that shows the firepit location: [https://uwaterloo.ca/economics/sites/ca.economics/files/uploads/files/firepit\_map\_oct\_2012.pdf](<https://uwaterloo.ca/economics/sites/ca.economics/files/uploads/files/firepit_map_oct_2012.pdf>)

