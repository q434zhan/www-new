---
name: 'Gloves 101'
short: 'Make touchscreen gloves with the CS Club on October 29, 5:30 PM in QNC 1506.'
startDate: 'October 29 2018 17:30'
online: false
location: 'QNC-1506'
---

Do you have a pair of favorite gloves that you wish you could use with your phone? Do you not have that but have always wondered how touchscreen gloves work? Join us on Monday Oct 29th 5:30-6:30pm at QNC1506 to learn how you can make your own pair of touchscreen gloves! Fatema Boxwala will be teaching a hands-on workshop with all materials provided (but you can also totally bring your own). Come and learn a super easy introduction to wearable electronics and sewing!

