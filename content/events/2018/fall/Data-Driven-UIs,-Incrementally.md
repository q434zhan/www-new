---
name: 'Data Driven UIs, Incrementally'
short: 'Jane Street''s Yaron Minsky is coming to Waterloo to give a talk aimed at undergraduate students.'
startDate: 'October 15 2018 17:30'
online: false
location: 'DC 1302'
---

Jane Street's Yaron Minsky is coming to Waterloo to give a talk aimed at undergraduate students. The talk titled Data Driven UIs, Incrementally will be held in DC 1302 on Oct. 15th 5:30-6:30pm. Yaron Minsky got his BA in Mathematics from Princeton and his PhD in Computer Science from Cornell, where he studied distributed systems. He joined Jane Street in 2003, where he started out developing quantitative trading strategies, going on to found the firm's quantitative research group. Here's a brief description of the talk:

Trading in financial markets is a data-driven affair, and as such, it requires applications that can efficiently filter, transform and present data to users in real time.

But there's a difficult problem at the heart of building such applications: finding a way of expressing the necessary transformations of the data in a way that is simultaneously easy to understand and efficient to execute over large streams of data.

This talk will show how we've approached this problem using Incremental, an OCaml library for constructing dynamic computations that update efficiently in response to changing data. We'll show how Incremental can be used throughout the application, from the servers providing the data to be visualized, to the JavaScript code that generates DOM nodes in the browser. We'll also discuss how these applications have driven us to develop ways of using efficiently diffable data structures to bridge the worlds of functional and incremental computing.

