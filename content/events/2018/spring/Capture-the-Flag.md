---
name: 'Capture the Flag'
short: 'Test your programming, web, networking, and trivia skills in this computer security themed contest. Learn how to reverse engineer, crack codes, find flaws in websites, and use security tools.'
startDate: 'July 11 2018 18:00'
online: false
location: 'STC 0010'
---

Test your programming, web, networking, and trivia skills in this computer security themed contest. Learn how to reverse engineer, crack codes, find flaws in websites, and use security tools.

Play as an individual, a team of up to three, or join a team at the event.

Bring your laptops, have fun, win prizes!

Run by Capture The Flag Club in partnership with CACR and the CS Club.

See http://ctf.uwaterloo.ca/ for more info!

