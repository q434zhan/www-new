---
name: 'Code Party 0'
short: 'The CS Club is hosting our first Code Party of the term from 6:30pm until \~9:30pm in STC 0020!Come code with us, eat some food, do some things.'
startDate: 'May 28 2018 18:30'
online: false
location: 'STC 0020'
---

The CS Club is hosting our first Code Party of the term from 6:30pm until \~9:30pm in STC 0020!

Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food.

Come any time after 6:30pm.

