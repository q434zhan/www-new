---
name: 'Code Party 0'
short: 'Our first code party of the term! Food is sandwiches, constitution amendments are a go, and Dr. Morton will be talking there! It''ll be fun.'
startDate: 'January 25 2018 18:00'
online: false
location: 'STC 0040'
---

The food is sandwiches, fruit platter, and coffee! You can consume this sustenance while:

- Dr. Andrew Morton talks about an upcoming CS final year project course!
- We take up Constitution and Code of Conduct amendments
- We elect someone to the position of Secretary
- Show off any cool things we're working on, and
- Just, like, hang out for a while (that's what code parties are for)

<!-- -->

Come out! There will be cool people there we promise. Like our VP Charlie Wang.

