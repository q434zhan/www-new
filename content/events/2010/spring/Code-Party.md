---
name: 'Code Party'
short: 'There is a CSC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times.'
startDate: 'July 09 2010 19:00'
online: false
location: 'MC Comfy'
---

There is a CSC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times.

