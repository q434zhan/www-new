---
name: 'A Brief Introduction to Video Encoding'
short: '*By Peter Barfuss*. In this talk, I will go over the concepts used in video encoding (such as motion estimation/compensation, inter- and intra- frame prediction, quantization and entropy encoding), and then demonstrate these concepts and algorithms in use in the MPEG-2 and the H.264 video codecs. In addition, some clever optimization tricks using SIMD/vectorization will be covered, assuming sufficient time to cover these topics.'
startDate: 'September 28 2010 16:30'
online: false
location: 'MC4061'
---

*By Peter Barfuss*. With the recent introduction of digital TV and the widespread success of video sharing websites such as youtube, it is clear that the task of lossily compressing video with good quality has become important. Similarly, the complex algorithms involved require high amounts of optimization in order to run fast, another important requirement for any video codec that aims to be widely used/adopted.

In this talk, I will go over the concepts used in video encoding (such as motion estimation/compensation, inter- and intra- frame prediction, quantization and entropy encoding), and then demonstrate these concepts and algorithms in use in the MPEG-2 and the H.264 video codecs. In addition, some clever optimization tricks using SIMD/vectorization will be covered, assuming sufficient time to cover these topics.

