---
name: 'UNIX 103'
short: 'Unix 103 will cover version control systems and how to use them to manage your projects. Unix 101 would be helpful, but all that is needed is basic knowledge of the Unix command line (how to enter commands).'
startDate: 'October 06 2010 16:30'
online: false
location: 'MC3003'
---

Unix 103 will cover version control systems and how to use them to manage your projects. Unix 101 would be helpful, but all that is needed is basic knowledge of the Unix command line (how to enter commands).

