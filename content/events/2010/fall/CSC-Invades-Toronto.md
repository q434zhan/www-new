---
name: 'CSC Invades Toronto'
short: 'The CSC is going to Toronto to visit UofT''s [CSSU](<http://cssu.cdf.toronto.edu/>), see what they do, and have beer with them. If you would like to come along, please come by the office and sign up. The cost for the trip is $2 per member. The bus will be leaving from the Davis Center (DC) Saturday Nov. 13 at NOON (some people may have been told 1pm, this is an error). Please show up a few minutes early so we may board.'
startDate: 'November 13 2010 12:00'
online: false
location: 'Outside DC'
---

The CSC is going to Toronto to visit UofT's [CSSU](<http://cssu.cdf.toronto.edu/>), see what they do, and have beer with them. If you would like to come along, please come by the office and sign up. The cost for the trip is $2 per member. The bus will be leaving from the Davis Center (DC) Saturday Nov. 13 at NOON (some people may have been told 1pm, this is an error). Please show up a few minutes early so we may board.

