---
name: 'Windows Azure Lab'
short: 'Get the opportunity to learn about Microsoft''s Cloud Services Platform, Windows Azure. Attend this Hands-on-lab session sponsored by Microsoft.'
startDate: 'April 07 2010 13:00'
online: false
location: 'MC2037'
---

We are in the midst of an industry shift as developers and businesses embrace the Cloud. Technical innovations in the cloud are dramatically changing the economics of computing and reducing barriers that keep businesses from meeting the increasing demands of today's customers. The cloud promises choice and enables scenarios that previously were not economically practical.

Microsoft's Windows Azure is an internet-scale cloud computing services platform hosted in Microsoft data centers. The Windows Azure platform, allows developers to build and deploy production ready cloud services and applications. With the Windows Azure platform, developers can take advantage of greater choice and flexibility in how they develop and deploy applications, while using familiar tools and programming languages.

Get the opportunity to learn about Microsoft's Cloud Services Platform, Windows Azure. Attend the Hands-on-lab session sponsored by Microsoft.

