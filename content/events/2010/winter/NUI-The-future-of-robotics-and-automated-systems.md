---
name: 'NUI: The future of robotics and automated systems'
short: 'Member Sam Pasupalak will present some of his ongoing work in Natural User Interfaces and Robotics in this sixth installment of CS10.'
startDate: 'March 30 2010 16:30'
online: false
location: 'DC1304'
---

Bill Gates in his article “A Robot in every home” in the Scientific American describes how the current robotics industry resembles the 1970’s of the Personal Computer Industry. In fact it is not just Microsoft which has already taken a step forward by starting the Microsoft Robotics studio, but robotics researchers around the world believe that robotics and automation systems are going to be ubiquitous in the next 10-20 years (similar to Mark Weiser’s analogy of Personal Computers 20 years ago). Natural User Interfaces (NUIs) are going to revolutionize the way we interact with computers, cellular phones, household appliances, automated systems in our daily lives. Just like the GUI made personal computing a reality, I believe natural user interfaces will do the same for robotics.

During the presentation I will be presenting my ongoing software project on natural user interfaces as well as sharing my goals for the future, one of which is to provide an NUI SDK and the other to provide a common Robotics OS for every hardware vendor that will enable people to make applications without worrying about underlying functionality. If time permits I would like to present a demo of my software prototype.

