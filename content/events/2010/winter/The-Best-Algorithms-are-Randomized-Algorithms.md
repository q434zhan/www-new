---
name: 'The Best Algorithms are Randomized Algorithms'
short: 'In this talk Nicholas Harvey discusses the prevalence of randomized algorithms and their application to solving optimization problems on graphs; with startling results compared to deterministic algorithms.'
startDate: 'February 23 2010 16:30'
online: false
location: 'MC5136B'
---

For many problems, randomized algorithms are either the fastest algorithm or the simplest algorithm; sometimes they even provide the only known algorithm. Randomized algorithms have become so prevalent that deterministic algorithms could be viewed as a curious special case. In this talk I will describe some startling examples of randomized algorithms for solving some optimization problems on graphs.

