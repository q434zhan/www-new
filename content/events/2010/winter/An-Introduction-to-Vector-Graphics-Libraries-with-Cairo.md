---
name: 'An Introduction to Vector Graphics Libraries with Cairo'
short: 'Cairo is an open source, cross platform, vector graphics library with the ability to output to many kinds of surfaces, including PDF, SVG and PNG surfaces, as well as X-Window, Win32 and Quartz 2D backends. Unlike the raster graphics used with programmes and libraries such as The Gimp and ImageMagick, vector graphics are not defined by grids of pixels, but rather by a collection of drawing operations. These operations detail how to draw lines, fill shapes, and even set text to create the desired image. This has the advantages of being infinitely scalable, smaller in file size, and simpler to express within a computer programme. This talk will be an introduction to the concepts and metaphors used by vector graphics libraries in general and Cairo in particular.'
startDate: 'February 09 2010 16:30'
online: false
location: 'DC1304'
---

Cairo is an open source, cross platform, vector graphics library with the ability to output to many kinds of surfaces, including PDF, SVG and PNG surfaces, as well as X-Window, Win32 and Quartz 2D backends. Unlike the raster graphics used with programmes and libraries such as The Gimp and ImageMagick, vector graphics are not defined by grids of pixels, but rather by a collection of drawing operations. These operations detail how to draw lines, fill shapes, and even set text to create the desired image. This has the advantages of being infinitely scalable, smaller in file size, and simpler to express within a computer programme. This talk will be an introduction to the concepts and metaphors used by vector graphics libraries in general and Cairo in particular.

