---
name: 'A Party of Code'
short: 'A fevered night of code, friends, fun, energy drinks, and the CSC.'
startDate: 'March 12 2010 19:00'
online: false
location: 'Comfy Lounge'
---

A fevered night of code, friends, fun, energy drinks, and the CSC.

Come join us for a night of coding. Get in touch with more experianced coders, advertize for/bug squash on your favourite open source project, write that personal project you were planning to do for a while but haven't found the time. Don't have any ideas but want to sit and hack? We can find something for you to do.

