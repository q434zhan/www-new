---
name: 'A Final Party of Code'
short: 'There is a CSC/CMC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times, spreading Intertube memes (optional), hacking on open source projects, doing some computational math, and other general classiness. There will be free energy drinks for everyone''s enjoyment. This is the last of the term so don''t miss out.'
startDate: 'March 26 2010 19:00'
online: false
location: 'MC7001'
---

There is a CSC/CMC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times, spreading Intertube memes (optional), hacking on open source projects, doing some computational math, and other general classiness. There will be free energy drinks for everyone's enjoyment. This is the last of the term so don't miss out.

