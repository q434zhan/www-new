---
name: 'CSC Goes to Toronto Erlang Factory Lite 2013'
short: 'The CSC has been invited to attend this Erlang conference in Toronto. If you are interested in attending, please sign up on our [web form](<http://goo.gl/8XOELB>). We have submitted a MEF proposal to cover the transportation fees of up to 25 math undergraduates.'
startDate: 'November 23 2013 00:00'
online: false
location: 'Toronto, ON'
---

The CSC has been invited to attend this Erlang conference in Toronto. If you are interested in attending, please sign up on our [web form](<http://goo.gl/8XOELB>), so we can coordinate the group. We have submitted a MEF proposal to cover the transportation fees of up to 25 math undergraduates to attend. You will be responsible for your conference fee and transportation, and if the MEF proposal is granted, you can submit your bus tickets/mileage record and conference badge to MEF for a reimbursement. From the [conference website](<https://www.erlang-factory.com/conference/Toronto2013>):

"Our first ever Toronto Erlang Factory Lite has been confirmed. Join us on 23 November for a full day debate on Erlang as a powerful tool for building innovative, scalable and fault tolerant applications. Our speakers will showcase examples from their work experience and their personal success stories, thus presenting how Erlang solves the problems related to scalability and performance. At this event we will focus on what Erlang brings to the table in the multicore era."

