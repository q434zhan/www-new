---
name: 'Why Should You Care About Security and Privacy'
short: 'The first lecture of our security and privacy series. By PhD student Sarah Harvey.'
startDate: 'October 08 2013 17:00'
online: false
location: 'MC 4041'
---

Recent media coverage has brought to light the presence of various government agencies' surveillance programs, along with the possible interference of governments in the establishment and development of standards and software. This brings to question of just how much we need to be concerned about the security and privacy of our information.

In this talk we will discuss what all this means in technological and social contexts, examine the status quo, and consider the long-standing implications. This talk assumes no background knowledge of security or privacy, nor any specific technical background. All students are welcome and encouraged to attend.

The first lecture of our security and privacy series. By PhD student Sarah Harvey.

