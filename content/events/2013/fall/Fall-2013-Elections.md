---
name: 'Fall 2013 Elections'
short: 'Elections for Fall 2013 are being held! The Executive will be elected, and the Office Manager and Librarian will be appointed by the new executive.'
startDate: 'September 17 2013 16:30'
online: false
location: 'Comfy Lounge'
---

It's elections time again! On Tuesday, Sept 17 at 4:30PM, come to the Comfy Lounge on the 3rd floor of the MC to vote in this term's President, Vice-President, Treasurer and Secretary. The Sysadmin, Librarian, and Office Manager will also be chosen at this time.

Nominations are open until 4:30PM on Monday, Sept 16, and can be written on the CSC office whiteboard (yes, you can nominate yourself). Full CSC members can vote and are invited to drop by. You may also send nominations to the [ Chief Returning Officer](<mailto:cro@csclub.uwaterloo.ca>). A full list of candidates will be posted when nominations close.

Nominations are now closed. The candidates are:

- President:- Dominik Chłobowski (<tt>dchlobow</tt>

    )
    - Elana Hashman (<tt>ehashman</tt>

    )
    - Sean Hunt (<tt>scshunt</tt>

    )
    - Marc Burns (<tt>m4burns</tt>

    )
    - Matt Thiffault (<tt>mthiffau</tt>

    )

    <!-- -->

- Vice-President:- Dmitri Tkatch (<tt>dtkatch</tt>

    )
    - Marc Burns (<tt>m4burns</tt>

    )
    - Sean Hunt (<tt>scshunt</tt>

    )
    - Visha Vijayanand (<tt>vvijayan</tt>

    )

    <!-- -->

- Treasurer:- Bernice Herghiligiu (<tt>baherghi</tt>

    )
    - Dominik Chłobowski (<tt>dchlobow</tt>

    )
    - Jonathan Bailey (<tt>jj2baile</tt>

    )
    - Marc Burns (<tt>m4burns</tt>

    )

    <!-- -->

- Secretary:- Dominik Chłobowski (<tt>dchlobow</tt>

    )
    - Edward Lee (<tt>e45lee</tt>

    )
    - Marc Burns (<tt>m4burns</tt>

    )

    <!-- -->


<!-- -->

