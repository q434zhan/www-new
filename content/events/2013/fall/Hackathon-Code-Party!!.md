---
name: 'Hackathon-Code Party!!'
short: 'Join us for a night of code, food, and caffeine! There will be plenty of edibles and hacking for your enjoyment. If you are interested in getting involved in Open Source, there will be mentors on hand to get you started. Hope to see you there—bring your friends!'
startDate: 'November 22 2013 18:30'
online: false
location: 'MC 3001 (Comfy)'
---

Join us for a night of code, food, and caffeine! There will be plenty of edibles and hacking for your enjoyment, including a full catered dinner courtesy of the Mathematics Society.

There will be two Open Source projects featured at tonight's code party, with mentors on hand for each. Here is a quick summary of each of the projects available:

**[OpenHatch](<http://openhatch.org>):** Not sure where to start? Not to fear! OpenHatch is a project that seeks to introduce people to Open Source for the first time and help you get involved. There will be a presentation with an introduction to the tools and information you will need, and mentors present to help you get set up to fix your first bug.

**[Social Innovation Simulation Design Jam](<http://uwaterloo.ca/games-institute/events/social-innovation-simulation-design-jam-day-1>):** The UWaterloo Games Institute and SiG@Waterloo will be partnering with us tonight to kick off their weekend hackathon Design Jam. They seek coders, artists, writers, database and graphics people to help them out with their project.

