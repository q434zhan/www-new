---
name: 'C++ GoingNative Lectures'
short: 'We will be showing GoingNative lectures from some of the top individuals working on C++ approximately biweekly on Thursdays at 6:30PM in the PHY 150 theatre. Every lecture will be accompanied with free pizza and drinks! Dates are Oct. 3, 17, 31 and Nov. 7 and 21. Please view this event in detail for more information.'
startDate: 'October 03 2013 18:30'
online: false
location: 'PHY 150'
---

If you're not familiar with the C++ GoingNative series, you can check them out on the [GoingNative website](<http://channel9.msdn.com/Events/GoingNative/2013>).

We will be showing lectures from some of the top individuals working on C++ approximately biweekly on Thursdays in the PHY 150 theatre. Every lecture will be accompanied with free pizza and drinks! Here is our schedule and the planned showings:

- Thu. Oct. 3, 6:30PM: Stroustrup - The Essence of C++
- Thu. Oct. 17, 6:30PM: Lavavej - Don't Help The Compiler
- Thu. Oct. 31, 6:30PM: Meyers - An Effective C++ Sampler
- Thu. Nov. 7, 6:30PM: Alexandrescu - Writing Quick C++ Code, Quickly
- Thu. Nov. 21, 6:30PM: Parent - C++ Seasoning

<!-- -->

