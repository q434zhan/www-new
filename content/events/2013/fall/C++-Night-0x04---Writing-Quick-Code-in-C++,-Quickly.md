---
name: 'C++ Night 0x04 - Writing Quick Code in C++, Quickly'
short: 'The fourth in a series of recorded talks from GoingNative 2013. Featuring Andrei Alexandrescu.'
startDate: 'November 07 2013 18:30'
online: false
location: 'PHY 150'
---

The fourth in a series of recorded talks from GoingNative 2013. Featuring Andrei Alexandrescu.

Contemporary computer architectures make it possible for slow code to work reasonably well. They also make it difficult to write really fast code that exploits the CPU amenities to their fullest. And the smart money is on fast code—we’re running out of cool things to do with slow code, and the battle will be on doing really interesting and challenging things at the envelope of what the computing fabric endures.

So how to write quick code, quickly? Turns out it’s quite difficult because today’s complex architectures defy simple rules to be applied everywhere. It is not uncommon that innocuous high-level artifacts have a surprisingly high impact on the bottom line of an application’s run time (and power consumed).

This talk is an attempt to set forth a few pieces of tactical advice for writing quick code in C++. Applying these is not guaranteed to produce optimal code, but is likely to put it reasonably within the ballpark.

These tips are based on practical experience but also motivated by the inner workings of modern CPUs.

