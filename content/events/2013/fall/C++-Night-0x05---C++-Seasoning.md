---
name: 'C++ Night 0x05 - C++ Seasoning'
short: 'The fifth in a series of recorded talks from GoingNative 2013. Featuring Sean Parent.'
startDate: 'November 21 2013 18:30'
online: false
location: 'PHY 150'
---

The fifth in a series of recorded talks from GoingNative 2013. Featuring Sean Parent.

A look at many of the new features in C++ and a couple of old features you may not have known about. With the goal of correctness in mind, we’ll see how to utilize these features to create simple, clear, and beautiful code. Just a little pinch can really spice things up.

