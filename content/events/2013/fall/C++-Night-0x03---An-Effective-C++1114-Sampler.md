---
name: 'C++ Night 0x03 - An Effective C++11/14 Sampler'
short: 'The third in a series of recorded talks from GoingNative 2013. Featuring Scott Meyers.'
startDate: 'October 31 2013 18:30'
online: false
location: 'PHY 150'
---

The third in a series of recorded talks from GoingNative 2013. Featuring Scott Meyers.

After years of intensive study (first of C++0x, then of C++11, and most recently of C++14), Scott thinks he finally has a clue. About the effective use of C++11, that is (including C++14 revisions). At last year’s Going Native, Herb Sutter predicted that Scott would produce a new version of Effective C++ in the 2013-14 time frame, and Scott’s working on proving him almost right. Rather than revise Effective C++, Scott decided to write a new book that focuses exclusively on C++11/14: on the things the experts almost always do (or almost always avoid doing) to produce clear, efficient, effective code. In this presentation, Scott will present a taste of the Items he expects to include in Effective C++11/14.

