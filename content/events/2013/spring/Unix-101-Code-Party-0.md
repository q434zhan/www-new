---
name: 'Unix 101/ Code Party 0'
short: 'We are offering a Unix tutorial on Friday, June 7th, 2013! Following the tutorial a code party will take place. Bring your laptops and chargers for an awesome night of coding, hacking and learning. All are welcome to join in the comfy lounge!'
startDate: 'June 07 2013 18:00'
online: false
location: 'Comfy Lounge'
---

We are offering a Unix tutorial on Friday, June 7th, 2013 at 6:00pm! Following the tutorial a code party will take place. Bring your laptops and chargers for an awesome night of coding, hacking and learning. All are welcome to join in the comfy lounge!

If you have any questions about Unix101/ Code Party 0 please contact exec@csclub.uwaterloo.ca.

Hope to see you there!

