---
name: 'Code Party 1'
short: 'Come out to the Code Party happening in the Comfy Lounge on July 26 at 7:00 PM! Why sleep when you could be hacking on $your\_favourite\_project or doing $something\_classy in great company? Join us for a night of coding, snacks, and camaraderie!'
startDate: 'July 26 2013 19:00'
online: false
location: 'Comfy Lounge'
---

Come out to the Code Party happening in the Comfy Lounge on July 26 at 7:00 PM! Why sleep when you could be hacking on $your\_favourite\_project or doing $something\_classy in great company? Join us for a night of coding, snacks, and camaraderie!

