---
name: '3D Movies and Computer Science'
short: 'While humans started making 3D motion pictures in the 1800''s, several technical and artistic challenges prevented widespread interest in the medium. By investing heavily in a computerized production pipeline, James Cameron''s 2009 release of Avatar ushered in an era of mainstream interest in 3D film. However, many technical and artistic problems still find their way into otherwise-modern 3D movies. The talk explores some of these problems while introducing the fundamentals of 3D film-making from a CS perspective.'
startDate: 'July 11 2013 17:00'
online: false
location: 'MC 4041'
---

While humans started making 3D motion pictures in the 1800's, several technical and artistic challenges prevented widespread interest in the medium. By investing heavily in a computerized production pipeline, James Cameron's 2009 release of Avatar ushered in an era of mainstream interest in 3D film. However, many technical and artistic problems still find their way into otherwise-modern 3D movies. The talk explores some of these problems while introducing the fundamentals of 3D film-making from a CS perspective.

