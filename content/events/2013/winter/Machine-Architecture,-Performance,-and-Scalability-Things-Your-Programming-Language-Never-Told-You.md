---
name: 'Machine Architecture, Performance, and Scalability: Things Your Programming Language Never Told You'
short: '"Herb Sutter is a leading authority on software development. He is the best selling author of several books including Exceptional C++ and C++ Coding Standards, as well as hundreds of technical papers and articles [and] has served for a decade as chair of the ISO C++ standards committee." - http://herbsutter.com/about'
startDate: 'February 28 2013 16:30'
online: false
location: 'DC 1302'
---

High-level languages insulate the programmer from the machine. That's a wonderful thing -- except when it obscures the answers to the fundamental questions of "What does the program do?" and "How much does it cost?"

The C++ and C# programmer is less insulated than most, and still we find that programmers are consistently surprised at what simple code actually does and how expensive it can be -- not because of any complexity of a language, but because of being unaware of the complexity of the machine on which the program actually runs.

This talk examines the "real meanings" and "true costs" of the code we write and run especially on commodity and server systems, by delving into the performance effects of bandwidth vs. latency limitations, the ever-deepening memory hierarchy, the changing costs arising from the hardware concurrency explosion, memory model effects all the way from the compiler to the CPU to the chipset to the cache, and more -- and what you can do about them.

