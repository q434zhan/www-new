---
name: 'Unix 101'
short: '*by Calum T. Dalek*. New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a tutorial on using Unix this upcoming Monday. Topics that will be covered include basic interaction with the shell and use of myriad powerful tools.'
startDate: 'April 01 2013 18:00'
online: false
location: 'MC 3003'
---

New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a tutorial on using Unix this upcoming Monday. Topics that will be covered include basic interaction with the shell and use of myriad powerful tools.

If you're interested in attending, make sure you can log into the Macs on the third floor, or show up to the CSC office (MC 3036) 20 minutes early for some help.

