---
name: 'Speed-Friending'
short: 'Join CSC to meet new people in a Speed-Friending event.'
startDate: 'March 16 2021 19:30'
online: true
location: 'Online'
---

Meeting people in University can be challenging, especially when it's virtual. We understand these struggles, and we want to give you a chance to meet other students! The CS community is huge, and we'd like to connect you all.

Have you heard of speed-dating? Well, CSC is now introducing our Speed-Friending event, where you will get the chance to meet people in fast rotating groups of 3 or 4, and talk to them for about 10 minutes We'll provide you with an optional prompt to start off your discussions, but the rest is up to you!. Feel free to leave at any time with a group you like.

You won't want to miss this event! Registration is optional; we'll just be sending you a reminder on the day of, as well as a calendar invite.

Event Date: Tuesday, March 16th from 7:30 - 8:30PM EST

Sign up at [https://bit.ly/uwcsclub-speed-friending-signup](<https://bit.ly/uwcsclub-speed-friending-signup>)! Alternatively, you can also email us at exec@csclub.uwaterloo.ca.

Click this link to join the event once it starts! We'll be hosting it on Zoom: [https://zoom.us/j/93667714038?pwd=aHIyOTAyOTNhd0VmREtONjl4QTNUUT09](<https://zoom.us/j/93667714038?pwd=aHIyOTAyOTNhd0VmREtONjl4QTNUUT09>).

