---
name: 'CTCI136'
short: 'Join us for tips and tricks on technical interviews.'
startDate: 'January 26 2021 19:00'
online: true
location: 'Online'
---

LeetCode? What is that?

Your favourite course is back and ready to help you cram for your upcoming WaterlooWorks interviews.

Join us on January 26 at 7PM EST on Twitch where we'll walk you through all the important concepts such as coding challenges, behavioural questions, and algorithms to help you crack the coding interview! Get your notebooks out and stay until the end to witness a CSC exec go through a sample problem and get some behind the scenes tips and tricks.

The event will be streamed at [twitch.tv/uwcsclub](<https://twitch.tv/uwcsclub>)

Register at [https://forms.gle/pqG47mPh8cyf2sWB8](<https://forms.gle/pqG47mPh8cyf2sWB8>)

