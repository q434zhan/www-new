---
name: 'Fun Friday'
short: 'Join CSC as we play games all night!'
startDate: 'February 19 2021 20:00'
online: true
location: 'Online'
---

Join CSC as we play games all night! Come for an opportunity to play League (win some free skins :eyes:), Scribbl.io, Among Us, Jackbox, Tetris and more!

This event will be occurring on our Discord: [https://discord.gg/pHfYBCg](<https://discord.gg/pHfYBCg>)

