---
name: 'Talk to CSC Tuesday'
short: 'Come meet and chill out with CSC execs.'
startDate: 'February 16 2021 20:00'
online: true
location: 'Online'
---

Come meet and chill out with CSC execs. This is an opportunity for you to meet new people and socialize with us!

This event will be occurring on our Discord: [https://discord.gg/pHfYBCg](<https://discord.gg/pHfYBCg>)

