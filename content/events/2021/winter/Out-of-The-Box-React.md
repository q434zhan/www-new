---
name: 'Out of The Box: React'
short: 'Learn how React works and make your own version!'
startDate: 'March 25 2021 19:00'
online: true
location: 'Online'
---

Modern web frameworks are a black-box. They're easy to use, but they have numerous minute details to master in order to apply them to truly scalable websites. Over the last few years, front-end frameworks have absorbed the responsibilities of the back-end, meaning it's become ever more important to dig their details out of the box.

Out of the Box is a series of code-along projects that explore what's under the hood of modern web frameworks. Nearly 5 million websites use React, including many of the internet's most popular websites. While its simple syntax attracts developers from all over, underneath lies a complex infrastructure of code to manage all elements from caching to hooks. Rishi will bring these ideas to light in our inaugural episode of Out of the Box. Come join him and code your own version of React!

Only basic web experience is needed. All JavaScript code will be written within a single HTML document for simplicity. Node.js will also be required to participate in the event!

Registration is not required to attend! We'll just be sending you an email reminder, as well as inviting you to our calendar event.

Event Date: Thurs. March 25 at 7-9 PM EST via Twitch ([http://twitch.tv/uwcsclub](<http://twitch.tv/uwcsclub>))

Register at [http://bit.ly/uwcsc-ootb-diy-react-signup!](<http://bit.ly/uwcsc-ootb-diy-react-signup!>) Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

