---
name: 'CSC BOT & Game Night'
short: 'Learn about our plans for the term and play some games with us.'
startDate: 'September 14 2021 19:00'
online: true
location: 'Twitch'
poster: 'images/events/2021/fall/BOT.png'
registerLink: https://bit.ly/csc-bot-event-signup-form
---

Kick off the fall term with CS Club’s BOT event! 🍂 Interested in attending upcoming CSC events? Want to meet others in the CS community?

💻 Come join us on Twitch to learn about how you can become a member of CSC and the fun events we have planned for next term. 🎲 Stay for games and socials on the CSC Discord!

Registration is not required to attend! We’ll just be sending you an email reminder, as well as inviting you to our calendar event.

📅 Event Date: Tuesday, September 14th from 7:00-8:30 pm ET via [Twitch](https://www.twitch.tv/uwcsclub) and [Discord](https://discord.gg/pHfYBCg)

👉 Register at https://bit.ly/csc-bot-event-signup-form! Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

Looking forward to seeing you there!! 🤗
