---
name: 'CSC x SE Soc: Interview Prep'
short: 'CSC and SE Soc are hosting an Interview Prep Workshop next week! Join Kristy Gao and Bimesh De Silva as they review the interview process, from behavioural to technical interviews. They''ll finish off with live mock interviews, and the event will be held on Twitch (https://www.twitch.tv/uwcsclub).'
startDate: 'May 24 2021 19:00'
online: true
location: 'Online'
---

Browsing through your unsolved Leetcode problems? Flipping open CTCI for the first time? Have no clue what to expect from a technical interview or just need an interview refresher?

We've got you covered at our CSC x SE Soc Interview Prep Workshop this Monday, May 24th from 7:00-8:30pm ET! Live-streamed on Twitch, Kristy Gao and Bimesh De Silva will be walking through important aspects of the interview process from coding challenges to behavioural interviews to algorithmic interviews, go through some live mock interviews, and share their tips and techniques along the way!

Registration isn't required, we'll just be sending you an email reminder, as well as inviting you to our calendar event!

The event will be hosted on Twitch at [https://www.twitch.tv/uwcsclub](<https://www.twitch.tv/uwcsclub>)

Register at [http://bit.ly/csc-sesoc-interview-prep-signup](<http://bit.ly/csc-sesoc-interview-prep-signup>)!

