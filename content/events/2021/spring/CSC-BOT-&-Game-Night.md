---
name: 'CSC BOT  & Game Night'
short: 'Learn about our plans for the term and play some games with us.'
startDate: 'May 17 2021 20:00'
online: true
location: 'Online'
---

Kick off your Spring term with CSC! Come join us on Discord to learn more about what we'll be up to this term and how you can participate!

Afterwards, stick around for a relaxing and fun game night. See you there!

