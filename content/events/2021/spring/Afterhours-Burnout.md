---
name: 'Afterhours: Burnout'
short: 'This Afterhours will be a discussion about recognizing burnout, recovering from burnout, and preventing it from occurring.'
startDate: 'July 17 2021 19:00'
online: true
location: 'Online'
---

CSC's next Afterhours is here! Afterhours is a series where we get the opportunity to dive into uncomfortable topics and share our experiences, as well as tips and tricks on how to overcome these problems.

It's unfortunately common to feel burnt out and exhausted, especially at this point in the term. This Afterhours will be a discussion about recognizing burnout, recovering from burnout, and preventing it from occurring. Speakers will share their personal stories, and the conversation will be open for all participants to discuss and share their own experiences.

Zoom Link: https://us06web.zoom.us/j/83131978922?pwd=Tm1NLzgraGZ2U1VCQVpDSFZxbGpSdz09

