#!/usr/bin/env bash

set -ex
ulimit -u 512

DIR=$(mktemp --directory)
trap "rm -rf $DIR" EXIT

# set USE_LDAP to true to generate member list using LDAP database
export USE_LDAP=true

pushd $DIR

git clone file:///srv/git/www/www-new.git --depth=1

cd www-new

npm install
npm run build
npm run export

chgrp -R www out
chmod -R g+w out

shopt -s dotglob

rm -rf /srv/www-csc/*
mv out/* /srv/www-csc/

popd
